$(function(){
    // carousel
    $('#my-carousel').carousel({
        interval: 5000
        });


    //sticky nav
    $("#prehead").sticky({topSpacing:0});
    
    $('#prehead').on('sticky-start', function(){
        $(this).css({'height':'50px'});
        $(this).parent().parent().removeClass('fixedheader');
        $(this).find('.logo img').attr('src', 'img/1.png');
    });

    $('#prehead').on('sticky-end', function(){
        console.log('end');
        $(this).css({'height':'100px'});
        $(this).parent().parent().addClass('fixedheader');
        $(this).find('.logo img').attr('src', 'img/2.png');
    });
   

    // button on click change pic
    $('button#digital').click(function(){
        var data = '<ul class=\"list-group\">' +
        '<li class=\"list-group-item\">Single Issue: <i class=\"fa fa-rupee-sign fa-fw small\"></i>10.</li>' +
        '<li class=\"list-group-item\">Read it while where ever you are.</li>'+
        '<li class=\"list-group-item\">Getting live experience.</li>'+
      '</ul>';

        console.log('clicked digital');
        $('#types .pic').find('img').fadeOut();
        $('#types .pic').css({'background': 'url(img/digital.png)', 'height': '350px', 'background-size': '250px', 'background-repeat': 'no-repeat', 'background-position': 'center'}, 'slow');
        $('#desc').empty().html(data);
    });

    $('button#print').click(function(){
        var latest = $('.pic img').attr('src');
        var data = '<ul class=\"list-group\">' +
        '<li class=\"list-group-item\">Single Issue: <i class=\"fa fa-rupee-sign fa-fw small\"></i> 15.</li>' +
        '<li class=\"list-group-item\">Great discount on annual pack.</li>'+
        '<li class=\"list-group-item\">Pay only <i class=\"fa fa-rupee-sign fa-fw small\"></i>150 for one year.</li>'+
      '</ul>';
        console.log('clicked print');
        $('#types .pic').find('img').fadeOut();
        $('#types .pic').css({'background': 'url('+ latest +')', 'height': '350px', 'background-size': '250px', 'background-repeat': 'no-repeat', 'background-position': 'center'}, 'slow');
        $('#desc').empty().html(data);
    });


    // Input mask
    $('#phone').inputmask("+\\91 9999999999", { 
        "escapeChar": "\\",
        "placeholder": " ___ ___ ____",
        "onincomplete": function() {
            $(this).addClass("incomplete");
        },
        "oncomplete": function() {
            $(this).removeClass("incomplete");
        }
        
    });
    
    $('#addCountryCode').click(function(){
        $('#mob').inputmask("+\\91 9999999999", { 
            "escapeChar": "\\",
            "placeholder": " ___ ___ ____",
            "onincomplete": function() {
                $(this).addClass("incomplete");
            },
            "oncomplete": function() {
                $(this).removeClass("incomplete");
            }
            
        });
    });
//static mask
    $('#pincode').inputmask("999999");  //static mask


    // total price calculator
    $('#period').on('change keyup', function(){
        var type = $('#type').val();
        var duration = $('#duration').val();
        var period = $(this).val();
        var total = $('#total');
        var price = 15;
        var totali = $('#totali');

        // console.log('Period changed');

        if($(this).val() >= 1){
            if(type == 'print'){
                
                price = 15;

                if(duration == 'year'){
                    price = 150;
                }

                if(duration == 'month' && period == 6){
                    price = 12.5;
                }

            }else{
                price = 10;
            }

            if(type == 'digital'){
                if(duration == 'month' && period == 1){
                    price = 10;
                }

                if(duration == 'year'){
                    price = 100;
                }
            }

            if(type == '' || duration == ''){
                price = 0;
                total.empty();
            }

            price = price * $(this).val();

            total.text(price);
            totali.val(price);
        }else{
            total.text(0);
        } 
        

    });

    $('#type').change(function(){
        var type = $(this).val();
        var duration = $('#duration').val();
        var period = $('#period').val();
        var price = 0;
        var total = $('#total');
        var totali = $('#totali');


        if(type !== '' && duration !== '' && period !== ''){
            if(type == 'print'){
                price = 15;

                if(duration == 'year'){
                    price = 150;
                }

                if(duration == 'month' && period == 6){
                    price = 12.5;
                }

            }else{
                price = 10;

                if(duration == 'year'){
                    price = 100;
                }
            }

            

            if(period >= 1){
                price = price * period;
            }else{
                price = 0;
            }

            total.text(price);
            totali.val(price);
            
        }else{
            total.text(0);
        }
    });

    $('#duration').change(function(){
        var duration = $(this).val();
        var type = $('#type').val();
        var period = $('#period').val();
        var price = 0;
        var total = $('#total');
        var totali = $('#totali');

        if(type !== '' || period !== ''){
            if(type == 'print'){
                price = 15;

                if(duration == 'year'){
                    price = 150;
                }

                if(duration == 'month' && period == 6){
                    price = 12.5;
                }
            }else{
                price = 10;

                if(duration == 'year'){
                    price = 100;
                }
            }

            

            if(period >= 1){
                price = price * period;
            }else{
                price = 0;
            }

            total.text(price);
            totali.val(price);
            
        }else{
            price = 0;
            total.text(price);
            totali.val(price);
        }

        if(duration == ''){
            price = 0;
            total.text(price);
            totali.val(price);
        }
    });




    // Year / Month function

    $('#duration').change(function(){
        var period = $('#period');

        if($(this).val() == 'year'){
            period.attr('placeholder', 'Year');
        }
        
        if($(this).val() == 'month'){
            period.attr('placeholder', 'Month');
        }

        if($(this).val() == ''){
            period.attr('placeholder', '');
        }


    });

    


});