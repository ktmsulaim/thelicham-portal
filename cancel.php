<?php
    $status=$_POST["status"];
    $firstname=$_POST["firstname"];
    $amount=$_POST["amount"];
    $txnid=$_POST["txnid"];
    
    $posted_hash=$_POST["hash"];
    $key=$_POST["key"];
    $productinfo=$_POST["productinfo"];
    $email=$_POST["email"];
    $salt="";
    
    // Salt should be same Post Request 
?>
    


<!DOCTYPE html>
<html lang="en">
    <head>
    
        <title>Thelicham Online | subscribe</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="style.css">

        <link rel="icon" href="img/home2.jpg" type="image/x-icon">
        

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,900&display=swap" rel="stylesheet">
        </head>
    <body>

        <header class="secondary" id="pay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="img/2.png" width="200" alt="">
                            <table class="float-right text-white">
                                    <tr>
                                        <td width="30px"><i class="fa fa-phone"></i></td>
                                        <td><a href="tel:+91 98473 33306">+91 98473 33306</a></td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><i class="fa fa-envelope"></i></td>
                                        <td><a href="mailto:info@thelicham.com">info@thelicham.com</a></td>
                                    </tr>
                                </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center"><i class="fa fa-fw fa-frown"></i> Cancelled</h1>
                    </div>
                </div>
            </div>
        </header>
        <main>
            <section class="section" id="payment">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="pay-box form-holder">
                                <div class="form-body">
                                    <?php
                                      
                                        If (isset($_POST["additionalCharges"])) {
                                            $additionalCharges=$_POST["additionalCharges"];
                                            $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
                                    } else {
                                            $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
                                            }
                                            $hash = hash("sha512", $retHashSeq);
                                    
                                            if ($hash != $posted_hash) {
                                                echo '<h2 class="form-title">'. $status .'!</h2>';
                                                echo '<p class="lead text-center">You have cancelled the payment process. If you want to subscribe to our magazine without paying online please contact our authority.</p>';
                                                echo '<div class="text-center mt-2" id="contact_whp"><a href="index.php" id="goback"class="btn btn-modern"><i class="fa fa-fw fa-home"></i> Go back</a></div>';
                                                } else {
                                                    echo "<p class=\"lead text-center\">Invalid Transaction. Please try again</p>";
                                            } 
                                    
                                    
                                    
                                    ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </section>
        </main>
        
        <?php include_once('footer.php'); ?>


    
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/jquery.inputmask.min.js"></script>
        <script src="main.js"></script>


    </body>
</html>