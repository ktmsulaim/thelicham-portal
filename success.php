<?php


$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt="";

// Salt should be same Post Request 


    $name = "";
    $so = "";
    $house = "";
    $place = "";
    $email = "";
    $phone = "";
    $district = "";
    $post = "";
    $pincode = "";
    $type = "";
    $duration = "";
    $period = "";
    $total = "";

?>


<!DOCTYPE html>
<html lang="en">
    <head>
    
        <title>Thelicham Online | subscribe</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="style.css">

        <link rel="icon" href="img/home2.jpg" type="image/x-icon">
        

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,900&display=swap" rel="stylesheet">
        </head>
    <body>

        <header class="secondary" id="pay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="img/2.png" width="200" alt="">
                            <table class="float-right text-white">
                                    <tr>
                                        <td width="30px"><i class="fa fa-phone"></i></td>
                                        <td><a href="tel:+91 98473 33306">+91 98473 33306</a></td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><i class="fa fa-envelope"></i></td>
                                        <td><a href="mailto:info@thelicham.com">info@thelicham.com</a></td>
                                    </tr>
                                </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center"><i class="fa fa-thumbs-up"></i> Success</h1>
                    </div>
                </div>
            </div>
        </header>
        <main>
            <section class="section" id="payment">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="pay-box form-holder">
                                <div class="form-body">

                                    <?php
                                        If (isset($_POST["additionalCharges"])) {
                                            $additionalCharges=$_POST["additionalCharges"];
                                             $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
                                       } else {
                                             $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
                                              }
                                              $hash = hash("sha512", $retHashSeq);
                                            if ($hash != $posted_hash) {

                                                session_start();
                                                    $_SESSION['status'] = 1;

                                                    echo '<h2 class="form-title">'. $status .'!</h2>    
                                                    <p class="lead text-center">Your payment has been proccessed successfully! You will be notified through <b>email</b> once subscription is done, please stay tuned.</p>
                                                    <p class="text-center mt-2">Your Transaction ID for this transaction is <b>'.$txnid.'</b></p>
                                                    <p class="text-center">We have received a payment of Rs. ' . $amount .'. Thank you!</p>';

                                                  echo '<div class="mt-4 info" id="contact_whp">
                                                    <div class="text-center btn-holder">
                                                        <a href="https://api.whatsapp.com/send?phone=919847333306&text=hi,%20I%20just%20subscribed%20to%20Thelicham%20Monthly%20and%20made%20payment%20of%20'.$amount.'.%20Transaction%20ID%20is%20'.$txnid.'" id="contact" class="btn btn-modern"><i class="fab fa-fw fa-whatsapp"></i> Send on whatsapp</a>
                                                        <a href="index.php" id="goback" class="btn btn-modern"><i class="fa fa-fw fa-home"></i> Go back</a>
                                                        <a href="track.php?id='.$_SESSION['sub_id'].'" id="view" class="btn btn-modern"><i class="fa fa-fw fa-eye"></i> View</a>
                                                    </div>
                                                </div>';
                                                 
                                                if($_SESSION['status'] == 1){
                                                    require_once('db.php');
                                                    if(Database::checkSubscription2($_SESSION['sub_id']) === true){
                                                        if(Database::updateSubscription($_SESSION['sub_id'], $_SESSION['method_id'], $txnid, $_SESSION['period'], $_SESSION['duration'])){
                                                            session_unset();
                                                        }else{
                                                            echo "Subscription failed. Please try again";
                                                        }
                                                    }else{
                                                        if(Database::addSubscription($_SESSION['sub_id'], $_SESSION['method_id'], $txnid, $_SESSION['period'], $_SESSION['duration'])){
                                                            session_unset();
                                                        }else{
                                                            echo "Subscription failed. Please try again";
                                                        }
                                                    }
                                                    
                                                }
                                                
                                                
                                                
                                                } else {
                                                    echo "Invalid Transaction. Please try again";
                                                }

                                    ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </section>
        </main>
        
        <?php include_once('footer.php'); ?>


    
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/jquery.inputmask.min.js"></script>
        <script src="main.js"></script>


    </body>
</html>