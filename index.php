<?php
    require_once('db.php');
    //insert new visitor into database
    
    $ip = $db::getUserIpAddr();

    //checking whether it is availabe in db
    $check = $db->query("SELECT ip FROM visitors WHERE ip = '{$ip}'");
    if($check->num_rows >= 1){

    }else{
        $db->query("INSERT INTO visitors(ip) VALUES('{$ip}')");
    }
    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
    
        <title>Thelicham Online | subscribe</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="style.css">

        <link rel="icon" href="img/favicon.ico" type="image/x-icon">
        

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,900&display=swap" rel="stylesheet">
        </head>
    <body>

        <header class="fixedheader">
            <section id="prehead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md col-6">
                            <div class="logo">
                                <img src="img/2.png" alt="Thelicham Monthly" height="80px">
                            </div>
                        </div>
                        <div class="col-md col-6">
                            <div class="clearfix info">
                            <table class="float-right">
                                    <tr>
                                        <td width="30px"><i class="fa fa-phone"></i></td>
                                        <td><a href="tel:+91 98473 33306">+91 98473 33306</a></td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><i class="fa fa-envelope"></i></td>
                                        <td><a href="mailto:info@thelicham.com">info@thelicham.com</a></td>
                                    </tr>
                                </table>
                                <div class="btn-holder p-1">
                                    <a id="contact" href="https://api.whatsapp.com/send?phone=919847333306&text=hi,%20I%20want%20to%20subscribe%20to%20Thelicham%20Monthly&source=&data=" class="btn-modern mr-4" title="Contact us">contact</a>
                                </div>
                                
                            </div>
                        </div>
                    </div>   
                </div>
            </section>
        </header>
        <main>
            <section id="plans">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                        <div id="my-carousel" class="carousel slide carousel-fade">
                        <div class="carousel-inner section .embed-responsive embed-responsive-16by9" role="listbox">
                           <h1 class="text-white text-center"><small>WELCOME TO</small><br>THELICHAM SUBSCRIPTION PORTAL</h1>
                            <!-- <div class="carousel-item active embed-responsive-item">
                                <div class="display-1 text-center">
                                    <i class="animated zoomIn fa fa-info-circle" style="animation-delay:1s"></i>
                                </div>
                                <div class="caption d-md-block mt-4">
                                    <h2 class="animated fadeInUp c-title1" style="animation-delay:1.5s;">Here is what you're <span class="red">looking</span> for!</h2>
                                    <p class="animated fadeInUp c-title1" style="animation-delay:1.8s;">really! alot <span class="red">to explore!</span></p>
                                </div>
                            </div>
                            <div class="carousel-item embed-responsive-item">
                                <div class="covers">
                                    <img id="june" class="d-inline-block m-auto animated fadeInLeft" src="img/june.jpg" width="180" alt="June 2019">
                                    <img id="may"class="d-inline-block m-auto animated fadeInLeft" src="img/may.jpg" width="200" alt="May 2019">
                                    <img id="july" class="d-inline-block m-auto animated zoomIn" src="img/july.jpg" width="230" alt="July 2019">
                                    <img id="april" class="d-inline-block m-auto animated fadeInRight" src="img/april.jpg" width="200" alt="April 2019">
                                    <img id="march" class="d-inline-block m-auto animated fadeInRight" src="img/march.jpg" width="180" alt="March 2019">
                                </div>

                                <div id="mainSubBtn" class="row animated fadeInDown" style="animation-delay:3s; margin-top:120px">
                                    <div class="col-md-6 mx-auto">
                                        <div class="btn-holder p-1 d-flex justify-content-center">
                                            <a id="btn-big" href="#form" class="btn-modern" title="Subscribe now">Subscribe now</a>
                                        </div>
                                    </div>
                                </div> -->
                                
                            </div>
                            <!-- <div class="carousel-item embed-responsive-item">
                            <img class="animated zoomIn d-block m-auto" style="animation-delay:1s;" src="img/july.jpg" width="200" alt="July 2019">
                                <div class="caption d-md-block mt-4">
                                    <h2 class="animated fadeInUp c-title1" style="animation-delay:1.5s;"><span class="red">get it</span> any way</h2>
                                    <p class="animated fadeInDown c-tag1" style="animation-delay:2s;">Now available in both <b class="red">print</b> and <b class="red">digital</b> format!</p>
                                </div>
                            </div>    -->
                        </div>

                        <!-- <a class="carousel-control-prev" href="#my-carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#my-carousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a> -->
                    </div>
                        </div>
                    </div>
                    

                    
                </div>
            </section>



            <section id="types" class="section d-flex">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="pic">
                                <?php 
                                    $current = Database::currentIssue();
                                    foreach($current as $issue){
                                        echo '<img src="manage/issues/'.$issue['cover'].'" width="300" alt="'.$issue['month'].' '.$issue['year'].'" title="'.$issue['month'].' '.$issue['year'].'">';
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-6" id="type-info">
                            <div class="btn-holder">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="head">Available formats</h3>
                                            <p class="text-muted">Choose one package!</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <button class="btn-modern btn-modern-type" id="print">print</button>
                                        </div>
                                        <div class="col">
                                        <button class="btn-modern btn-modern-type" id="digital">digital</button>
                                        </div>
                                    </div>
                                    <div class="row pt-4 pb-2" id="type-desc">
                                        <div class="col-12">
                                            <div id="desc">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </section>

            <section id="form" class="section">
                <div class="container">

                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="form-holder">
                                <div class="form-header">
                                    <h3 class="form-title">subscribe</h3>
                                    <p class="form-tip">Required fields are indicated *</p>
                                </div>
                                <div class="form-body">
                                    <!-- Maintenance mode -->
                                    <!-- <div class="text-center">
                                        <i class="fa fa-exclamation-triangle fa-3x"></i><br>
                                        Sorry! the service is temporarily unavailable
                                    </div> -->
                                <form action="add_subscriber.php" method="get" class="form-horizontal" role="form">
                                    <div class="form-row">
                                        <div class="errors w-100">
                                            <?php
                                                if(isset($_GET['error'])){
                                                    if($_GET['error'] == 'email'){
                                                        echo '<div class="alert alert-danger">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                        <strong>Error!</strong> The email address you have entered is already registered.
                                                    </div>';
                                                    }elseif($_GET['error'] == 'phone'){
                                                        echo '<div class="alert alert-danger">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                        <strong>Error!</strong> The mobile number you have entered is already registered.
                                                    </div>';
                                                    }elseif($_GET['error'] == 'enp'){
                                                        echo '<div class="alert alert-danger">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                        <strong>Error!</strong> The email address you have entered is already registered.
                                                    </div>';
                                                        echo '<div class="alert alert-danger">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                        <strong>Error!</strong> The mobile number you have entered is already registered.
                                                    </div>';
                                                    }else{
                                                        echo '';
                                                    }

                                                }
                                            ?>
                                            
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Name *</label>
                                            <input type="text" class="form-control" id="name" placeholder="Name" name="name" required="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="so">S/o, D/o, C/o *</label>
                                            <input type="text" class="form-control" name="so" id="so" placeholder="Son of/ Daughter of/ Care of" required="">
                                        </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="house">House *</label>
                                            <input type="text" class="form-control" id="house" name="house" placeholder="House" required="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="place">Place  *</label>
                                            <input type="text" class="form-control" id="place" name="place" placeholder="Place" required="">
                                        </div>
                                    </div>
                                    
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Valid email adress">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="phone">Phone  *</label>
                                                <input type="text" class="form-control phone" id="phone" name="phone" placeholder="Contact number" required="">
                                            </div>
                                    </div>
                                    

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="district">Dis­trict *</label>
                                            <input placeholder="District" type="text" class="form-control" name="district" id="district" required="">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="post">P.O *</label>
                                            <input placeholder="Post office" type="text" class="form-control" name="post" id="post" required="">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="pincode"><span class="caps">PIN</span> *</label>
                                            <input placeholder="Pincode" type="text" class="form-control" id="pincode" name="pincode" required="">
                                        </div>
                                    </div>
                                    <div class="form-group subs">
                                        <label for="plan">Sub­scrip­tion *</label>
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <select name="type" id="type"  class="form-control" required>
                                                    <option value="">Select type</option>
                                                    <option value="print">Print</option>
                                                    <option value="digital" disabled>Digital</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="duration" id="duration" class="form-control" required="">
                                                    <option value="">Select duration</option>
                                                    <option value="year">Year</option>
                                                    <option value="month">Month</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="number" id="period" class="form-control" name="period" required="">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="d-flex justify-content-center">
                                            <label for="total" class="control-form mr-2" style="line-height: 55px;">Total <i class="fa fa-rupee-sign"></i></label>
                                                 <h1 class="bordered w-25 text-center" id="total">0</h1>
                                                 <input type="hidden" name="total" value="0" id="totali">
                                                 <span class="loader"></span>
                                        </div>
                                    </div>
                                    <hr>
                                    <span class="promoStatus">Have a promo code? Apply</span> <span id="promoBtn" style="cursor:pointer" role="button" data-toggle="modal" data-target="#promoModal"><b>here</b></span>
                                    <input type="hidden" name="promo" id="promo">
                                    <!-- Promo Modal-->
                                   <div class="modal fade" id="promoModal" tabindex="-1" role="dialog" aria-labelledby="promoModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="promoModalLabel">Apply promo code</h5>
                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">If you have a valid promo code apply here, or contact the authority to get one.<br>
                                                        <input type="text" name="promoCode" id="promoCode" class="form-control col-md-3 mx-auto mt-2" placeholder="Eg: TH000">

                                                    </div>
                                                    <div class="modal-footer">
                                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                                    <span class="btn btn-primary" id="apply">Apply</span>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>

                                            <!-- Model end -->

                                            <div class="form-group mt-2">
                                                <div class="promoInserted">
                                                    <span id="prm"></span>
                                                </div>
                                            </div>
                                    <div class="form-group mt-4">
                                        <button id="submit" type="submit" class="btn-modern" name="submit"><i class="fa fa-paper-plane mr-2"></i> Submit</button>
                                    </div>
                                        </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section" id="track">
                <div class="main">
                    <div class="head text-center text-white">
                        <h3>Track your status online</h3>
                        <p>Enter your subscriber ID and mobile number to see the status!</p>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 mx-auto">
                                <form action="search.php" method="GET">
                                    <div class="input-group">
                                        <input type="text" name="id" class="form-control bg-light border-0 border-right small" placeholder="Subscriber ID" required>
                                        <input type="text" name="phone" class="form-control bg-light border-0 small phone" placeholder="Mobile number" required>
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="getId text-white py-2 small">Don't remember ID? <span id="findId" class="tooltip-test" style="cursor:pointer" role="button" title="Tooltip" data-toggle="modal" data-target="#findIdModal"><b>Find</b></span></div>
                                 <!-- Find ID Modal-->
                                 <div class="modal fade" id="findIdModal" tabindex="-1" role="dialog" aria-labelledby="findIdModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="promoModalLabel">Find subscriber ID</h5>
                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">Enter your mobile number to find ID<br>
                                                        <div class="input-group my-4">
                                                            <input type="text" id="mob" name="mobile" class="form-control bg-light border-1 small" placeholder="Enter a valid mobile number" required>
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary" type="submit" id="findIdBtn">
                                                                    <i class="fas fa-search fa-sm"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="py-3" id="resp"><b>Note:</b> Try adding country code eg(+91)</div>
                                                        <span class="small btn" style="cursor:pointer" id="addCountryCode">Add country code</span>
                                                        
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button id="modalClose" class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>

                                            <!-- Model end -->
                                <?php if(isset($_GET['notFound'])){ ?>
                                    <div class="py-2 text-center text-danger">
                                        Subscriber not found!
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
       
        <?php include_once('footer.php'); ?>

        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/jquery.inputmask.min.js"></script>
        <script src="main.js"></script>
        <!-- <script src="js/findId.js"></script> -->

        <script>
            $(function(){
                if($('#prm').text() == ""){
                    $('#prm').hide();
                }else{
                    $('#prm').show();
                }

                $('#apply').click(function(){
                   var promoModal = $("#promoModal");
                   var pormoCode = promoModal.find('#promoCode').val();
                   var prm = $('#prm');

                   if(pormoCode !== ''){
                       prm.text(pormoCode);
                       promoModal.find('#promoCode').empty();
                       $('#promoModal').modal('hide');
                       $('.promoStatus').text('Promo code inserted. ');
                       $('#promoBtn b').text("Change");
                       prm.show();
                   }else{
                       prm.empty();
                       $('.promoStatus').text('Have a promo code? Apply ');
                       $('#promoBtn b').text("here");
                       $('#prm').hide();
                   }
                });


                $('#submit').submit(function(){
                    $(this).attr('disabled', true).css({'opacity':0.5});
                    $(this).find('i').remove().append("<i class=\"fa fa-spin fa-circle-notch fa-fw\"></i>");

                });


                // ajax request
                $('#findIdBtn').click(function(){
                    $('#addCountryCode').hide();

                    var mobile = $("#mob").val();
                    var loader = '<i class="fa fa-circle-notch fa-spin fa-fw mr-2"></i> Searching';
                    
                    if(mobile !== ''){
                            $.ajax({
                            url: "findId.php",
                            method: "GET",
                            data: {mobile: mobile},
                            beforeSend: function(){
                                $('#resp').html(loader);
                            }
                        }).done(function(data){
                            $('#resp').empty();
                            $('#resp').html(data);
                        });

                    //clear data on close
                    $('#findIdModal').on('hidden.bs.modal', function(){
                        $(this).find('#resp').empty();
                        $(this).find('#mob').val('');
                        $('#addCountryCode').show();
                    });
                }else{
                    $('#resp').html('Moblie number is empty!');
                }

                });
                
            });
        </script>


    </body>
</html>