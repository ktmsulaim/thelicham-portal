<?php
    $nodata = "";
    if(isset($_GET['id'])){
        require_once('db.php');
        $id = $_GET['id'];
        $phone = $_GET['phone'];
        $search = Database::search($id, $phone);

        if($search->num_rows == 0){
            header('Location: index.php?notFound#track');
        }elseif($search->num_rows == 1){
            header('Location: track.php?id='.$id);
        }else{
            $nodata = '<div class="alert alert-info text-center">No results found</div>';
        }
    }else{
        header('Location: index.php');
    }

?>


<!DOCTYPE html>
<html lang="en">
    <head>
    
        <title>Thelicham Online | subscribe</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="style.css">

        <link rel="icon" href="img/home2.jpg" type="image/x-icon">
        

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,900&display=swap" rel="stylesheet">
        </head>
    <body>

        <header class="secondary" id="pay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="img/2.png" width="200" alt="">
                            <table class="float-right text-white">
                                    <tr>
                                        <td width="30px"><i class="fa fa-phone"></i></td>
                                        <td><a href="tel:+91 98473 33306">+91 98473 33306</a></td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><i class="fa fa-envelope"></i></td>
                                        <td><a href="mailto:info@thelicham.com">info@thelicham.com</a></td>
                                    </tr>
                                </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center">Search</h1>
                    </div>
                </div>
            </div>
        </header>
        <main>
            <section class="section" id="search">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="pay-box form-holder">
                                <div class="form-body">
                                    <div class="search mb-4">
                                        <form action="search.php" method="GET">
                                            <div class="input-group">
                                                <input type="text" name="q" class="form-control bg-light border-0 small" placeholder="<?php echo isset($_GET['q']) ? $_GET['q'] : 'Search for...'; ?>" required>
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary" type="submit">
                                                        <i class="fas fa-search fa-sm"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="search-term d-flex justify-content-between">
                                        <p>You have searched for: <b><?php echo $_GET['id']; ?></b></p> <p> Total results: <span class="badge badge-secondary"><?php echo $search->num_rows; ?></span></p>
                                    </div>
                                    <div class="mt-4">
                                        <div class="search-resul">
                                            <ul class="p-0">
                                                
                                                
                                                <?php
                                                    if($search->num_rows > 1){
                                                        foreach($search as $sub){
                                                            // get subscription
                                                            $subscription = Database::getSubscription($sub['id']);

                                                            if($sub['period'] > 1){
                                                                $sub['duration'] = $sub['duration'].'s';
                                                            }

                                                            if($subscription->num_rows >= 1){
                                                                foreach($subscription as $sb){
                                                                    $subdate = $sb['subscription_date'];
                                                                    $expdate = $sb['expiry_date'];
                                                                    $total = $sb['credits'];
                                                                    $left = $sb['credits_left'];
                                                                    $status = $sb['status'];
                                                                    $method = $sb['method_id'];
                                                                }
                                                                $credits = $left.'/'.$total;
                                                            }else{
                                                                    $subdate = "";
                                                                    $expdate = "";
                                                                    $total = "";
                                                                    $left = "";
                                                                    $method = "";
                                                                    $status = "Not active";
                                                                    $credits = "No credits";
                                                            }


                                                            echo '<a href="track.php?id='.$sub['id'].'" class="container d-block" id="adblock">
                                                            <li class="row">
                                                                <div class="col-md-3" id="pic">
                                                                    <img src="img/usernew.svg" class="img-fluid" alt="">
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div class="details py-2">
                                                                        <h4>'.strtoupper($sub['name']).'</h4>
                                                                        <table id="address"class="text-muted small">
                                                                            <tr>
                                                                                <td width="30"><i class="fa fa-mobile"></i></td>
                                                                                <td>'.(!empty($sub['phone']) ? $sub['phone'] : '<i>No phone number</i>').'</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="30"><i class="fa fa-envelope"></i></td>
                                                                                <td>'.(!empty($sub['email']) ? $sub['email'] : '<i>No email address</i>').'</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="30"><i class="fa fa-map-marker"></i></td>
                                                                                <td>'.$sub['house'].', '.$sub['place'].', '.$sub['district'].', '.$sub['post'].', '.$sub['pincode'].', '.(!empty($sub['rms']) ? $sub['rms'] : '<i>RMS not found</i>').'</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="30"><i class="fa fa-credit-card"></i></td>
                                                                                <td>'.ucfirst($sub['type']).' '.$sub['period'].' '.$sub['duration'].'</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="30"><i class="fa fa-calendar"></i></td>
                                                                                <td>Validity: '.$expdate.'</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="30"><b>C</b></td>
                                                                                <td>Credits: '.$credits.'</td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </li>
                                                            '.($method == 3 ? '<div class="verified" title="Payed with PayuMoney"></div>' : '').'
                                                        </a>';
                                                        }
                                                    }elseif($search->num_rows === 1){
                                                        foreach($search as $sub){
                                                            $id = $sub['id'];
                                                        }
                                                        //
                                                    }else{
                                                        echo $nodata; 
                                                    }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </section>
        </main>
        
        <?php include_once('footer.php'); ?>


    
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="main.js"></script>


    </body>
</html>