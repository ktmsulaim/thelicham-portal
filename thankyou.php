<?php

if(isset($_GET['added']) && isset($_GET['id'])){

    require_once("db.php");

    $id = $_GET['id'];
    $subscribers = $db::findById($id);

    while($row = mysqli_fetch_array($subscribers)){
        $name = $row['name'];
        $so = $row['so'];
        $house = $row['house'];
        $place = $row['place'];
        $email = $row['email'];
        $phone = $row['phone'];
        $district = $row['district'];
        $post = $row['post'];
        $pincode = $row['pincode'];
        $type = $row['type'];
        $duration = $row['duration'];
        $period = $row['period'];
        $total = $row['total'];
        $promo = $row['promo'];
    }
    
    // $grand_total = trim($_GET['amount']);
    $pfee = ($total * 2) / 100;
    $amount = $total + $pfee;
}

    

    // if(isset($_GET['added'])){
    //     $name = $_SESSION['name'];
    //     $so = $_SESSION['so'];
    //     $house = $_SESSION['house'];
    //     $place = $_SESSION['place'];
    //     $email = $_SESSION['email'];
    //     $phone = $_SESSION['phone'];
    //     $district = $_SESSION['district'];
    //     $post = $_SESSION['post'];
    //     $pincode = $_SESSION['pincode'];
    //     $type = $_SESSION['type'];
    //     $duration = $_SESSION['duration'];
    //     $period = $_SESSION['period'];
    //     $total = $_SESSION['total'];
    //     $pfee = $_SESSION['pfee'];
    //     $amount = $_SESSION['grand_total'];
    //     $promo = $_SESSION['promo'];
    // }else{
    //     session_unset();

    //     $name = "";
    //     $so = "";
    //     $house = "";
    //     $place = "";
    //     $email = "";
    //     $phone = "";
    //     $district = "";
    //     $post = "";
    //     $pincode = "";
    //     $type = "";
    //     $duration = "";
    //     $period = "";
    //     $total = "";
    //     $pfee = "";
    //     $amount = "";
    //     $promo = "";

    //     header('index.php');
    // }

    if(isset($_GET['id'])){
        $insert_id = $_GET['id'];
    }
    
?>
    


<!DOCTYPE html>
<html lang="en">
    <head>
    
        <title>Thelicham Online | subscribe</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="css/fancybox/jquery.fancybox.css">
        <link rel="stylesheet" href="style.css">

        <link rel="icon" href="img/home2.jpg" type="image/x-icon">
        

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,900&display=swap" rel="stylesheet">
        </head>
    <body>

        <header class="secondary" id="pay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <a href="index.php"><img src="img/2.png" width="200" alt="Thelicham Monthly"></a>
                            <table class="float-right text-white">
                                    <tr>
                                        <td width="30px"><i class="fa fa-phone"></i></td>
                                        <td><a href="tel:+91 98473 33306">+91 98473 33306</a></td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><i class="fa fa-envelope"></i></td>
                                        <td><a href="mailto:info@thelicham.com">info@thelicham.com</a></td>
                                    </tr>
                                </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center"><i class="fa fa-fw fa-thumbs-up"></i> Thank you</h1>
                    </div>
                </div>
            </div>
        </header>
        <main>
            <section class="section" id="payment">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="pay-box form-holder">
                                <div class="form-header">
                                    <h4 class="form-title">Subscription pending</h4>
                                    <p class="form-tip">Complete payment for confirmation.</p>
                                </div>
                                <div class="form-body">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th width="200">Name</th>
                                                <td><?php echo !empty($name) ? ucfirst($name) : 'No data'; ?></td>
                                            </tr>
                                            <tr>
                                                <th width="200">Email</th>
                                                <td><?php echo !empty($email) ? $email : 'No data'; ?></td>
                                            </tr>
                                            <tr>
                                                <th width="200">Phone</th>
                                                <td><?php echo !empty($phone) ? $phone : 'No data'; ?></td>
                                            </tr>
                                            <?php
                                                if(!empty($promo)){ ?>
                                                <tr>
                                                    <th width="200">Promo</th>
                                                    <td><?php 
                                                        require_once('db.php');
                                                        $results= $db::query("SELECT * FROM `institutions` WHERE promo = '{$promo}'");
                                                        while($row = mysqli_fetch_array($results)){
                                                            $insti = $row['name'];
                                                            $promo = $row['promo'];
                                                        }
                                                        echo $promo . " - ". $insti;

                                                        if(mysqli_num_rows($results) == 0){
                                                            echo "Promo code is not valid";
                                                        }
                                                    ?></td>
                                                </tr>
                                            <?php
                                                }
                                            ?>
                                            <tr>
                                                <th width="200">Subscription</th>
                                                <td><span class="badge badge-primary"><?php echo !empty($type) ? ucfirst($type) : 'None'; ?></span> <?php echo !empty($period) ? $period : 'No data'; ?>  <?php if(!empty($duration)){if($period > 1){echo $duration.'s';}else{echo $duration;}}else{echo 'No data';} ?></td>
                                            </tr>
                                            <tr>
                                                <th width="200">Amount</th>
                                                <td><i class="fa fa-rupee-sign fa-fw small"></i><?php echo !empty($total) ? $total : 'No data'; ?></td>
                                            </tr>
                                            <tr>
                                                <th width="200">Proccessing fee</th>
                                                <td><i class="fa fa-rupee-sign fa-fw small"></i><?php echo !empty($pfee) ? $pfee : 'No data'; ?> <span class="text-muted small ml-2">(Applicable on online payment)</span></td>
                                            </tr>
                                            <tr>
                                                <th width="200">Total</th>
                                                <td><h4><i class="fa fa-rupee-sign fa-fw small"></i><?php echo !empty($amount) ? $amount : 'No data'; ?></h4></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="payment-method mt-"3>
                                        <h4>Payment methods</h4>

                                        <div class="container">
                                            <div class="row mt-3">
                                                <div class="col-md-6 mt-2">
                                                    <a href="#payu" class="popup">
                                                        <center>
                                                            <span><img src="img/payu.png" width="80" alt=""></span><br>
                                                            <b class="text-muted">Pay online with Payu</b>
                                                        </center>
                                                    </a>
                                                    <div style="display:none" id="payu">
                                                        <center>
                                                            <p class="text-center">
                                                                <img src="img/payu.png" width="100" alt="Pay with PayuMoney.com">
                                                            </p>
                                                            <h4>Payu Money</h4>
                                                            <p class="lead text-muted">
                                                                <b><a href="pay.php?submit&id=<?php echo $insert_id; ?>">Go to payment page</a></b><br>
                                                            </p>
                                                        </center>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </section>
        </main>
        
        <?php include_once('footer.php'); ?>


    
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.fancybox.js"></script>
        <script src="main.js"></script>

        <script type="text/javascript">
            $(function() {
                $(".popup").fancybox({
                    maxWidth	: 300,
                    maxHeight	: 250,
                    fitToView	: false,
                    width		: '70%',
                    height		: '70%',
                    autoSize	: false,
                    closeClick	: true,
                    openEffect	: 'elastic',
                    closeEffect	: 'elastic'
                });

            });
    </script>

    </body>
</html>