<?php
include_once('db.php');

if(isset($_GET['mobile'])){
    $resp = '<div class="alert alert-info">It works</div>';

    $mobile = $_GET['mobile'];
    $result = $db::query("SELECT id, name, place, phone FROM subscribers WHERE phone = '{$mobile}'");
    $data = '<table class="table">
    <tbody>';

    if($result){
        
        if($result->num_rows == 1){
            foreach($result as $id){
                $data .=  '<tr>
                <th>ID</th>
                <td>'.$id['id'].'</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>'.$id['name'].'</td>
            </tr>
            <tr>
                <th>Place</th>
                <td>'.$id['place'].'</td>
            </tr>
            <tr>
                <th>Phone</th>
                <td>'.$id['phone'].'</td>
            </tr>';
            }

            $data .= '</tbody>
            </table>';
        echo $data;
        }elseif($result->num_rows > 1){
            echo '<p>Total results found: <span class="badge badge-secondary">'.$result->num_rows.'</span></p>';

            foreach($result as $id){
                $data .=  '<table class="table table-bordered">
                <tbody><tr>
                <th>ID</th>
                <td>'.$id['id'].'</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>'.$id['name'].'</td>
            </tr>
            <tr>
                <th>Place</th>
                <td>'.$id['place'].'</td>
            </tr>
            <tr>
                <th>Phone</th>
                <td>'.$id['phone'].'</td>
            </tr></tbody>
            </table>';
            }

            echo $data;

        }else{
            echo '<div class="alert alert-info text-center">Mobile number seems to be not valid!</div>';
        }
    }else{
        echo '<div class="alert alert-danger text-center">Sorry! ID number can\'t be found!</div>';
    }
}
