$(function(){
    $('#findIdBtn').click(function(){
        var mobile = $('#mobile').val();
        var loader = '<i class="fa fa-circle-notch fa-spin fa-fw mr-2"></i> Searching';
        if(mobile == ''){
            $('#resp').text('No mobile number found!');
        }else{
            $('#resp').html(loader);
        }

       $.ajax({
           method: "GET",
           url: "findId.php",
           data: {mobile:mobile},
           timeout: 2000,
           beforeSend: function(){
               $('#resp').html(loader);
           },
           complete: function(){
               $('#resp').empty();
           },
           success: function(data){
                $('#resp').html(data);
           },
           fail: function(){
               $('#resp').html('Sorry! Faild');
           }
       })
    });
});