<?php

$id = "";
$name = "";
$so = "";
$house = "";
$place = "";
$email = "";
$phone = "";
$district = "";
$post = "";
$pincode = "";
$type = "";
$duration = "";
$period = "";
$total = "";
$pfee = "";
$amount = "";
$promo = "";

if(isset($_GET['id'])){

    require_once("db.php");

    $id = $_GET['id'];
    $subscribers = $db::findById($id);
    $subscription = $db::getSubscription($id);


    while($row = mysqli_fetch_array($subscribers)){
        $id = $row['id'];
        $name = $row['name'];
        $so = $row['so'];
        $house = $row['house'];
        $place = $row['place'];
        $email = $row['email'];
        $phone = $row['phone'];
        $district = $row['district'];
        $post = $row['post'];
        $pincode = $row['pincode'];
        $type = $row['type'];
        $duration = $row['duration'];
        $period = $row['period'];
        $total = $row['total'];
        $promo = $row['promo'];
    }

    //making plurel duration
    if($period > 1){
        $duration = $duration .'s';
    }

    if($subscription->num_rows > 0){
        foreach($subscription as $plan){
            $credits_left = $plan['credits_left'];
            $valid_from = $plan['subscription_date'];
            $expiry_date = $plan['expiry_date'];
            $credits = $plan['credits'];
            $status = $plan['status'];
            $ref = $plan['ref_no'];
        }
    }else{
        $credits_left = 0;
        $valid_from = "";
        $expiry_date = "";
        $credits = 1;
        $status = "";
        $ref = "";
    }
    
    // $grand_total = trim($_GET['amount']);
    $pfee = ($total * 2) / 100;
    $amount = $total + $pfee;

    //credits percent
    $perc = ($credits_left * 100) / $credits;

    // progress class
    $pgclass = "";
    if($perc >= 75){
        $pgclass = "bg-success";
    }elseif($perc >= 50){
        $pgclass = "bg-info";
    }elseif($perc >= 25){
        $pgclass = "bg-warning";
    }else{
        $pgclass = "bg-danger";
    }


    if($subscribers->num_rows == 0){
        header('Location: index.php?notFound#track');
    }

}else{
    header('Location: index.php');
}

?>
    


<!DOCTYPE html>
<html lang="en">
    <head>
    
        <title>Thelicham Online | subscribe</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="style.css">

        <link rel="icon" href="img/home2.jpg" type="image/x-icon">
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,600,900&display=swap" rel="stylesheet">
        </head>
    <body>

        <header class="secondary" id="pay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="img/2.png" width="200" alt="">
                            <table class="float-right text-white">
                                    <tr>
                                        <td width="30px"><i class="fa fa-phone"></i></td>
                                        <td><a href="tel:+91 98473 33306">+91 98473 33306</a></td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><i class="fa fa-envelope"></i></td>
                                        <td><a href="mailto:info@thelicham.com">info@thelicham.com</a></td>
                                    </tr>
                                </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center">Track status</h1>
                    </div>
                </div>
            </div>
        </header>
        <main>
            <section class="section" id="payment">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 mx-auto">
                            <div class="pay-box form-holder">
                                <div id="track" class="form-body">
                                    <div class="row pb-3 mb-3">
                                        <div class="col-md-3 offset-md-9">
                                            <div class="search">
                                                <form action="search.php" method="GET">
                                                    <div class="input-group">
                                                        <input type="text" name="q" class="form-control bg-light border-0 small" placeholder="Search for..." required>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-primary" type="submit">
                                                                <i class="fas fa-search fa-sm"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="row">
                                       <div class="col-md-2 mb-4">
                                           <p class="text-center">
                                            <img src="img/user.png" width="100" alt="" class="user">
                                           </p>
                                           <p class="text-center m-0">
                                            <b><?php echo $name; ?></b>
                                           </p>
                                           <small><i class="fa fa-envelope mr-2 fa-fw"></i><?php echo $email; ?></small><br>
                                           <small><i class="fa fa-phone mr-2 fa-fw"></i><?php echo $phone; ?></small>
                                       </div>

                                       <div class="col-md-7 mb-4">
                                           <!-- Nav tabs -->
                                           <div class="tabpan box p-0">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#personal"><i class="fa fa-info-circle fa-fw mr-1"></i>Info</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#issues"><i class="fa fa-file fa-fw mr-1"></i>Issues</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#current"><i class="fa fa-circle-notch fa-fw mr-1"></i>Current Issue</a>
                                                </li>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div class="tab-pane container active" id="personal">
                                                    <h6 class="mt-2 mb-3">Personal Info</h6>
                                                    <table class="table resp mb-3">
                                                        <tbody>
                                                            <tr>
                                                                <td><b>So/Do/Co</b></td>
                                                                <td><?php echo strtoupper($so); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>House</b></td>
                                                                <td><?php echo strtoupper($house); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Place</b></td>
                                                                <td><?php echo strtoupper($place); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>District</b></td>
                                                                <td><?php echo strtoupper($district); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Post</b></td>
                                                                <td><?php echo strtoupper($post); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>PIN</b></td>
                                                                <td><?php echo $pincode; ?></td>
                                                            </tr>
                                                            </tbody>
                                                    </table>
                                                    </div>

                                                    <div class="tab-pane container fade" id="issues">
                                                        <h6 class="mt-2 mb-3">Issues</h6>

                                                        <?php
                                                            if(isset($_GET['id'])){
                                                                $id = $_GET['id'];
                                                                $dispatch = Database::getIssues($id);
                                                                if($dispatch->num_rows >= 1){
                                                                echo '<div class="owl-carousel owl-theme">';
                                                                foreach($dispatch as $log){
                                                                    $issue_id = $log['issue_id'];
                                                                    $date = $log['date'];

                                                                    $issues = Database::getIssueById($issue_id);
                                                                    foreach($issues as $issue){
                                                                        if(empty($issue['cover']) || $issue['cover'] == 'null'){
                                                                            $issue['cover'] = 'dummy.jpg';
                                                                        }
                                                                        echo '<div class="item mb-3 text-center"><img src="manage/issues/'.$issue['cover'].'" width="100" alt=""><h6 class="mt-2">'.$issue['month'].' '.$issue['year'].'</h6></div>';
                                                                    }
                                                                }

                                                                // end of owl carousel div
                                                                    echo ' </div>';
                                                                }else{
                                                                    echo '<div class="text-center"><b>No issues.</b> Make sure you have made a subscription.</div>';
                                                                }
                                                            }
                                                        ?>
                                                       
                                                    </div>
                                                    
                                                    <div class="tab-pane container fade p-4" id="current">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <?php
                                                                    $current = Database::getCurrent();
                                                                    $issues = Database::getIssueById($current['issue_id']);
                                                                    foreach($issues as $issue){
                                                                        $cover = $issue['cover'];
                                                                        $month = $issue['month'];
                                                                        $year = $issue['year'];
                                                                    }

                                                                    $dispatched = Database::checkDispatch($id, $current['issue_id']);

                                                                ?>
                                                                <img src="manage/issues/<?php echo $cover; ?>" class="img-fluid" width="130" alt="">
                                                            </div>
                                                            <div class="col-md-8 mt-3">
                                                                    <table class="table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td><b>Issue</b></td>
                                                                                <td><?php echo $month . ' ' . $year; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><b>Dispatch status</b></td>
                                                                                <td><?php echo !empty($dispatched) ? 'Dispatched on '. date('d-m-Y', strtotime($dispatched)) : '<span class="badge badge-warning">Pending</span>' ;  ?></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                        </div>
                                        
                                       </div>
                                       <div class="col-md-3 mb-4">
                                        <h6 class="mb-3">Subscription</h6>
                                        <?php if($subscription->num_rows == 1){ ?>
                                        <div class="text-center mb-2">
                                            <span class="display-4"><?php echo  $credits_left; ?></span><small> Credits left</small>
                                        </div>
                                        <div class="progress mt-2 mb-2" style="height:10px">
                                            <div class="progress-bar <?php echo $pgclass; ?>" role="progressbar" style="width: <?php echo number_format($perc, 2); ?>%; height:10px;" aria-valuenow="<?php echo number_format($perc, 2); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <table class="table">
                                            <tr>
                                                <td>Valid from</td>
                                                <td><?php echo $valid_from; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Expiry date</td>
                                                <td><?php echo $expiry_date; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Total credits</td>
                                                <td><?php echo $credits; ?></td>
                                            </tr>
                                            <?php 
                                                if(!empty($ref)){ ?>
                                                 <tr>
                                                    <td title="Receipt number">Receipt.No</td>
                                                    <td><?php echo $ref; ?></td>
                                                </tr>
                                              <?php  }
                                            ?>

                                            <tr>
                                                <td>Status</td>
                                                <td><?php echo $status == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Not active</span>'; ?></td>
                                            </tr>
                                        </table>
                                        <?php }else{ ?>
                                            <div class="alert alert-info">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <strong>Sorry!</strong> You have no subscription. <a href="thankyou.php?added&id=<?php echo $id; ?>">Pay now</a>
                                            </div>
                                            
                                        <?php } ?>
                                        <div class="plan">
                                            <div class="plan-details">
                                                <h3><?php echo $type == 'print' ? '<i class="fa fa-fw mr-2 fa-book-open"></i>' : '<i class="fa fa-fw mr-2 fa-mobile"></i>'; echo ucfirst($type); ?> <small><?php echo $period; ?> <?php echo $duration; ?></small></h3>
                                            </div>
                                        </div>
                                       </div>
                                       <div class="clearfix"></div>
                                   </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </section>
        </main>
        
        <?php include_once('footer.php'); ?>


    
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
        <script src="main.js"></script>
        <script>
            $(function(){
                // $(window).bind("resize", function () {
                //     console.log($(this).width())
                //     if ($(this).width() < 500) {
                //         $('table.resp').addClass('table-responsive')
                //     } else {
                //         $('table.resp').removeClass('table-responsive')
                //     }
                // }).trigger('resize');

                var owl = $('.owl-carousel');
                owl.owlCarousel({
                    items:4,
                    loop:true,
                    margin:10,
                    autoplay:true,
                    autoplayTimeout:1000,
                    autoplayHoverPause:true,
                    responsiveClass:true,
                    responsive:{
                        0:{
                            items:1,
                            nav:true
                        },
                        600:{
                            items:3,
                            nav:false
                        },
                        1000:{
                            items:5,
                            nav:true,
                            loop:false
                        }
                    }
                });
            });

        </script>


    </body>
</html>