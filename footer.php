<footer class="section">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md">
                        <div class="logo">
                            <a href="index.php"><img src="img/1.png" alt="Thelicham Monthly" width="200"></a>
                            <p class="small text-muted mt-2">&copy; <?php echo date('Y'); ?> All rights reserved.</p>
                        </div>
                    </div>
                    <div class="col-6 col-md">
                        <div class="footer-head">
                            <h5>Important links</h5>
                            <ul class="list-unstyled text-small">
                                <li><a class="text-muted" href="http://thelicham.com">Thelicham Official</a></li>
                                <li><a class="text-muted" href="#">Articles</a></li>
                                <li><a class="text-muted" href="#">Stories</a></li>
                                <li><a class="text-muted" href="#">Donate us</a></li>
                                <li><a class="text-muted" href="manage/">Portal login</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-md">
                    <div class="footer-head">
                            <h5>Locate us</h5>
                            <div class="text-small text-muted mt-3">
                                <div class="icon">
                                    <i class="fa fa-map-marker-alt fa-2x"></i>
                                    <p>Darul Huda Islamic University <br>
                                        Kerala,India,676306</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md">
                    <div class="footer-head">
                            <h5>About</h5>
                            <p class="text-small text-muted">
                            Thelicham Monthly is an Islamic cultural magazine in the Malayalam languages published by Darul Huda Islamic University from Kerala State of India.
                            </p>
                            <p>Developed by: <a href="http://sacreationsknr.info" target="_blank">SA Creations</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>