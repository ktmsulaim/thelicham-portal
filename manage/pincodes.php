<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>

<?php
if(isset($_POST['add'])){
    $office_name = trim($_POST['office_name']);
    $pincode = trim($_POST['pincode']);
    $district = trim($_POST['district']);
    $rms = trim($_POST['rms']);

    $pin = new Pincode();
    $pin->office_name = $office_name;
    $pin->pincode = $pincode;
    $pin->district = $district;
    $pin->rms = $rms;

    if($pin->create()){
        redirect("http://".$_SERVER['HTTP_HOST']."/manage/pincodes.php?added");
    }else{
        redirect("http://".$_SERVER['HTTP_HOST']."/manage/pincodes.php?failed");
    }
}

?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>





<!-- Begin Page Content -->
<div class="container-fluid">

<?php 
$pincodes = Pincode::all(); 
$total = count($pincodes);

if(isset($_GET['added'])){
    echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <strong>Success!</strong> The pincode has been added.
  </div>';
  }
if(isset($_GET['updated'])){
    echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <strong>Success!</strong> The pincode has been updated.
  </div>';
  }
  if(isset($_GET['deleted'])){
    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <strong>Success!</strong> The pincode has been deleted.
  </div>';
  }
?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Pincodes <span class="badge badge-primary"><?php echo $total; ?></span></h1>
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->

<!-- Content Row -->

<div class="row">
    <div class="col-md-8">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">All pincodes</h6>
            </div>
            <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Office Name</th>
                      <th>Pincode</th>
                      <th>District</th>
                      <th>RMS</th>
                      <th>Date</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                      
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="card shadow mb-4">
        <div class="card-header">
            <h6 class="m-0 font-weight-bold text-info">Add new pincode</h6>
        </div>
        <div class="card-body">
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <div class="form-group">
                    <label for="office_name">Office name</label>
                    <input class="form-control" type="text" name="office_name" required>
                </div>
                <div class="form-group">
                    <label for="pincode">Pincode</label>
                    <input class="form-control" type="text" name="pincode" required>
                </div>
                <div class="form-group">
                    <label for="district">District</label>
                    <input class="form-control" type="text" name="district" required>
                </div>
                <div class="form-group">
                    <label for="rms">RMS</label>
                    <input class="form-control" type="text" name="rms" required>
                </div>
                <div class="form-group">
                    <button type="submit" name="add" class="btn btn-info"><i class="fa fa-plus fa-fw"></i>Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>



<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable({
    'processing': true,
      'serverSide': true,
      'serverMethod': 'post',
      'ajax': {
          'url':'pincodes_ajax.php'
      },
      'columns': [
         { data: 'id'},
         { data: 'office_name' },
         { data: 'pincode' },
         { data: 'district' },
         { data: 'rms' },
         { data: 'date' },
         { 
           data: 'weblink',
           "render": function(data, type, row, meta){
            if(type === 'display'){
                data = '<a href="pincodes/edit.php?id=' + row.id + '"><i class="fa fa-edit"></i></a>';
            }
            return data;
           }
         
         }
      ]
  });
});
</script>