<?php

include_once('includes/init.php');

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value

## Search 
$searchQuery = " ";
if($searchValue != ''){
   $searchQuery = " and (subscribers.name like '%".$searchValue."%' or 
   subscribers.phone like '%".$searchValue."%' or 
   subscribers.id like'%".$searchValue."%' or subscribers.payment like'%".$searchValue."%') ";
}

## Total number of records without filtering
$totalRecords = count(Subscriber::getActive('print'));;


## Total number of record with filtering
$sel = $db->query("select count(*) as allcount from subscribers LEFT JOIN subscriptions ON subscribers.id = subscriptions.subscriber_id WHERE subscribers.type = 'print' AND subscriptions.status = 1 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];


## Fetch records
$empQuery = "select subscribers.* from subscribers LEFT JOIN subscriptions ON subscribers.id = subscriptions.subscriber_id WHERE subscribers.type = 'print' AND subscriptions.status = 1 ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
$empRecords = $db->query($empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {
    $subscription = Subscription::findBySubscriberId($row['id']);
    if($subscription){
        $ref_no = $subscription->ref_no;
    }else{
        $ref_no ="";
    }
    $data[] = array( 
       "id"=>$row['id'],
       "name"=>$row['name'],
       "phone"=>$row['phone'],
       "date"=>date('d F, Y', strtotime($row['created_at'])),
       "payment"=>$row['payment'],
       "subscription"=>  $row['period'].' '.$row['duration'].' '.$row['type'],
       "rno"=>$ref_no,
    );
 }

 ## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecordwithFilter,
    "iTotalDisplayRecords" => $totalRecords,
    "aaData" => $data
  );

  echo json_encode($response);

?>