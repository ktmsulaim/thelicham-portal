<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>
<?php
    $type = "";
    $message = "";
    $count = "";
    if (isset($_POST["import"])) {
        
        $fileName = $_FILES["file"]["tmp_name"];
        
        if ($_FILES["file"]["size"] > 0) {
            
            $file = fopen($fileName, "r");
            $fp = file($fileName, FILE_SKIP_EMPTY_LINES);
            $count = count($fp) - 1;
            $row = 1;
            while (($column = fgetcsv($file, 10000, ',')) !== FALSE) {
                // put pincode into session variable
                $_SESSION['pincode'] = $column[9];
                // skip the first row for head
                if($row == 1){
                    $row++;
                    continue;
                }


                $subscriber = new Subscriber();
                $subscriber->name = $column[0];
                $subscriber->so = $column[1];
                $subscriber->house = $column[2];
                $subscriber->place = $column[3];
                $subscriber->email = $column[4];
                $subscriber->phone = $column[5];
                $subscriber->rms = $column[6];
                $subscriber->district = $column[7];
                $subscriber->post = $column[8];
                $subscriber->pincode = $column[9];
                $subscriber->type = $column[10];
                $subscriber->duration = $column[11];
                $subscriber->period = $column[12];
                $subscriber->total = $column[13];
                $subscriber->created_at = Database::now();
                $subscriber->payment = $column[14];
                $subscriber->promo = $column[15] ? $column[15] : '';
                
                if ($subscriber->save()) {
                    Subscriber::updateRms($_SESSION['pincode'], $db->last_id());
                    $type = "success";
                    $message = "CSV Data Imported into the Database";
                } else {
                    $type = "error";
                    $message = "Problem in Importing CSV Data";
                }
            }
        }
    }
?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Import subscribers</h1>
</div>


<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Import CSV</h6>
    </div>
    <div class="card-body">
        <?php if($type !== '' || $type == "success"){}else{ ?>
            <div class="my-2">
            <h6>Instuctions</h6>
            <ul>
                <li>File should be on <b>.csv</b> extension</li>
                <li>Starter file can be downloaded from <a href="starter.csv">here</a>.</li>
                <li>
                    <b>Data types:</b><br>
                    <span>Name - string (Eg.Some one)</span><br>
                    <span>So/Do/Co - string (Eg.Another person)</span><br>
                    <span>House - string (Eg.House name)</span><br>
                    <span>Place - string (Eg.Place .....)</span><br>
                    <span>Email - string (Eg.test@thelicham.com)</span><br>
                    <span>Phone - number (Eg.(+9x)1234567890)</span><br>
                    <span>Rms - string - Not required (Leave it blank)</span><br>
                    <span>District - string (Eg.Malappuram)</span><br>
                    <span>Post - string (Eg.Post office)</span><br>
                    <span>Pincode - number (Eg.678909)</span><br>
                    <span>Type - string - (Print | Digital)</span><br>
                    <span>Duration - string - (Year | Month)</span><br>
                    <span>Period - number - (Number of years/months)</span><br>
                    <span>Total - number - (Calculated price)</span><br>
                    <span>Payment - number - (<b>1</b> = Payed | <b>0</b> = Not payed)</span><br>
                    <span>Promo - string - (If any or leave it blank)</span><br>
                </li>
            </ul>
        </div>
        <?php }?>
        <form class="form-horizontal" action="" method="post" name="uploadCSV" enctype="multipart/form-data">
            <div class="input-row">
                <label class="col-md-4 control-label">Choose CSV File</label> <input
                    type="file" name="file" id="file" accept=".csv" required>
                    <input type="hidden" name="import">
                <button type="submit" id="submit" name="import"
                    class="btn btn-primary">Import</button>
                <br />

            </div>
            <div id="labelError"></div>
        </form>
        <div id="imported" class="mt-4">
            <div class="status">
                <?php if($type == "success") : ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Success!</strong> The subscribers have been imported. To add subscription click <a href="subscription.php">here</a>
                    </div>
                <?php endif; ?>
                <?php if($type == "error") : ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Error!</strong> Sorry there was an error while importing subscribers.
                    </div>
                <?php endif; ?>
            </div>
            <?php if(!empty($type) && $type = "success") { 
                // get inserted subscribers
                $subscribers = Subscriber::query("SELECT * FROM subscribers ORDER BY id DESC LIMIT {$count}");
                ?>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Place</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Plan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($subscribers) > 0) : 
                            $i = 1;
                            foreach($subscribers as $subscriber) :    
                        ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $subscriber->name; ?></td>
                                <td><?php echo $subscriber->place; ?></td>
                                <td><?php echo $subscriber->phone; ?></td>
                                <td><?php echo $subscriber->email; ?></td>
                                <td><?php echo $subscriber->period.' '.$subscriber->duration.' '.$subscriber->type; ?></td>
                            </tr>
                        <?php 
                            $i++;
                        endforeach;
                    endif; ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>

</div>

<!-- /.container-fluid -->

</div>

<?php include('includes/footer.php'); ?>
<script type="text/javascript">
	// $(document).ready(
	// function() {
	// 	$("form").on(
	// 	"submit",
	// 	function() {

	// 		$("#response").attr("class", "");
	// 		$("#response").html("");
	// 		var fileType = ".csv";
	// 		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+("
	// 				+ fileType + ")$");
	// 		if (!regex.test($("#file").val().toLowerCase())) {
	// 			$("#response").addClass("error");
	// 			$("#response").addClass("display-block");
	// 			$("#response").html(
	// 					"Invalid File. Upload : <b>" + fileType
	// 							+ "</b> Files.");
	// 			return false;
	// 		}
	// 		return true;
	// 	});
	// });
</script>