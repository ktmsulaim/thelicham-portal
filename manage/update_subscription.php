<?php

require_once('includes/init.php');


// update subscription
if(isset($_POST['update']) || isset($_POST['pwd'])){
    //user verification
    $user = User::findById($_SESSION['user_id']);
    $pass = $user->password;

    if($_POST['pwd'] === $pass){
        $id = $_POST['id'];
        $ref_no = $_POST['ref_no'];
        $period = $_POST['period'];
        $duration = $_POST['duration'];
        $type = $_POST['type'];
        $total = $_POST['total'];

        $subscriber = Subscriber::findById($id);
        $subscriber->period = $period;
        $subscriber->duration = $duration;
        $subscriber->type = $type;
        $subscriber->total = $total;
        $subscriber->update();

        if(Subscription::makePayment($id)){
            if(Subscription::updateSubscription($id, $ref_no, 1, $period, $duration)){
                redirect('subscribers/view.php?id='.$id);
            }else{
                redirect($_SERVER['PHP_SELF'].'?failed=subscription');
            }
        }
    }else{
        redirect($_SERVER['PHP_SELF'].'?failed=pwd');
    }
}