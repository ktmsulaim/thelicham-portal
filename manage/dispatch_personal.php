<?php
    require_once('includes/init.php');

        // personal dispatch
    if(isset($_GET['issue'])){
        $issue = $_GET['issue'];
        $subscriber = $_GET['subscriber'];
    
        if(Subscription::dispatch($subscriber)){
            $dispatch = new Dispatch();
            $dispatch->subscriber_id = $subscriber;
            $dispatch->issue_id = $issue;
            $dispatch->type = 'personal';
            $dispatch->date = $db->now();
    
            if($dispatch->save()){
                Subscription::checkSubscriptionById($subscriber);
                redirect("dispatch.php?success");
            }else{
                redirect("dispatch.php?failed");
            }
        }else{
            redirect("dispatch.php?failed");
        }
    }
?>