<?php

class Institution {
    public $name;
    public $place;
    public $phone;
    public $email;
    public $promo;

    public static function query($sql){
        global $db;

        $results = $db->query($sql);
        $objArray = array();

        while($row = mysqli_fetch_array($results)){
            $objArray[] = self::instance($row);
        }

        return $objArray;
    }

    public static function all(){
        global $db;
        return self::query("SELECT * FROM institutions");
    }

    public static function findById($id){
        global $db;
        return self::query("SELECT * FROM institutions WHERE id = {$id}");
    }

    public static function instance($row){
        $obj = new self;

        foreach($row as $rowColumn => $value){
            if($obj->hasColumn($rowColumn)){
                $obj->$rowColumn = $value;
            }
        }

        return $obj;
    }

    private function hasColumn($rowColumn){
        $object = get_object_vars($this);

        return array_key_exists($rowColumn, $object);
    }

    public function update(){
        global $db;

        $id = $this->id;
        $name = $db->escapeString($this->name);
        $place = $db->escapeString($this->place);
        $phone = $db->escapeString($this->phone);
        $email = $db->escapeString($this->email);


        $sql = "UPDATE institutions SET name = '{$name}', place = '{$place}', phone = '{$phone}', email = '{$email}' WHERE id = {$id}";

        $db->query($sql);

        if(mysqli_affected_rows($db->connection) == 1){
            return true;
        }else{
            return false;
        }
    }


} // end of class

?>