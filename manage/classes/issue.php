<?php

class Issue {
    public $id;
    public $cover;
    public $month;
    public $year;
    public $created_at;
    public $issue_id;

    public static function query($sql){
        global $db;

        $results = $db->query($sql);
        $objectArray = array();

        while($row = mysqli_fetch_array($results)){
            $objectArray[] = self::instance($row);
        }

        return $objectArray;
    }

    public static function all(){
        global $db;
        return self::query("SELECT * FROM issues");
    }
    
    public static function findById($id){
        global $db;
        $result = self::query("SELECT * FROM issues WHERE id=". $id);

        return !empty($result) ? array_shift($result) : false;
    }

    public static function instance($user){
        $obj = new self;
        
        foreach($user as $userColumn => $value){
            if($obj->hasColumn($userColumn)){
                $obj->$userColumn = $value;
            }
        }
        
        return $obj;
    }


    private function hasColumn($userColumn){
        $object = get_object_vars($this);
        
        return array_key_exists($userColumn, $object);
    }

    public function create(){
        global $db;

        date_default_timezone_set('Asia/Kolkata');
        $date = date('Y-m-d h:i:s');

        $cover = $db->escapeString($this->cover);
        $month = $db->escapeString($this->month);
        $year = $db->escapeString($this->year);

        $sql = "INSERT INTO issues (cover, month, year, created_at) ";
        $sql .= "VALUES('{$cover}', '{$month}', '{$year}', '{$date}')";

        if($db->query($sql)){
            return true;
        }else{
            return false;
        }
    }

    public function update(){
        global $db;

        date_default_timezone_set('Asia/Kolkata');
        $date = date('Y-m-d h:i:s');

        $cover = $db->escapeString($this->cover);
        $month = $db->escapeString($this->month);
        $year = $db->escapeString($this->year);

        $sql = "UPDATE issues SET cover='{$cover}', month='{$month}', year='{$year}', created_at = '{$date}' WHERE id= {$this->id}";

        $db->query($sql);
        
        return mysqli_affected_rows($db->connection) == 1 ? true : false;

    }

    public function delete(){
        global $db;

        $db->query("DELETE FROM issues WHERE id={$this->id}");

        return mysqli_affected_rows($db->connection) == 1 ? true : false;
    }

    public static function makeCurrent($id){
        global $db;

        $db->query("UPDATE current_issue SET issue_id = {$id}");

        return mysqli_affected_rows($db->connection) == 1 ? true : false;
    }

    public static function getCurrent(){
        global $db;

        $result = self::query("SELECT * FROM current_issue");

        $resultNew = array_shift($result);

        return  self::findById($resultNew->issue_id);

    }





} // End of class

?>