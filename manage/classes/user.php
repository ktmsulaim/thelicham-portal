<?php
    

class User extends Model{
    
        public $id;
        public $username;
        public $password;
        public $email;
        public $first_name;
        public $last_name;
        public $is_admin;
        public $created_at;
        public $updated_at;
    
    
        protected static $db_table = "users";
        protected static $db_table_fields = array('username', 'password', 'email', 'first_name', 'last_name', 'is_admin', 'created_at', 'updated_at');

        
        public static function verifyUser($username, $password){
            global $db;
            
            $username = $db->escapeString($username);
            $password = $db->escapeString($password);
            
            $sql = "SELECT * FROM users WHERE username = '{$username}' AND password = '{$password}' LIMIT 1";
            
            $user = self::query($sql);
            
            return !empty($user) ? array_shift($user) : false;
        }    
    
    

}
    
?>