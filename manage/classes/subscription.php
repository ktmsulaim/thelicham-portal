<?php

class Subscription extends Model {
    public $id;
    public $subscriber_id;
    public $method_id;
    public $ref_no;
    public $payment_date;
    public $subscription_date;
    public $expiry_date;
    public $credits;
    public $credits_left;
    public $status = 0;

    protected static $db_table = "subscriptions";
    protected static $db_table_fields = array('subscriber_id', 'method_id', 'ref_no', 'payment_date', 'subscription_date', 'expiry_date', 'credits', 'credits_left', 'status');

    // function __construct()
    // {
    //     self::checkSubscription();
    //     self::removeSubscriptionNotPayed();
    // }


    public static function findBySubscriberId($subId){
        $result = self::query("SELECT * FROM ".self::$db_table." WHERE subscriber_id = {$subId}");

        return !empty($result) ? array_shift($result) : false;
    }

    public static function getBySubscriberId($subId){
        return self::query("SELECT * FROM ".self::$db_table." WHERE subscriber_id = {$subId}");
    }
    
    //update payment info
    public static function makePayment($subId){
        global $db;

        if($db->query("UPDATE subscribers SET payment = 1 WHERE id = {$subId}")){
            return true;
        }else{
            return false;
        }
    }


    //add subscription
    public static function addSubscription($subId, $ref_no, $method_id, $period, $duration){

        date_default_timezone_set('Asia/Kolkata');

        $timestamp = Database::now(); 

        $now = date('Y-m-d', strtotime('now'));

        $credits = 0;
        $status = 0;

        $date = new DateTime($now);

        $newDate = date_format($date, 'Y-m-d');

        $diff =  $period." ".$duration;

        $expiry_date = date_add($date, date_interval_create_from_date_string($diff));
        
        $exp_date = date_format($expiry_date, 'Y-m-d');

        $newDate1 = new DateTime($newDate);
        $newExp = new DateTime($exp_date);
        $interval = $newDate1->diff($newExp);
        $days = $interval->days;
        $months = floor($days / 30);

        $credits = $months;

        if($credits >= 1){
            $status = 1;
        }

    
        $subscription = new self;
        $subscription->subscriber_id = $subId;
        $subscription->method_id = $method_id;
        $subscription->ref_no = $ref_no;
        $subscription->payment_date = $timestamp;
        $subscription->subscription_date = $newDate;
        $subscription->expiry_date = $exp_date;
        $subscription->credits = $credits;
        $subscription->credits_left = $credits;
        $subscription->status = $status;

        
        if($subscription->create()){
            self::makePayment($subId);
            return true;
        }else{
            return false;
        }
        
    }
    


    public static function updateSubscription($subId, $ref_no, $method_id, $period, $duration){
        global $db;
        date_default_timezone_set('Asia/Kolkata');

        $timestamp = Database::now(); 
        $now = date('Y-m-d', strtotime('now'));

        $credits = 0;
        $status = 0;

        $date = new DateTime($now);

        $newDate = date_format($date, 'Y-m-d');

        $diff =  $period." ".$duration;

        $expiry_date = date_add($date, date_interval_create_from_date_string($diff));
        
        $exp_date = date_format($expiry_date, 'Y-m-d');

        $newDate1 = new DateTime($newDate);
        $newExp = new DateTime($exp_date);
        $interval = $newDate1->diff($newExp);
        $days = $interval->days;
        $months = floor($days / 30);

        $credits = $months;

        if($credits >= 1){
            $status = 1;
        }

        $subscriptionId = self::getSubscriptionId($subId);

        $subscription = self::findById($subscriptionId->id);
        $subscription->subscriber_id = $subId;
        $subscription->method_id = $method_id;
        $subscription->ref_no = $ref_no;
        $subscription->payment_date = $timestamp;
        $subscription->subscription_date = $newDate;
        $subscription->expiry_date = $exp_date;
        $subscription->credits = $credits;
        $subscription->credits_left = $credits;
        $subscription->status = $status;

        $subscription->update();

        return mysqli_affected_rows($db->connection) == 1 ? true : false;
    }

    public static function getSubscriptionId($subId){
        global $db;
        $result = self::query("SELECT id FROM subscriptions WHERE subscriber_id = {$subId}");

        return !empty($result) ? array_shift($result) : false;
    }


    public static function hasSubscription($subId){
        global $db;

        $results = $db->query("SELECT * FROM subscriptions WHERE subscriber_id = {$subId} LIMIT 1");

        if($results->num_rows > 0){
            return true;
        }else{
            return false;
        }
    }

    public static function removeSubscription($subid){
        global $db;

        $sql = "DELETE FROM subscriptions WHERE subscriber_id = {$subid}";

        if($db->query($sql)){
            return true;
        }else{
            return false;
        }
    }

    public static function getSubscription($subId){
        global $db;

        $sql = "SELECT * FROM subscriptions WHERE subscriber_id = {$subId}";

        $results = $db->query($sql);

        return $results;
    }

    public static function checkSubscriptionById($subId){
        global $db;

        $results = $db->query("SELECT * FROM subscriptions WHERE subscriber_id = {$subId}");

        if($results->num_rows == 1){
            while($row = mysqli_fetch_array($results)){
                $credits_left = $row['credits_left'];
            }

            if($credits_left >= 1){
                $db->query("UPDATE subscriptions SET status = 1 WHERE subscriber_id = {$subId}");

                if(mysqli_affected_rows($db->connection) == 1){
                    return true;
                }else{
                    return false;
                }
            }else{
                $db->query("UPDATE subscriptions SET status = 0 WHERE subscriber_id = {$subId}");

                if(mysqli_affected_rows($db->connection) == 1){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }

    private static function checkSubscription(){
        global $db;
        $subscribers = $db->query("SELECT * FROM subscribers");

        foreach($subscribers as $subscriber){
            $subId = $subscriber['id'];

            self::checkSubscriptionById($subId);
        }
    }

    public static function checkPaymentById($subId){
        global $db;
        $result = $db->query("SELECT payment FROM subscribers WHERE id = {$subId}");

        if($result->num_rows >= 1){
            while($row = mysqli_fetch_array($result)){
                $payment = $row['payment'];
            }

            return $payment;
        }else{
            return false;
        }
    }

    public static function removeSubscriptionNotPayed(){
        global $db;

        $subscribers = Subscriber::all();

        foreach($subscribers as $subscriber){
          $id = $subscriber->id;
          $payment = $subscriber->payment;
        }
        
        if($payment == 0){
            if($db->query("DELETE FROM subscriptions WHERE subscriber_id = {$id}")){
              return true;
            }else{
              return false;
            }
          }
    }

    // all payment methods
    public static function paymentMethods(){
        global $db;

        $sql = "SELECT * FROM payment_methods";
        $results = $db->query($sql);
        return $results;
    }

    public static function getMethod($subId){
        global $db;

        $results = $db->query("SELECT m.name FROM payment_methods m INNER JOIN subscriptions s ON s.method_id = m.id WHERE s.subscriber_id = {$subId}");

        return $results;
    }

    public static function totalAmount(){
        global $db;

        $sql = "SELECT SUM(sb.total) as total FROM subscribers sb INNER JOIN subscriptions sc ON sb.id = sc.subscriber_id WHERE sc.status = 1 AND sb.payment = 1";

        $result = $db->query($sql);
        return $result;
    }

    // dispatching 
    public static function dispatch($subId){

        $subscription = self::findBySubscriberId($subId);
        
        if($subscription){
            $credits_left = $subscription->credits_left;
        }else{
            return false;
        }

        if($credits_left > 0){
            $credits_left--;

        //udpate into the database
            $subscription->credits_left = $credits_left;
            if($subscription->update()){
                return true;
            }else{
                return false;
            }

        }else{
            false;
        }

    }

    // public static function groupDispatch($issue_id){
    //     global $db;

    //     $subscribers = Subscriber::getActive('print');
    //     if($subscribers && count($subscribers) > 0){
    //         // dispatch
    //         foreach($subscribers as $subscriber){
    //             if(self::dispatch($subscriber->id)){
    //                 $dispatch = new Dispatch();
    //                 $dispatch->subscriber_id = $subscriber->id;
    //                 $dispatch->issue_id = $issue_id;
    //                 $dispatch->type = 'group';
    //                 $dispatch->date = $db->now();
    //                 if($dispatch->save()){
    //                     return true;
    //                 }else{
    //                     return false;
    //                 }
    //             }else{
    //                 return false;
    //             }
    //         }
    //     }
    // }

    public static function updateCredits($subId, $credits){
        global $db;

        $db->query("UPDATE subscriptions SET credits_left = {$credits} WHERE subscriber_id = {$subId}");

        return mysqli_affected_rows($db->connection) == 1 ? true : false;

    }

    // public static function dispatchLog($subId, $issueId, $type){
    //     global $db;

    //     $date = $db->now();

    //     $dispatch = new Dispatch();
    //     $dispatch->subscriber_id = $subId;
    //     $dispatch->issue_id = $issueId;
    //     $dispatch->type = $type;
    //     $dispatch->date = $date;

    //     if($dispatch->save()){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }

    public static function getDispatchLog(){
        return Dispatch::all();
    }

    public static function withdrawDispatch($did, $subId){
        global $db;
        // delete from dispatch log
        $dispatch = Dispatch::findById($did);
        if($dispatch->delete()){
            //re-fill withdrawn issues as credits back to subscriber
            // get credits left
            $subscription = self::getSubscription($subId);
            foreach($subscription as $credits){
                $credits_left = $credits['credits_left'];
                $credit = $credits['credits'];
            }
            
            if($credit > $credits_left){
                $credits_left++;
            }
            
            //update new credit value to db
            if(self::updateCredits($subId, $credits_left)){
                self::checkSubscriptionById($subId);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    //get last date of dispatch log
    public static function getDispatchLogDate($issue_id, $type){
        global $db;
        $sql= "SELECT date FROM dispatch_history WHERE issue_id = {$issue_id} AND type= '{$type}' ORDER BY date DESC LIMIT 1";
        $result = $db->query($sql);
        if($result->num_rows == 1){
            foreach($result as $date){
                $date = $date['date'];
            }
            return $date;
        }
    }


} // End of class Subscription

// $subscription = new Subscription();

?>