<?php

class Dispatch extends Model {
    public $id;
    public $subscriber_id;
    public $issue_id;
    public $type;
    public $date;

    protected static $db_table = "dispatch_history";
    protected static $db_table_fields = array('subscriber_id', 'issue_id', 'type', 'date');


    public static function getByType($type){
        return self::query("SELECT * FROM ".self::$db_table." WHERE type = '".$type."'");
    }

    public static function getGroupLog(){
        return self::query("SELECT DISTINCT issue_id FROM ".self::$db_table." WHERE type = 'group' GROUP BY issue_id");
    }

}

?>