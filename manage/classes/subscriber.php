<?php

class Subscriber extends Model{

    public $id;
    public $name;
    public $so;
    public $house;
    public $place;
    public $email;
    public $phone;
    public $rms;
    public $district;
    public $post;
    public $pincode;
    public $type;
    public $duration;
    public $period;
    public $total;
    public $created_at;
    public $payment;
    public $promo;

    // subscription
    public $subscriber_id;
    public $method_id;
    public $payment_date;
    public $subscription_date;
    public $expiry_date;
    public $credits;
    public $credits_left;
    public $status = 0;

    protected static $db_table = "subscribers";
    protected static $db_table_fields = array('name', 'so', 'house', 'place', 'email', 'phone', 'rms', 'district', 'post', 'pincode', 'type', 'duration', 'period', 'total', 'payment', 'promo', 'created_at');

  

    public static function allPrtSubscribers(){
        global $db;
        return self::query("SELECT * FROM subscribers WHERE type = 'print'");
    }

    public static function allDgtSubscribers(){
        global $db;
        return self::query("SELECT * FROM subscribers WHERE type = 'digital'");
    }

    public static function getActive($type){
        $results = self::query("SELECT s.*, sb.id as subscription_id, sb.method_id, sb.tax_id, sb.payment_date, sb.subscription_date, sb.expiry_date, sb.credits, sb.credits_left, sb.status FROM subscribers s LEFT JOIN subscriptions sb ON s.id = sb.subscriber_id WHERE sb.status = 1 AND s.type = '{$type}'");
        return $results;
    }

    public static function search($q){
        $results = self::query("SELECT * FROM subscribers WHERE type = 'print' AND name LIKE '%{$q}%' OR email LIKE '%{$q}%'");
        return $results;
    }




    public static function verifyEmail($email){
        global $db;

        $sql = "SELECT * FROM subscribers WHERE email = '{$email}'";
        $results = $db->query($sql);
        
        if($results->num_rows > 1){
            return false;
        }else{
            return true;
        }
    }




    public static function updateRms($pincode, $id){
        global $db;

        $sql = "SELECT * FROM pincodes WHERE pincode = {$pincode}";

        $results = $db->query($sql);

        if($results->num_rows >= 1){
            while($row = mysqli_fetch_array($results)){
                $rms = $row['rms'];
            }

            
    
            $update = "UPDATE subscribers SET rms = '{$rms}' WHERE id = {$id}";
    
            $db->query($update);
    
            if(mysqli_affected_rows($db->connection) == 1){
                return true;
            }else{
                return false;
            }
        }else{

            $rms = "NULL";

            $update = "UPDATE subscribers SET rms = '{$rms}' WHERE id = {$id}";
    
            $db->query($update);
    
            if(mysqli_affected_rows($db->connection) == 1){
                return true;
            }else{
                return false;
            }
        }

    }


    public static function hasNoSubscription()
    {
        global $db;
        $result = $db->query("SELECT subscribers.* FROM subscribers LEFT OUTER JOIN subscriptions ON (subscribers.id = subscriptions.subscriber_id) WHERE subscribers.payment = 1 AND subscriptions.subscriber_id IS NULL");
        if($result){
            return $result;
        }
    }

} // End of class subscriber

?>