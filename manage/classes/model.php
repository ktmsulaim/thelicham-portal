<?php

class Model {
    protected static $db_table;
    protected static $db_table_fields;
    public $type;
    public $size;
    public $tmp_name;
    public $filename;
    public $directory = "images";
    public $custom_errors = array();
    public $upload_errors = array(
        UPLOAD_ERR_OK => 'There is no error, the file uploaded with success.',
        UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
        UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
        UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
        UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
        UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
        UPLOAD_ERR_CANT_WRITE => 'Cannot write to target directory. Please fix CHMOD.',
        UPLOAD_ERR_EXTENSION => 'A PHP extension stopped the file upload.'
    );
    public $extensions = array('jpg', 'jpeg', 'png', 'gif');

    public function changeFilename($file){
        $temp = explode(".", $file["name"]);
        $newfilename = round(microtime(true)) . '.' . end($temp);
        
        return $newfilename;
    }

    public function setFile($file){
        if(empty($file) || !$file || !is_array($file)){
            $this->custom_errors[] = "There was no file to upload";
            return false;
        }elseif($file['error'] !=0){
            $this->custom_errors[] = $this->upload_errors[$file['error']];
        }else{
            $this->filename = $this->changeFilename($file);
            $this->tmp_name = $file['tmp_name'];
            $this->type = $file['type'];
            $this->size = $file['size'];
        }
    }

    public function upload_photo(){    
            //validation
            if(!empty($this->custom_errors)){
                return false;
            }
            
            if(empty($this->filename) || empty($this->tmp_name)){
                $this->custom_errors[] = "The file not available";
                return false;
            }

            $filext = pathinfo($this->filename, PATHINFO_EXTENSION);
            if(!in_array($filext, $this->extensions)){
                $this->custom_errors[] = "The extension is not supported";
                return false;
            }
            
            $target_path = SITE_ROOT . DS . 'admin' . DS . $this->directory . DS . $this->filename;
            
            if(file_exists($target_path)){
                $this->custom_errors[] = "The file {$target_path} is already existing";
                return false;
            }
            
            if(move_uploaded_file($this->tmp_name, $target_path)){
                    unset($this->tmp_name);
                    return true;
            }
        }
    




        public static function query($sql){
            global $db;
            
            $results = $db->query($sql);
            $objectArray = array();
            
            while($row = mysqli_fetch_array($results)){
                $objectArray[] = static::instance($row);
            }
            return $objectArray;
        }
    
    
        public static function all(){
            
            global $db;
            
            return static::query("SELECT * FROM ". static::$db_table);
        }
    
    
        public static function findById($id){
            $user = static::query("SELECT * FROM ".static::$db_table." WHERE id=".$id." LIMIT 1");
            return !empty($user) ? array_shift($user) : false;
        }
    
        public static function instance($user){
            $calling_class = get_called_class();
            $obj = new $calling_class;
            
            foreach($user as $userColumn => $value){
                if($obj->hasColumn($userColumn)){
                    $obj->$userColumn = $value;
                }
            }
            
            return $obj;
        }
    
    
        private function hasColumn($userColumn){
            $object = get_object_vars($this);
            
            return array_key_exists($userColumn, $object);
        }
    
        private function properties(){
            
            $properties = array();
            
            foreach(static::$db_table_fields as $db_field){
                if(property_exists($this, $db_field)){
                    $properties[$db_field] = $this->$db_field;
                }
            }
            
            return $properties;
        }
    
        protected function cleanProps(){
            global $db;
            
            $clean_props = array();
            
            foreach($this->properties() as $key => $value){
                $clean_props[$key] = $db->escapeString($value);
            }
            
            return $clean_props;
        }
    
    
        public function save(){
            return isset($this->id) ? $this->update() : $this->create();
        }
        
    
        public function create(){
            global $db;
            
            $properties = $this->cleanProps();
            
            
            $sql = "INSERT INTO ".static::$db_table." ( ".implode(",", array_keys($properties))." ) ";
            $sql .= "VALUES ('".implode("','", array_values($properties))."')";
            
            if($db->query($sql)){
                $this->id = $db->last_id();
                return true;
            }else{
                return false;
            }
        }
    
        public function update(){
            global $db;
            
            $properties = $this->cleanProps();
            $prop_pair = array();
            
            foreach($properties as $key => $value){
                $prop_pair[] = "{$key}='{$value}'";
            }
            
            
            $sql = "UPDATE ".static::$db_table." SET ";
            $sql .= implode(", ", $prop_pair);
            $sql .= " WHERE id = {$this->id}";
            
            $db->query($sql);
            
            if(mysqli_affected_rows($db->connection) == 1){
                return true;
            }else{
                return false;
            }
            
        }
    
        public function delete(){
            global $db;
            
            $sql = "DELETE FROM ".static::$db_table." WHERE id=" . $db->escapeString($this->id);
            $sql .= " LIMIT 1";
            
            $db->query($sql);
            
            return mysqli_affected_rows($db->connection) == 1 ? true : false;
        }
    

    public static function totalRows(){
        global $db;
        $result = $db->query("SELECT COUNT(*) as total FROM ".static::$db_table);
        foreach($result as $total){
            $total = $total['total'];
        }
        return $total;
    }

    public static function paginate($offset, $records_per_page){
        return self::query("SELECT * FROM ".static::$db_table." LIMIT {$offset}, {$records_per_page}");
    }

    public static function getCount(){
        global $db;
        $result = $db->query("SELECT COUNT(*) as total FROM ".static::$db_table);
        foreach($result as $count){
            $total = $count['total'];
        }
        return $total;
    }
    
} // End of class

?>