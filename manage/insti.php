<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
  <?php

if(isset($_GET['added'])){
  echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> The institution has been added.
</div>';
}
if(isset($_GET['updated'])){
  echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> The institution has been updated.
</div>';
}
if(isset($_GET['deleted'])){
  echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> The institution has been deleted.
</div>';
}

?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Institutions</h1>
  <a href="addInst.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add</a>
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->

<!-- Content Row -->
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">All institutions</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Place</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Promo Code</th>
                      <th>Total Subscribers</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tfoot>
                  <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Place</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Promo Code</th>
                      <th>Total Subscribers</th>
                      <th></th>
                    </tr>
                  </tfoot>
                  <tbody>

                  <?php
                    $results = $db->query("SELECT * FROM institutions");
                    $i = 1;
                    while($row = mysqli_fetch_array($results)){
                        $promo = $row['promo'];
                        $getCount = $db->query("SELECT * FROM subscribers WHERE promo = '{$promo}'");

                        echo '<tr>
                                <td>'.$i.'</td>
                                <td>'.$row['name'].'</td>
                                <td>'.$row['place'].'</td>
                                <td>'.$row['phone'].'</td>
                                <td>'.$row['email'].'</td>
                                <td>'.$row['promo'].'</td>
                                <td class="text-center"><span class="badge badge-primary">'.$getCount->num_rows.'</span></td>
                                <td><a href="editInst.php?id='.$row['id'].'" title="Edit'.$row['name'].'"><i class="fa fa-edit"></i></a></td>
                            </tr>';

                            $i++;
                    }

                    ?>
                    
                  </tbody>
                </table>
              </div>
            </div>
</div>


<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('includes/footer.php'); ?>

<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>