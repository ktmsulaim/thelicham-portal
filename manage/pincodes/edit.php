<?php include('../includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("../login.php"); } ?>
<?php


 if(isset($_GET['id'])){
    $_SESSION['pid'] = $_GET['id'];
    $id = $_GET['id'];
    $pincode = Pincode::findById($id);
    $office_name = $pincode->office_name;
    $pincode1 = $pincode->pincode;
    $district = $pincode->district;
    $rms = $pincode->rms;
}else{
    redirect('http://'.$_SERVER['HTTP_HOST'].'/manage/pincodes.php');
}

if(isset($_POST['update']) || isset($_POST['pincode'])){
  $pincode = Pincode::findById($_SESSION['pid']);
  $pincode->office_name = trim($_POST['office_name']);
  $pincode->pincode = trim($_POST['pincode']);
  $pincode->district = trim($_POST['district']);
  $pincode->rms = trim($_POST['rms']);

  if($pincode->update()){
    redirect("http://".$_SERVER['HTTP_HOST']."/manage/pincodes.php?updated");
  }else{
    header('Location: http://'.$_SERVER['HTTP_HOST'].'/manage/pincodes/edit.php?id='.$id);
  }
}




?>

<?php include('../includes/sidebar.php'); ?>

<?php include('../includes/topnav.php'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">


<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Edit Pincode</h1>
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->


<!-- Content Row -->

    <div class="row">
        <div class="col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Edit <?php echo $pincode->pincode; ?></h6>
                </div>
                <div class="card-body">
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                        <div class="form-group">
                            <label for="office_name">Office name</label>
                            <input class="form-control" type="text" name="office_name" value="<?php echo $office_name; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="pincode">Pincode</label>
                            <input class="form-control" type="text" name="pincode" value="<?php echo $pincode1; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="district">District</label>
                            <input class="form-control" type="text" name="district" value="<?php echo $district; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="rms">RMS</label>
                            <input class="form-control" type="text" name="rms" value="<?php echo $rms; ?>" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="update" class="btn btn-info"><i class="fa fa-paper-plane fa-fw mr-2"></i>Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-danger">Delete</h6>
                </div>
                <div class="card-body">
                    <p>Do you want to delete this pincode?</p>
                    <span title="Delete" class="float-right btn btn-danger" style="cursor:pointer" role="button" data-toggle="modal" data-target="#deleteModal">Proceed</span>
                </div>
            </div>
        </div>
    </div>


<!-- Subscriber delete confirmation Modal-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
              <h5 class="modal-title" id="promoModalLabel">Are sure?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
              </div>
              <div class="modal-body">You are going to delete this pincode. You cannot undo after deleted.<br>
              </div>
              <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

              <form action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/pincodes/delete.php" method="post">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="delete">
                <button type="submit" role="submit" name="delete" class="btn btn-danger">Delete</button>
              </form>

              </div>
          </div>
          </div>
      </div>



<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('../includes/footer.php'); ?>