<?php

include_once('includes/init.php');

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value

## Search 
$searchQuery = " ";
if($searchValue != ''){
   $searchQuery = " and (office_name like '%".$searchValue."%' or 
        pincode like '%".$searchValue."%' or 
        district like'%".$searchValue."%' or rms like'%".$searchValue."%') ";
}

## Total number of records without filtering
$sel = $db->query("SELECT COUNT(*) as allcount FROM pincodes");
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];


## Total number of record with filtering
$sel = $db->query("select count(*) as allcount from pincodes WHERE 1 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];


## Fetch records
$empQuery = "select * from pincodes WHERE 1 ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
$empRecords = $db->query($empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {
    $data[] = array( 
       "id"=>$row['id'],
       "office_name"=>$row['office_name'],
       "pincode"=>$row['pincode'],
       "district"=>$row['district'],
       "rms"=>$row['rms'],
       "date"=>  date('d-m-Y',strtotime($row['created_at']))
    );
 }

 ## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecordwithFilter,
    "iTotalDisplayRecords" => $totalRecords,
    "aaData" => $data
  );

  echo json_encode($response);

?>