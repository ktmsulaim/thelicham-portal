<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } 

  $count = 0;

  if(isset($_GET['active'])){
    $count = $db->getTotal("FROM subscribers LEFT JOIN subscriptions ON subscribers.id = subscriptions.subscriber_id WHERE subscribers.type = 'print' AND subscriptions.status = 1");
  }elseif(isset($_GET['q'])){
    $search_result = Subscriber::search($_GET['q']);
    $count = count($search_result);
  }else{
    $count = $db->getTotal("FROM subscribers WHERE type = 'print'");
  }
?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

<?php
if(isset($_GET['updated'])){
  echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> The subscriber has been updated.
</div>';
}
if(isset($_GET['deleted'])){
  echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> The subscriber has been deleted.
</div>';
}

?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Subscribers</h1>
  <div class="d-none d-sm-inline-block">
    <a href="subscribers/create.php" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add</a>
    <a href="import.php" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-file-import fa-sm text-white-50"></i> Import</a>
    <a href="dumpSubs.php" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
  </div>
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->


<!-- Content Row -->
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php if(isset($_GET['active'])){echo 'Active subscribers (Print) ';}elseif(isset($_GET['q'])){echo 'Search result';}else{echo 'All subscribers (Print) ';} ?> <span class="badge badge-primary"><?php echo $count; ?></span></h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th title="Receipt number">R.No</th>
                      <th>Name</th>
                      <th>Phone</th>
                      <th>Subscription</th>
                      <th>Date</th>
                      <th>Payment</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(isset($_GET['q'])){
                        $q = $_GET['q'];
                        $subscribers = Subscriber::search($q);
                        if(count($subscribers) >= 1){
                          $i = 1;
                          foreach($subscribers as $subscriber){
                              $subscription = Subscription::findBySubscriberId($subscriber->id);
                              if($subscription){
                                $ref_no = $subscription->ref_no;
                              }else{
                                $ref_no ="";
                              }
                            ?>
                          <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $ref_no; ?></td>
                            <td><?php echo $subscriber->name ?></td>
                            <td><?php echo $subscriber->phone ?></td>
                            <td><span class="badge badge-primary"><?php echo ucfirst($subscriber->type) ?> </span> <?php echo $subscriber->period ." "; ?> <?php echo $subscriber->period > 1 ? $subscriber->duration."s" : $subscriber->duration ?></td>
                            <td class="small"><?php echo date('d F, Y', strtotime($subscriber->created_at)); ?></td>
                            <td><?php echo $subscriber->payment == 0 ? '<span class="badge badge-danger">Not Payed</span>' : '<span class="badge badge-success">Payed</span>' ?></td>
                            <td><a title="View <?php echo $subscriber->name ?>" href="subscribers/view.php?id=<?php echo $subscriber->id; ?>" class="icon-circle bg-primary"><i class="fa fa-eye text-white"></i></a></td>
                            <td><a title="Edit <?php echo $subscriber->name ?>" href="subscribers/edit.php?id=<?php echo $subscriber->id; ?>" class="icon-circle bg-info"><i class="fa fa-edit text-white"></i></a></td>
                          </tr>
                     <?php   $i++;  }
                        }
                      }else{
                          redirect($_SERVER['HTTP_HOST'].'/manage/index.php');
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
</div>


<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>