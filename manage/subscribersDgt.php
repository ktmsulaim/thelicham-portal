<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } 


$count = 0;

if(isset($_GET['active'])){
  $count = $db->getTotal("FROM subscribers LEFT JOIN subscriptions ON subscribers.id = subscriptions.subscriber_id WHERE subscribers.type = 'digital' AND subscriptions.status = 1");
}else{
  $count = $db->getTotal("FROM subscribers WHERE type = 'digital'");
}
?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

<?php
if(isset($_GET['updated'])){
  echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> The subscriber has been updated.
</div>';
}

if(isset($_GET['deleted'])){
  echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> The subscriber has been deleted.
</div>';
}

?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Subscribers</h1>
  <a href="subscribers/create.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add</a>
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->


<!-- Content Row -->
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">All subscribers (Digital) <span class="badge badge-primary"><?php echo $count; ?></span></h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Subscription</th>
                      <th>Date</th>
                      <th>Payment</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Subscription</th>
                      <th>Date</th>
                      <th>Payment</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php 
                    if(isset($_GET['active'])){
                      $i = 1;
                      $subscribers = Subscriber::getActive('digital');

                      if(count($subscribers) >= 1){
                      foreach($subscribers as $sub){
                        ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $sub->name ?></td>
                        <td><?php echo $sub->phone ?></td>
                        <td><?php echo $sub->email ?></td>
                        <td><span class="badge badge-primary"><?php echo ucfirst($sub->type) ?> </span> <?php echo $sub->period ." "; ?> <?php echo $sub->period > 1 ? $sub->duration."s" : $sub->duration ?></td>
                        <td class="small"><?php echo date('d F, Y', strtotime($sub->created_at)); ?></td>
                        <td><?php echo $sub->payment == 0 ? '<span class="badge badge-danger">Not Payed</span>' : '<span class="badge badge-success">Payed</span>' ?></td>
                        <td><a title="View <?php echo $sub->name ?>" href="subscribers/view.php?id=<?php echo $sub->id; ?>" class="icon-circle bg-primary"><i class="fa fa-eye text-white"></i></a></td>
                        <td><a title="Edit <?php echo $sub->name ?>" href="subscribers/edit.php?id=<?php echo $sub->id; ?>" class="icon-circle bg-info"><i class="fa fa-edit text-white"></i></a></td>
                      </tr>
                      <?php $i++;   } }else{
                        echo '<tr>
                          <td colspan="10" class="text-center">No data</td>
                        </tr>';
                      }
                    }else{
                        $i = 1;
                        $subscribers = Subscriber::allDgtSubscribers();

                        if(count($subscribers) >= 1){
                        foreach($subscribers as $sub){
                          ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $sub->name ?></td>
                          <td><?php echo $sub->phone ?></td>
                          <td><?php echo $sub->email ?></td>
                          <td><span class="badge badge-primary"><?php echo ucfirst($sub->type) ?> </span> <?php echo $sub->period ." "; ?> <?php echo $sub->period > 1 ? $sub->duration."s" : $sub->duration ?></td>
                          <td class="small"><?php echo date('d F, Y', strtotime($sub->created_at)); ?></td>
                          <td><?php echo $sub->payment == 0 ? '<span class="badge badge-danger">Not Payed</span>' : '<span class="badge badge-success">Payed</span>' ?></td>
                          <td><a title="View <?php echo $sub->name ?>" href="subscribers/view.php?id=<?php echo $sub->id; ?>" class="icon-circle bg-primary"><i class="fa fa-eye text-white"></i></a></td>
                          <td><a title="Edit <?php echo $sub->name ?>" href="subscribers/edit.php?id=<?php echo $sub->id; ?>" class="icon-circle bg-info"><i class="fa fa-edit text-white"></i></a></td>
                        </tr>
                     <?php $i++;   } }else{
                       echo '<tr>
                        <td colspan="10" class="text-center">No data</td>
                       </tr>';
                     }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
</div>


<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>