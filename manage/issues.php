<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>

<?php

$msg = "";

if(isset($_POST['add'])){
  if(isset($_FILES['cover'])){
    $filename = $_FILES['cover']['name'];

    if($filename !== ''){
      $size = $_FILES['cover']['size'];
      $tmp_name = $_FILES['cover']['tmp_name'];
      $dir = "issues/";
      
      if(move_uploaded_file($tmp_name, $dir.$filename)){
        $issue = new Issue();
        $issue->cover = $filename;
        $issue->month = $_POST['month'];
        $issue->year = $_POST['year'];

        if($issue->create()){
          $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong>Success!</strong> The issue has been added.
        </div>';
        redirect($_SERVER['PHP_SELF']."?added");
      }else{
        redirect($_SERVER['PHP_SELF']."?failed");
      }

    }else{
      $issue = new Issue();
      $issue->cover = "null";
      $issue->month = $_POST['month'];
      $issue->year = $_POST['year'];

      if($issue->create()){
        $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>Success!</strong> The issue has been added.
      </div>';
      }
    }
  }
}
}

if(isset($_POST['update'])){
  if(isset($_FILES['cover'])){
    if($_FILES['cover']['name'] !== ''){
      $filename = $_FILES['cover']['name'];
      $size = $_FILES['cover']['size'];
      $tmp_name = $_FILES['cover']['tmp_name'];
      $dir = "issues/";
      
      if(move_uploaded_file($tmp_name, $dir.$filename)){
        $issue = Issue::findById($_POST['id']);
        $issue->cover = $filename;
        $issue->month = $_POST['month'];
        $issue->year = $_POST['year'];

        if(isset($_POST['current'])){
          Issue::makeCurrent($_POST['id']);
        }
  
        if($issue->update()){
          $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong>Success!</strong> The issue has been updated.
        </div>';
        }

      }
    }else{
      $issue = Issue::findById($_POST['id']);
      $issue->month = $_POST['month'];
      $issue->year = $_POST['year'];

      if(isset($_POST['current'])){
        Issue::makeCurrent($_POST['id']);
      }

      if($issue->update()){
        $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>Success!</strong> The issue has been updated.
      </div>';
      }
    }
  }
}

if(isset($_POST['delete'])){
  $id = $_POST['id'];
  $issue = Issue::findById($id);
  if(file_exists("issues/".$issue->cover)){
    if(unlink("issues/".$issue->cover)){
      if($issue->delete()){
        $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>Success!</strong> The issue has been deleted.
      </div>';
      }
    }
  }else{
    if($issue->delete()){
      $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong>Success!</strong> The issue has been deleted.
    </div>';
    }
  }
}

?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>



<!-- Begin Page Content -->
<div class="container-fluid">
  <?php



  echo $msg;

if(isset($_GET['added'])){
  echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> The issue has been added.
</div>';
}
if(isset($_GET['updated'])){
  echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> The issue has been updated.
</div>';
}
if(isset($_GET['deleted'])){
  echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> The issue has been deleted.
</div>';
}

?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Issues</h1>
  <span title="Add Issue" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" style="cursor:pointer" role="button" data-toggle="modal" data-target="#addModal"><i class="fas fa-plus fa-sm text-white-50"></i> New</span>
  <!-- <a href="addInst.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> New</a> -->
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->

<!-- Content Row -->
<div class="row">
  <div class="col-md-8">
    <div class="card shadow mb-4">
      <div class="card-header">
        <h6 class="m-0 font-weight-bold text-primary">All issues</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Issue</th>
                  <th>Date</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>#</th>
                  <th>Issue</th>
                  <th>Date</th>
                  <th></th>
                  <th></th>
                </tr>
              </tfoot>
              <tbody>
                <?php

                $issues = Issue::all();
                if(count($issues) >=1){
                  $i = 1;
                  foreach($issues as $iss){
                    echo '<tr>
                          <td>'.$i.'</td>
                          <td>'.$iss->month.' '.$iss->year.'</td>
                          <td>'.date('Y-m-d', strtotime($iss->created_at)).'</td>
                          <td class="text-center"><a class="btn btn-default text-primary" href="issues.php?edit&id='.$iss->id.'"><i class="fa fa-edit"></i></a></td>
                          <td class="text-center"><form method="POST"><input type="hidden" name="id" id="id" class="form-control" value="'.$iss->id.'"><input type="hidden" name="delete"><button class="btn btn-default" type="submit" name="delete"><i class="fa fa-trash"></i></button></form></td>
                    </tr>';

                    $i++;
                  }
                }else{
                  echo '<tr><td colspan="4" class="text-center">No data found</td></tr>';
                }

                ?>
              </tbody>
            </table>
            
          </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card shadow mb-4">
        <div class="card-header">
          <h6 class="m-0 font-weight-bold text-danger">Current Issue</h6>
        </div>
        <div class="card-body text-center">
          <?php $current = Issue::getCurrent();  
          
                if($current){

                
          ?>
          <p>
            <?php 
              if($current->cover !== 'null'){
                echo '<img src="issues/'.$current->cover.'" alt="Thelicham Monthly" width="200px">';
              }else{
                echo '<img src="issues/dummy.jpg" alt="Thelicham Monthly" width="200px">';
              }

              if(!$current->cover){
                echo '<img src="issues/dummy.jpg" alt="Thelicham Monthly" width="200px">';
              }
            
            ?>
            
          </p>
          <b><?php echo $current->month ." ".$current->year; }else{ ?></b>

            <p>
              <img src="issues/dummy.jpg" alt="Thelicham Monthly" width="200px">
            </p>
            <b>No Issue</b>
          <?php } ?>
        </div>
      </div>
  </div>
  <div class="clearfix"></div>
</div>
<?php if(isset($_GET['edit'])){ 
      if($_GET['id'] !== ''){

      $issueEdit = Issue::findById($_GET['id']);
        $cover = $issueEdit->cover;
        $year = $issueEdit->year;
        $month = $issueEdit->month;

        $id = $_GET['id'];
        $check = $db->query("SELECT issue_id FROM current_issue WHERE issue_id = {$id}");
        $cstat = 0;
        if($check->num_rows == 1){
          $cstat = 1;
        }else{
          $cstat = 0;
        }

      
?>

<div class="row">
  <div class="col-md-8">
    <div class="card shadow mb-4">
      <div class="card-header">
        <h6 class="m-0 font-weight-bold text-info">Edit <?php echo $cstat == 1 ? '<span class="badge badge-danger">Current</span>' : ''; ?></h6>
      </div>
      <div class="card-body">
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
            <p class="mb-4 text-center">
              <?php
                if($cover !== 'null'){
                  echo '<img src="issues/'.$cover.'" width="150">';
                }elseif($cover == ''){
                  echo '<img src="issues/dummy.jpg" width="150">';
                }else{
                  echo '<img src="issues/dummy.jpg" width="150">';
                }
              ?>
            </p>
            <div class="modal-body">
              <p>Required fields are marked (*)</p>
              <div class="form-group">
                <div class="uploaded-cover"></div>
                <label for="cover">Cover</label>
                <input type="file" name="cover" id="cover" class="form-control-file">
              </div>
              <div class="form-group">
                <label for="month">Month *</label>
                <input value="<?php echo $month; ?>" type="text" name="month" id="month" class="form-control" required>
              </div>
              <div class="form-group">
                <label for="year">Year *</label>
                <input value="<?php echo $year; ?>" type="text" name="year" id="year" class="form-control" required>
              </div>
              <div class="form-group">
                <div class="custom-control custom-checkbox small">
                  <input type="checkbox" class="custom-control-input" id="current" name="current" <?php echo $cstat == 1 ? 'checked' : ''; ?>>
                  <label class="custom-control-label" for="current">Current issue</label>
                </div>
                <input type="hidden" name="update">
              </div>
            </div>
              <button class="btn btn-info" type="submit" name="update">Update</button>
              <a href="issues.php" class="btn btn-secondary">Cancel</a>
          </form>
      </div>
    </div>
  </div>
</div>


<?php } }?>


<!-- Content Row -->


</div>
<!-- /.container-fluid -->
<!-- New issue add Modal-->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="addModalLabel">New Issue</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        </div>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
          <div class="modal-body">
            <p>Required fields are marked (*)</p>
            <div class="form-group">
              <div class="uploaded-cover"></div>
              <label for="cover">Cover</label>
              <input type="file" name="cover" id="cover" class="form-control-file">
            </div>
            <div class="form-group">
              <label for="month">Month *</label>
              <input type="text" name="month" id="month" class="form-control" required>
            </div>
            <div class="form-group">
              <label for="year">Year *</label>
              <input type="text" name="year" id="year" class="form-control" required>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary" type="submit" name="add">Add</button>
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          </div>
          <input type="hidden" name="add">
        </form>
    </div>
    </div>
</div>

</div>

<?php include('includes/footer.php'); ?>
<script src="js/jquery.easing.min.js"></script>

<script>
$(document).ready(function() {
  $('#dataTable').DataTable();
});
</script>