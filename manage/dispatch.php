<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>
<?php
// dispatch
$status = 0;
if(isset($_POST['dispatch_grp']) || isset($_POST['issue'])){
  $issue = $_POST['issue'];

  // active subscribers
  $actives = Subscriber::getActive('print');
  foreach($actives as $active){
    if(Subscription::dispatch($active->id)){
      $dispatch = new Dispatch();
      $dispatch->subscriber_id = $active->id;
      $dispatch->issue_id = $issue;
      $dispatch->type = 'group';
      $dispatch->date = $db->now();
      if($dispatch->save()){
        Subscription::checkSubscriptionById($active->id);
        $status = 1;
      }else{
        $status = 0;
      }
    }
  }
  
  if(isset($status)){
    if($status == 1){
      redirect($_SERVER['PHP_SELF']."?success");
    }else{
      redirect($_SERVER['PHP_SELF']."?failed");
    }
  }
}


//roll back dispatch log personal
    if(isset($_POST['personal']) || isset($_POST['subid'])){
      $did = $_POST['did'];
      $subid = $_POST['subid'];
      if(Subscription::withdrawDispatch($did, $subid)){
        $msg = '<span class="text-success"><i class="fa fa-check fa-fw mr-1"></i>Issue rolled back.</span>';
        header('refresh:3');
      }else{
        // redirect($_SERVER['PHP_SELF']."?failed");
        $msg = '<span class="text-danger"><i class="fa fa-times fa-fw mr-1"></i>Issue not rolled back.</span>';
      }
    }

    //roll back dispatch log group
    if(isset($_POST['group']) || isset($_POST['id'])){
      if(!empty($_POST['id'])){
        $id = $_POST['id'];
        $subscribers = $db->query("SELECT id, subscriber_id FROM dispatch_history WHERE issue_id = {$id} AND type='group'");
        if($subscribers->num_rows >= 1){
            foreach($subscribers as $subscriber){
                $did = $subscriber['id'];
                $subid = $subscriber['subscriber_id'];
                if(Subscription::withdrawDispatch($did, $subid)){
                  $msg = '<span class="text-success"><i class="fa fa-check fa-fw mr-1"></i>Issue rolled back.</span>';
                  header('refresh:3');
                }
            }
        }
      }
    }

    // status msg
    if(isset($_GET['success'])){
      $msg = '<span class="text-success"><i class="fa fa-check fa-fw mr-1"></i>Dispatched successfully.</span>';
    }elseif(isset($_GET['failed'])){
      $msg = '<span class="text-danger"><i class="fa fa-times fa-fw mr-1"></i>Dispatch failed.</span>';
    }
    ?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>

<?php

$msg;

  // all issues
  $issues = Issue::all();

  // subscribers active list
  $subscribers = Subscriber::getActive('print');


    // Get dispatch log
    $logs = $db->query("SELECT s.name, d.* FROM dispatch_history d INNER JOIN subscribers s ON d.subscriber_id = s.id WHERE d.type='personal'");

    
    
    
?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Dispatch</h1>
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->

<!-- Content Row -->

<div class="row">
  <div class="col-md-6">
    <div class="card shadow mb-4">
      <div class="card-header">
        <h6 class="m-0 font-weight-bold text-primary">Batch process</h6>
      </div>
      <div class="card-body">
        <form action="" method="post">
          <div class="form-group">
            <b>Dispatch:</b> Every active subscribers
          </div>
          <div class="form-group">
            <label for="issue">Select Issue</label>
            <select name="issue" id="issueGrp" class="form-control select" required>
              <option value="" selected>Select Issue</option>
              <?php
                foreach($issues as $issue){
                  echo '<option value="'.$issue->id.'">'.$issue->month.' '.$issue->year.'</option>';
                }
              ?>
            </select>
          </div>
          <div class="form-group">
            <input type="submit" value="Dispatch now" class="btn btn-primary" name="dispatch_grp">
          </div>
        </form>
      </div>
      <div class="card-footer text-right">
           <?php echo isset($msg) ? $msg : '<i class="fa fa-clock fa-fw"></i> Not dispatched yet'; ?>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card shadow mb-4">
        <div class="card-header">
          <h6 class="m-0 font-weight-bold text-secondary">Personal dispatch</h6>
        </div>
        <div class="card-body">
          <form action="dispatch_personal.php" method="get">
            <div class="form-group">
              <label for="issue">Issue</label>
              <select name="issue" id="issue" class="form-control select" required>
                <option value="" selected>Select Issue</option>
                <?php
                foreach($issues as $issue){
                  echo '<option value="'.$issue->id.'">'.$issue->month.' '.$issue->year.'</option>';
                }
              ?>
              </select>
            </div>
            <div class="form-group">
              <label for="subscriber">Subscriber</label>
              <select name="subscriber" id="subscriber" class="form-control select" required>
                <?php 
                  if(count($subscribers) >= 1){
                    echo '<option value="" selected>Select subscriber</option>';
                    foreach($subscribers as $subscriber){
                      echo '<option value="'.$subscriber->id.'">'.$subscriber->name.'</option>';
                    }
                  }else{
                    echo '<option value="" selected>No subscriber found</option>';
                  }
                ?>
              </select>
            </div>
              <div class="form-group">
                <input type="submit" value="Dispatch" class="btn btn-secondary" name="dispatch">
            </div>
          </form>
        </div>
      </div>
  </div>
  <div class="clearfix"></div>
</div>


<!-- Dispatch history -->
<div class="row">
  <div class="col-md-12">
    <div class="card shadow mb-4">
      <div class="card-header">
        <h6 class="m-0 font-weight-bold text-info">History</h6>
      </div>
      <div class="card-body">
        <div class="tab-pan">
          <ul class="nav nav-tabs">
              <li class="nav-item">
                <a href="#group" data-toggle="tab" class="nav-link active"><i class="fa fa-users fa-fw mr-1"></i>Group</a>
              </li>
              <li class="nav-item">
                <a href="#personal" data-toggle="tab" class="nav-link"><i class="fa fa-user fa-fw mr-1"></i>Personal</a>
              </li>
          </ul>
        </div>
        <div class="tab-content border p-3 border-1">
          <div class="tab-pane active" id="group">
            <div class="table-responsive">
              <table class="table table-bordered table-condensed" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Issue</th>
                    <th>Type</th>
                    <th>Subscribers</th>
                    <th>Date</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                    $grp = Dispatch::getGroupLog();
                    $i =1;
                    foreach($grp as $gr){
                      $issueId = $gr->issue_id;

                      $subCount = $db->query("SELECT COUNT(*) as total FROM dispatch_history WHERE issue_id = {$issueId} AND type='group'");
                      if($subCount){
                        foreach($subCount as $count){
                          $count = $count['total'];
                        }
                      }
                      //getting date of dispatch
                      $date = Subscription::getDispatchLogDate($issueId, 'group');

                      $issueGrp = Issue::findById($issueId);
                      echo '<tr>
                      <td>'.$i.'</td>
                      <td>'.$issueGrp->month.' '.$issueGrp->year.'</td>
                      <td>Group</td>
                      <td>'.$count.'</td>
                      <td>'.date('d-m-Y', strtotime($date)).'<br>'.date('g:i A', strtotime($date)).'</td>
                      <td class="text-center"><a href="viewIssues.php?id='.$issueId.'&type=group" class="btn">View</a></td>
                      <td class="text-center"><span id="dispDel">
                        <form action="" method="post">
                          <input type="hidden" name="id" value="'.$issueId.'">
                          <button class="btn" name="group" type="submit"><i class="fa fa-times"></i></span></button>
                      </form>
                      </td>
                    </tr>';

                    $i++;
                    }

                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="tab-pane fade" id="personal">
            <div class="table-responsive">
              <table class="table table-bordered table-condensed" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Issue</th>
                    <th>Type</th>
                    <th>Subscriber</th>
                    <th>Date</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $i =1;
                    foreach($logs as $log){

                    $name = $log['name'];

                      $issue = Issue::findById($log['issue_id']);
                      // print_r($subscriber);
                      echo '<tr>
                      <td>'.$i.'</td>
                      <td>'.$issue->month.' '.$issue->year.'</td>
                      <td>'.ucfirst($log['type']).'</td>
                      <td>'.$name.'</td>
                      <td>'.date('Y-m-d', strtotime($log['date'])).'</td>
                      <td width="100" class="text-center">'.($log['type'] == 'personal' ? '<a href="subscribers/view.php?id='.$log['subscriber_id'].'" class="btn btn-default text-primary" title="View"><i class="fa fa-eye"></i></a>' : '').'</td>
                      <td class="text-center"><span id="dispDel">
                        <form action="" method="post">
                          <input type="hidden" name="did" value="'.$log['id'].'">
                          <input type="hidden" name="subid" value="'.$log['subscriber_id'].'">
                          <button class="btn" name="personal" type="submit"><i class="fa fa-times"></i></span></button>
                        </form>
                      </td>
                    </tr>';

                    $i++;
                    }

                  ?>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('includes/footer.php'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>
$(document).ready(function() {
  $('.table').DataTable();
  $('.select').select2();
});
</script>