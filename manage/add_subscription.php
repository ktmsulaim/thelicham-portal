<?php
require_once('includes/init.php');
// adding subscription
if(isset($_POST['add']) || isset($_POST['pwd'])){
    //user verification
    $user = User::findById($_SESSION['user_id']);
    $pass = $user->password;

    echo "it works";

    if($_POST['pwd'] === $pass){
        $id = $_POST['id'];
        $ref_no = $_POST['ref_no'];
        $period = $_POST['period'];
        $duration = $_POST['duration'];
        $type = $_POST['type'];
        $total = $_POST['total'];

        $subscriber = Subscriber::findById($id);
        $subscriber->period = $period;
        $subscriber->duration = $duration;
        $subscriber->type = $type;
        $subscriber->total = $total;
        
        if($subscriber->update()){
            if(Subscription::addSubscription($id, $ref_no, 1, $period, $duration)){
                redirect('subscribers/view.php?id='.$id);
            }else{
                redirect('subscription.php?failed=subscription');
            }
        }else{
            redirect('subscription.php?failed=update');
        }
        
       
        
    }else{
        redirect('subscription.php?failed=pwd');
    }
}