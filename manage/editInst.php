<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>

<?php 
if(isset($_GET['id'])){
    $id = $_GET['id'];
    $_SESSION['inst_id'] = $id;
    $inst = Institution::findById($id);


    foreach($inst as $insti){
      $name = $insti->name;
      $place = $insti->place;
      $phone = $insti->phone;
      $email = $insti->email;
      $promo = $insti->promo;
    }
}else{
    // redirect("http://".$_SERVER['HTTP_HOST']."/manage/insti.php");
}

if(isset($_POST['update'])){
  $inst = new Institution();
  $inst->id = $_SESSION['inst_id'];
  $inst->name = trim($_POST['name']);
  $inst->place = trim($_POST['place']);
  $inst->phone = trim($_POST['phone']);
  $inst->email = trim($_POST['email']);

  if($inst->update()){
    unset($_SESSION['inst_id']);
    redirect("http://".$_SERVER['HTTP_HOST']."/manage/insti.php?updated");
  }else{
    redirect("http://".$_SERVER['HTTP_HOST']."/manage/editInst.php?id=".$_SESSION['inst_id']);
  }
}

if(isset($_POST['delete'])){
  $id = $_POST['id'];
  $delete = $db->query("DELETE FROM institutions WHERE id = {$id}");

  if($delete){
    redirect("http://".$_SERVER['HTTP_HOST']."/manage/insti.php?deleted");
  }else{
    die("Deletion failed.");
  }
}

?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>



<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Edit Institution </h1>
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->

<!-- Content Row -->

<div class="row">
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit <?php echo $name; ?></h6>
            </div>
            <div class="card-body">
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" type="text" id="name" name="name" value="<?php echo $name; ?>" placeholder="Full name of institution" required>
                    </div>
                    <div class="form-group">
                        <label for="place">Place</label>
                        <input class="form-control" type="text" id="place" name="place" value="<?php echo $place; ?>" placeholder="Place of institution" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input class="form-control" type="text" id="phone" name="phone" value="<?php echo $phone; ?>" placeholder="Contact number" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" type="email" id="email" name="email" value="<?php echo $email; ?>" placeholder="Valid email address" required>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-info" name="update" role="submit"><i class="fa fa-edit fa-fw"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
      <div class="card shadow mb-4">
        <div class="card-header">
          <h6 class="m-0 font-weight-bold text-danger">Delete</h6>
        </div>
        <div class="card-body">
          <p>Delete this institution.</p>
          <span title="Delete" class="float-right text-danger" style="cursor:pointer" role="button" data-toggle="modal" data-target="#deleteModal">Proceed</span>

          <!-- Institution delete confirmation Modal-->
          <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="promoModalLabel">Are sure?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        </div>
                        <div class="modal-body">You are going to delete this institution. You cannot undo after deleted.<br>
                        </div>
                        <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                          <input type="hidden" name="id" value="<?php echo $id; ?>">
                          <button type="submit" role="submit" name="delete" class="btn btn-danger">Delete</button>
                        </form>

                        </div>
                    </div>
                    </div>
                </div>
        </div>
      </div>
    </div>
</div>

<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('includes/footer.php'); ?>