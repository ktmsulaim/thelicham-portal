<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>


<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>

<?php
    $active = $db->getTotal("FROM subscribers LEFT JOIN subscriptions ON subscribers.id = subscriptions.subscriber_id WHERE subscribers.type = 'print' AND subscriptions.status = 1");
?>

<!-- Begin Page Content -->
<div class="container-fluid">
    
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Print</h1>
</div>

<!-- Content Row -->

<div class="row">
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">Print Labels</h6>
            </div>
            <div class="card-body">
                <form action="print.php" method="get">
                    <p class="text-center">Print settings</p>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Paper</td>
                                <td>A4</td>
                            </tr>
                            <tr>
                                <td>Orientation</td>
                                <td>Portrait</td>
                            </tr>
                            <tr>
                                <td>Labels</td>
                                <td><input value="12" type="number" name="col" id="col" min="1" max="15" class="form-control" required></td>
                            </tr>
                            <tr>
                                <td width="150">Font size <br> <div class="text-center"><small>In pixels</small></div></td>
                                <td><input type="number" name="fontsize" id="fontsize" class="form-control" min="18" value="18"></td>
                            </tr>
                            <tr>
                                <td width="150">Margin Bottom <br> <div class="text-center"><small>In pixels</small></div></td>
                                <td><input type="number" name="mb" id="mb" class="form-control" min="20" value="20"></td>
                            </tr>
                            <tr>
                                <td>Color</td>
                                <td><input type="color" name="color" id="color" value="#444"></td>
                            </tr>
                        </tbody>
                    </table>
                    <button class="btn btn-primary" type="submit">Print</button>
                </form>
                    
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card border-bottom-primary shadow py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Active subscribers</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $active; ?></div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-users fa-2x text-gray-300"></i>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>

<?php include('includes/footer.php'); ?>