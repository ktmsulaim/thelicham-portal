<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>
<?php
$cols = 1;
$mb = 20;
$fontSize = 18;
$color = '#444';

if(isset($_GET['col'])){
    $cols = $_GET['col'];
}

if(isset($_GET['fontsize'])){
    $fontSize = $_GET['fontsize'];
}

if(isset($_GET['mb'])){
    $mb = $_GET['mb'];
}

if(isset($_GET['color'])){
    $color = $_GET['color'];
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0-11/css/all.min.css">
<link rel="stylesheet" href="style.css">
<!-- <link rel="stylesheet" href="css/print.css" media="print"> -->
<title>Active subscribers</title>
<style>
    .bordered{
        padding: 10px;
        border: 1px solid #444;
        margin: 5px;
        margin-bottom: <?php echo $mb; ?>px !important;
    }
    p{
        margin: 0;
        font-size: <?php echo $fontSize; ?>px;
        font-weight: bold;
        text-transform: uppercase;
        color: <?php echo $color; ?>;
    }
    
</style>
<style  type="text/css" media="print">
    div.page-break{
        page-break-before: always;
        page-break-inside: avoid;
    }
</style>
</head>
<body class="form">
    <div class="container">
            <?php
                $subscribers = Subscriber::query("SELECT s.* FROM subscribers s LEFT JOIN subscriptions sb ON s.id = sb.subscriber_id WHERE sb.status = 1 AND type = 'print' ORDER BY s.rms, s.pincode");
                $i = 0;
                $index = 1;
                foreach($subscribers as $subscriber){
                    if($i % $cols == 0 || $i == 0){
                        echo '<div class="row page-break">';
                    }

                    echo '<div class="col-md-4">
                    <div class="bordered mb-4">
                        <p>
                        Sub.No: '.$subscriber->id.'<br>
                        '.$subscriber->name.'<br>
                        C/O '.$subscriber->so.'<br>
                        '.$subscriber->house.' (H)<br>
                        '.$subscriber->place.'<br>
                        '.$subscriber->district.'<br>
                        '.$subscriber->post.' (P.O), '.$subscriber->pincode.'<br>
                        '.$subscriber->phone.'<br>
                        '.$subscriber->rms.'<br>
                        </p>
                    </div>
                </div>';
                $i++;

                if($i % $cols == 0 || $i == 0){
                    echo '</div>';
                }

                $index++;
                }
            ?>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>