<?php include('../includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("../login.php"); } ?>

<?php
 $id = "";
 $name = "";
 $so = "";
 $house = "";
 $place = "";
 $email = "";
 $phone = "";
 $rms = "";
 $district = "";
 $post = "";
 $pincode = "";
 $type = "";
 $duration = "";
 $period = "";
 $total = "";
 $created_at = "";
 $payment = "";
 $pmethod = "";

 if(isset($_GET['id'])){
  $_SESSION['sub_id'] = $_GET['id'];
  $id = $_GET['id'];
  $subscriber = Subscriber::findById($id);

  // find subscription
  $subscription = Subscription::findBySubscriberId($id);
  if($subscription){
    $ref_no = $subscription->ref_no;
  }else{
    $ref_no = "";
  }

  $id = $subscriber->id;
  $name = $subscriber->name;
  $so = $subscriber->so;
  $house = $subscriber->house;
  $place = $subscriber->place;
  $email = $subscriber->email;
  $phone = $subscriber->phone;
  $rms = $subscriber->rms;
  $district = $subscriber->district;
  $post = $subscriber->post;
  $pincode = $subscriber->pincode;
  $type = $subscriber->type;
  $duration = $subscriber->duration;
  $period = $subscriber->period;
  $total = $subscriber->total;
  $created_at = $subscriber->created_at;
  $payment = $subscriber->payment;
  $promo = $subscriber->promo;


  
}else{
  redirect("http://".$_SERVER['HTTP_HOST']."/manage/subscribers.php");
}

if(isset($_POST['update'])){
  $subscriber = new Subscriber();
  $subscriber->id = $_SESSION['sub_id'];
  $subscriber->name = trim($_POST['name']);
  $subscriber->so = trim($_POST['so']);
  $subscriber->house = trim($_POST['house']);
  $subscriber->place = trim($_POST['place']);
  $subscriber->email = trim($_POST['email']);
  $subscriber->phone = trim($_POST['phone']);
  $subscriber->rms = trim($_POST['rms']);
  $subscriber->district = trim($_POST['district']);
  $subscriber->post = trim($_POST['post']);
  $subscriber->pincode = trim($_POST['pincode']);
  $subscriber->type = trim($_POST['type']);
  $subscriber->promo = trim($_POST['promo']);
  $subscriber->payment = trim($_POST['payment']);
  $subscriber->created_at = trim($_POST['created_at']);
  $subscriber->period = trim($_POST['period']);
  $subscriber->duration = trim($_POST['duration']);
  $subscriber->total = trim($_POST['total']);


  $url = "";

  if($_POST['type'] == 'print'){
    $url = "subscribers";
  }else{
    $url = "subscribersDgt";
  }

   // // check if he has a subscription
   if(Subscription::hasSubscription($_POST['id'])){
    if(isset($_POST['ref_no'])){
      $subscription = Subscription::findBySubscriberId($_POST['id']);
      $sub = Subscription::findById($subscription->id);
      if($sub){
        if($_POST['ref_no'] !== $sub->ref_no){
          $sub->ref_no = $_POST['ref_no']; 
          if($sub->update()){
            redirect("http://".$_SERVER['HTTP_HOST']."/manage/".$url.".php?updated");
          }else{
            header('Location: http://'.$_SERVER['HTTP_HOST'].'/manage/subscribers/edit.php?failed&id='.$_SESSION['sub_id']);
          } 
        }else{
          if($subscriber->update()){
            header('Location: http://'.$_SERVER['HTTP_HOST'].'/manage/subscribers/edit.php?failed=ref&id='.$_SESSION['sub_id']);
          }
        }
      }
    }else{
      if($subscriber->update()){
        header('Location: http://'.$_SERVER['HTTP_HOST'].'/manage/subscribers/edit.php?failed=ref&id='.$_SESSION['sub_id']);
      }
    }
  }else{

    if($subscriber->update()){
    
      redirect("http://".$_SERVER['HTTP_HOST']."/manage/".$url.".php?updated");

      
    }else{
      header('Location: http://'.$_SERVER['HTTP_HOST'].'/manage/subscribers/edit.php?id='.$_SESSION['sub_id']);
    }

  }
}

?>

<?php include('../includes/sidebar.php'); ?>

<?php include('../includes/topnav.php'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">


<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Edit subscriber</h1>
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->


<!-- Content Row -->
<form action="" method="POST">
  <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
<div class="row">
  <div class="col-md-8">
    <div class="card shadow mb-4" id="personal">
      <div class="card-header">
        <h6 class="m-0 font-weight-bold text-primary clearfix">Personal Info <span title="Delete" class="float-right text-danger" style="cursor:pointer" role="button" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i></span></h6>
      </div>
      <div class="card-body">
        <div class="prof-pic">
          <p class="text-center">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzTfxWVyzy3C3nJUSoW8_I8UZE9-pfOYO3c9zceejO-0r4Tp--" alt="User" width="150">
          </p>
        </div>
        <div class="personal-info">
            <table class="table">
              <tr>
                <th>Name</th>
                <td><input class="form-control" name="name" type="text" value="<?php echo $name; ?>" required></td>
                <th>So, Do, Co</th>
                <td><input class="form-control" name="so" type="text" value="<?php echo $so; ?>" required></td>
              </tr>
              <tr>
                <th>Email</th>
                <td><input class="form-control" name="email" type="email" value="<?php echo $email; ?>" required></td>
                <th>Phone</th>
                <td><input class="form-control" name="phone" type="text" value="<?php echo $phone; ?>" required></td>
              </tr>
            </table>
        </div>
      </div>
    </div>


      <div class="row">
        
        <?php
        if(!empty($promo)){

          $instName = "No data";
          $instPlace = "No data";
          $instPhone = "No data";

         $insts =  $db->query("SELECT * FROM institutions WHERE promo = '{$promo}'");
          while($row = mysqli_fetch_array($insts)){
              $instName = $row['name'];
              $instPlace = $row['place'];
              $instPhone = $row['phone'];
          }

          ?>
        <div class="col-md-6">
          <div class="card shadow mb-4 h-100">
            <div class="card-header">
              <h6 class="m-0 font-weight-bold text-warning">Promo code</h6>
            </div>
            <div class="card-body">
              <table class="table">
                <tr>
                  <th>Promo code</th>
                  <td><input type="text" class="form-control" name="promo" value="<?php echo $promo; ?>" required></td>
                </tr>
                <tr>
                  <th>Instituiton</th>
                  <td><?php echo $instName; ?></td>
                </tr>
                <tr>
                  <th>Place</th>
                  <td><?php echo $instPlace; ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <?php }else{ ?>

          <div class="col-md-6">
          <div class="card shadow mb-4 h-100">
            <div class="card-header">
              <h6 class="m-0 font-weight-bold text-info">Have a promo code</h6>
            </div>
            <div class="card-body d-flex justify-content-center" style="flex-direction: column;">
              <span class="text-center mb-2">Insert your promo code here</span>
              <input type="text" name="promo" class="form-control">
            </div>
          </div>
          </div>
        <?php }?>

        <div class="col-md-6">
          <div class="card shadow mb-4 h-100">
            <div class="card-header">
              <h6 class="m-0 font-weight-bold text-secondary">Save</h6>
            </div>
            <div class="card-body">
              <p>Make sure you have filled all fields. Editable values are compulsory.</p>
            </div>
            <div class="card-footer d-flex justify-content-between">
              <button class="btn btn-info" name="update" role="submit">Update</button>
              <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers.php" class="btn btn-secondary">Cancel</a>
          </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>



  </div>
  <div class="col-md-4">
    <div class="card shadow mb-4">
      <div class="card-header">
        <h6 class="m-0 font-weight-bold">Address</h6>
      </div>
      <div class="card-body">
        <table class="table">
          <tr>
            <th>House</th>
            <td><input class="form-control" name="house" type="text" value="<?php echo $house; ?>" required></td>
          </tr>
          <tr>
            <th>Place</th>
            <td><input class="form-control" name="place" type="text" value="<?php echo $place; ?>" required></td>
          </tr>
          <tr>
            <th>District</th>
            <td><input class="form-control" name="district" type="text" value="<?php echo $district; ?>" required></td>
          </tr>
          <tr>
            <th>Post</th>
            <td><input class="form-control" name="post" type="text" value="<?php echo $post; ?>" required></td>
          </tr>
          <tr>
            <th>PIN</th>
            <td><input class="form-control" name="pincode" type="text" value="<?php echo $pincode; ?>" required></td>
          </tr>
          <tr>
            <th>RMS</th>
            <td><input class="form-control" name="rms" type="text" value="<?php echo $rms; ?>"></td>
          </tr>
          <?php
              if(Subscription::hasSubscription($_GET['id'])){ ?>
              <tr>
                  <th class="text-danger">Receipt number</th>
                  <td><input type="text" class="form-control" name="ref_no" value="<?php echo $ref_no; ?>"></td>
              </tr>
           <?php   }
          ?>
        </table>
      </div>
    </div>

    <input type="hidden" name="type" value="<?php echo $type; ?>">
    <input type="hidden" name="payment" value="<?php echo $payment; ?>">
    <input type="hidden" name="created_at" value="<?php echo $created_at; ?>">
    <input type="hidden" name="period" value="<?php echo $period; ?>">
    <input type="hidden" name="duration" value="<?php echo $duration; ?>">
    <input type="hidden" name="total" value="<?php echo $total; ?>">

    
  </div>
  <div class="clearfix"></div>
</div>
</form>

<!-- Subscriber delete confirmation Modal-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
              <h5 class="modal-title" id="promoModalLabel">Are sure?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
              </div>
              <div class="modal-body">You are going to delete this subscriber. You cannot undo after deleted.<br>
              </div>
              <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

              <form action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers/delete.php" method="get">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="type" value="<?php echo $type; ?>">
                <!-- <button type="submit" role="submit" name="delete" value="delete" class="btn btn-danger">Delete</button> -->
                <input type="submit" value="Delete" name="delete" class="btn btn-danger">
              </form>

              </div>
          </div>
          </div>
      </div>



<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('../includes/footer.php'); ?>