<?php include('../includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("../login.php"); } ?>

<?php
 $id = "";
 $name = "";
 $so = "";
 $house = "";
 $place = "";
 $email = "";
 $phone = "";
 $rms = "";
 $district = "";
 $post = "";
 $pincode = "";
 $type = "";
 $duration = "";
 $period = "";
 $total = "";
 $created_at = "";
 $payment = "";
 $ref_no = "";
  $payment_date = "";
  $subscription_date = "";
  $expiry_date = "";
  $credits = "";
  $credits_left = "";
  $status = "";

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $subscriber = Subscriber::findById($id);
    $subscription = Subscription::getSubscription($id);
    $payment_method = Subscription::getMethod($id);

    if(Subscription::checkPaymentById($id) == 0){
      Subscription::removeSubscription($id);
    }

    $issues = $db->query("SELECT * FROM dispatch_history WHERE subscriber_id = {$id} AND type='group'");
    $issuesprs = $db->query("SELECT * FROM dispatch_history WHERE subscriber_id = {$id} AND type='personal'");

    if($subscription){
      while($row = mysqli_fetch_array($subscription)){
        $payment_date = $row['payment_date'];
        $ref_no = $row['ref_no'];
        $subscription_date = $row['subscription_date'];
        $expiry_date = $row['expiry_date'];
        $credits = $row['credits'];
        $credits_left = $row['credits_left'];
        $status = $row['status'];
        $tax_id = $row['tax_id'];
      }
    }

    while($mrow = mysqli_fetch_array($payment_method)){
      $pmethod = $mrow['name'];
    }


      $id = $subscriber->id;
      $name = $subscriber->name;
      $so = $subscriber->so;
      $house = $subscriber->house;
      $place = $subscriber->place;
      $email = $subscriber->email;
      $phone = $subscriber->phone;
      $rms = $subscriber->rms;
      $district = $subscriber->district;
      $post = $subscriber->post;
      $pincode = $subscriber->pincode;
      $type = $subscriber->type;
      $duration = $subscriber->duration;
      $period = $subscriber->period;
      $total = $subscriber->total;
      $created_at = $subscriber->created_at;
      $payment = $subscriber->payment;
      $promo = $subscriber->promo;


      if($period >1){
        $duration = $duration."s";
      }

    // update rms automatically
    if(isset($_POST['updaterms'])){
      $id = $_POST['id'];
      $pincode = $_POST['pincode'];

      if(Subscriber::updateRms($pincode, $id)){
        header('Location: Refresh:0');
      }

    }

  
    


}else{
  header('Location: http://'.$_SERVER['HTTP_HOST'].'/manage/subscribers.php');
}


?>

<?php include('../includes/sidebar.php'); ?>

<?php include('../includes/topnav.php'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
  <style>
      .issues{
      height: 250px;
      overflow-y: scroll;
  }

  .issues ol{
      padding: 0;
  }

  .issues ol li {

      display: block;
      padding: 5px 5px 5px 10px;
      background: #f5f5f5;
      margin: 5px;
      border-left: 3px solid #555;
      position: relative;
  }
  .issues ol li .dt{
    position: absolute;
    display:none;
    right: 5px;
    top: 0px;
  }
  .issues ol li:hover .dt{
    display: inline;
  }
  .issues ol li:hover #ddate{
    display: none;
  }
  .issues .personal{
    border-color: #36b9cc;
  }
  </style>


<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <!-- <h1 class="h3 mb-0 text-gray-800">View <?php echo $name; ?></h1> -->
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->

<!-- Content Row -->
<div class="row">
  <div class="col-md-8">
    <div class="card shadow mb-4" id="personal">
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Personal Info <span class="badge badge-primary">Id: <?php echo $id; ?></span></h6>
        <div class="dropdown no-arrow">
          <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; transform: translate3d(-156px, 19px, 0px); top: 0px; left: 0px; will-change: transform;">
            <div class="dropdown-header">Action:</div>
            <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers/edit.php?id=<?php echo $id; ?>" class="dropdown-item"><i class="fa fa-edit mr-2"></i>Edit</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="prof-pic">
          <p class="text-center">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzTfxWVyzy3C3nJUSoW8_I8UZE9-pfOYO3c9zceejO-0r4Tp--" alt="User" width="150">
          </p>
        </div>
        <div class="personal-info">
            <table class="table">
              <tr>
                <th>Name</th>
                <td><?php echo $name; ?></td>
                <th>So, Do, Co</th>
                <td><?php echo $so; ?></td>
              </tr>
              <tr>
                <th>Email</th>
                <td><?php echo $email; ?></td>
                <th>Phone</th>
                <td><?php echo $phone; ?></td>
              </tr>
            </table>
        </div>
      </div>
    </div>

    <div class="card shadow mb-4">
      <div class="card-header">
        <h6 class="text-info m-0 font-weight-bold">Subscription</h6>
      </div>
      <div class="card-body">
        <table class="table">
          <tr>
            <th>Type</th>
            <td><?php echo ucfirst($type); ?></td>
            <th>Duration</th>
            <td><?php echo $period." ".ucfirst($duration); ?></td>
          </tr>
          <tr>
            <th>Date</th>
            <td><?php echo !empty($subscription_date) ? $subscription_date : 'NULL'; ?></td>
            <th>Validity</th>
            <td><?php echo !empty($expiry_date) ? $expiry_date : 'NULL'; ?></td>
          </tr>
          <tr>
            <th>Status</th>
            <td><span class="badge <?php echo $status == 1 ? 'badge-success' : 'badge-danger' ?>"><?php if($status){if($status == 1){echo 'Active';}else{echo 'Not active';}}else{echo 'No subscription';} ?></span></td>
            <th>Credits</th>
            <td><?php echo !empty($credits_left) ? $credits_left : '0'; ?> /<?php echo !empty($credits) ? $credits : 'NULL'; ?> Left</td>
          </tr>
        </table>
      </div>
      <div class="card-footer"></div>
    </div>

    <div class="row">

    <?php
        if(!empty($promo)){
          $instName = "";
          $instPlace = "";
          $instPhone = "";
         $insts =  $db->query("SELECT * FROM institutions WHERE promo = '{$promo}'");
          while($row = mysqli_fetch_array($insts)){
              $instName = $row['name'];
              $instPlace = $row['place'];
              $instPhone = $row['phone'];
          }

          if($insts->num_rows >= 1){
          ?>
        
        <div class="col-md-6">
          <div class="card shadow mb-4">
            <div class="card-header">
              <h6 class="m-0 font-weight-bold text-warning">Promo code</h6>
            </div>
            <div class="card-body">
              <table class="table">
                <tr>
                  <th>Promo code</th>
                  <td><?php echo $promo; ?></td>
                </tr>
                <tr>
                  <th>Instituiton</th>
                  <td><?php echo $instName; ?></td>
                </tr>
                <tr>
                  <th>Place</th>
                  <td><?php echo $instPlace; ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        
        <?php } }?>

        <?php

          if($issues->num_rows >= 1 || $issuesprs->num_rows >= 1){ ?>
          <div class="col-md-6">
            <div class="card shadow mb-4">
              <div class="card-header">
                <h6 class="m-0 font-weight-bold text-secondary">Issues <span class="badge badge-secondary"><?php echo $issues->num_rows + $issuesprs->num_rows; ?></span></h6>
              </div>
              <div class="card-body issues">
                <ol>
                  <?php 
                    foreach($issues as $issue){
                      $issuesName = Issue::findById($issue['issue_id']);
                      $dispatched = $db->query("SELECT date, type FROM dispatch_history WHERE issue_id = {$issuesName->id} AND subscriber_id = {$id}");
                      foreach($dispatched as $dd){
                        $date = $dd['date'];
                      }
                      // print_r($issuesName->month);
                      echo '<li>'.$issuesName->month.' '.$issuesName->year.' <span id="ddate" class="float-right"><small><i class="fa fa-truck fa-fw"></i> '.date('Y-m-d', strtotime($date)).'</small></span><div class="dt"><form action="withdrawIssue.php" method="POST"><input type="hidden" name="id" value="'.$issue['id'].'"><input type="hidden" name="subId" value="'.$id.'"><button class="btn" name="withdraw"><i class="fa fa-trash"></i></button></form></div></li>';
                    }
                  ?>
                </ol>
                <ol>
                  <?php
                    if($issuesprs->num_rows >= 1){
                      echo '<h6>Personal</h6>';
                    }
                  ?>
                  <?php 
                    foreach($issuesprs as $issueprs){
                      $issuesName = Issue::findById($issueprs['issue_id']);
                      $dispatchedp = $db->query("SELECT date, type FROM dispatch_history WHERE issue_id = {$issuesName->id} AND subscriber_id = {$id}");
                      foreach($dispatchedp as $dp){
                        $date = $dp['date'];
                      }
                      // print_r($issuesName->month);
                      echo '<li class="personal">'.$issuesName->month.' '.$issuesName->year.' <span id="ddate" class="float-right"><small><i class="fa fa-truck fa-fw"></i> '.date('Y-m-d', strtotime($date)).'</small></span><div class="dt"><form action="withdrawIssue.php" method="POST"><input type="hidden" name="id" value="'.$issueprs['id'].'"><input type="hidden" name="subId" value="'.$id.'"><button class="btn" name="withdraw"><i class="fa fa-trash"></i></button></form></div></li>';
                      
                    }
                  ?>
                </ol>
              </div>
            </div>
          </div>
        <?php  } ?> 
      </div>
  </div>
  <div class="col-md-4">
    <div class="card shadow mb-4">
      <div class="card-header">
        <h6 class="m-0 font-weight-bold">Address</h6>
      </div>
      <div class="card-body">
        <table class="table">
          <tr>
            <th>House</th>
            <td><?php echo ucfirst($house); ?></td>
          </tr>
          <tr>
            <th>Place</th>
            <td><?php echo ucfirst($place); ?></td>
          </tr>
          <tr>
            <th>Post</th>
            <td><?php echo ucfirst($post); ?></td>
          </tr>
          <tr>
            <th>PIN</th>
            <td><?php echo ucfirst($pincode); ?></td>
          </tr>
          <tr>
            <th>RMS</th>
            <td><?php 
              if(empty($rms)){
                echo '<form action="" method="post">
                <input type="hidden" name="id" value="'.$id.'">
                <input type="hidden" name="pincode" value="'.$pincode.'">
                <input type="hidden" name="updaterms">
                <button name="updaterms" type="submit" class="btn-sm btn btn-primary">Update</button>
              </form>';
              }else{
                echo ucwords($rms);
              }
            ?></td>
          </tr>
        </table>
      </div>
    </div>

    <div class="card shadow mb-4">
      <div class="card-header">
        <h6 class="m-0 font-weight-bold text-danger">Payment <span title="Receipt number" class="float-right"><?php echo $ref_no ? $ref_no : ''; ?></span></h6>
      </div>
      <div class="card-body">
        <table class="table">
          <tr>
            <th>Status</th>
            <td><span class="badge <?php echo $payment == 0 ? 'badge-danger' : 'badge-success'; ?>"><?php echo $payment == 0 ? 'Not payed' : 'Payed'; ?></span></td>
          </tr>
          <tr>
            <th>Amount</th>
            <td><span class="small"><i class="fas fa-rupee-sign fa-fw"></i></span><?php echo $total; ?></td>
          </tr>
          <tr>
            <th>Method</th>
            <td><?php echo !empty($pmethod) ? ucfirst($pmethod) : 'NULL'; ?></td>
          </tr>
          <tr>
            <th>Tax ID</th>
            <td><?php echo !empty($tax_id) ? ucfirst($tax_id) : 'NULL'; ?></td>
          </tr>
          <tr>
            <th>Date</th>
            <td><?php echo !empty($payment_date) ? date('d-m-Y', strtotime($payment_date)) : 'NULL'; ?></td>
          </tr>
        </table>
      </div>
    </div>
    
  </div>
  <div class="clearfix"></div>
</div>


<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('../includes/footer.php'); ?>