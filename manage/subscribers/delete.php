<?php

require_once('init.php');

if(isset($_GET['id'])){
    $id = $_GET['id'];

    if(empty($id)){
        redirect($_SERVER['HTTP_HOST']."/manage/subscribers.php");
    }

    $type = $_GET['type'];
    $url = "";

    if($type == 'print'){
        $url = "subscribers.php";
    }else{
        $url = "subscribersDgt.php";
    }

    $subscriber = Subscriber::findById($id);
    if($subscriber){
        echo 'Subscriber found <br>';
        // if he has a subscription
        if(Subscription::hasSubscription($id)){
            echo 'He has subscription <br>';
            $sbid = Subscription::getSubscriptionId($id);
            if($sbid){
                $subscription = Subscription::findById($sbid->id);

                if($subscription->delete()){
                    echo 'Subscription removed';
                    if($subscriber->delete()){
                        echo 'Subscriber removed';
                        redirect("http://".$_SERVER['HTTP_HOST']."/manage/".$url."?deleted");
                    }else{
                        redirect("http://".$_SERVER['HTTP_HOST']."/manage/subscribers/edit.php?failed&id={$id}");
                    }
                }else{
                    redirect("http://".$_SERVER['HTTP_HOST']."/manage/subscribers/edit.php?failed&id={$id}");
                }
            }
        }else{
            if($subscriber->delete()){
                redirect("http://".$_SERVER['HTTP_HOST']."/manage/".$url."?deleted");
            }else{
                redirect("http://".$_SERVER['HTTP_HOST']."/manage/subscribers/edit.php?failed&id={$id}");
            }
        }
    }else{
        echo 'No subscriber found';
    }
}

?>