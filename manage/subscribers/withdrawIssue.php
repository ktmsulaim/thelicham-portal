<?php
include_once('../includes/head.php');

if(isset($_POST['withdraw'])){
    $did = $_POST['id'];
    $subId = $_POST['subId'];

    if(Subscription::withdrawDispatch($did, $subId)){
        redirect('view.php?id='.$subId);
    }
}