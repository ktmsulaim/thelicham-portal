<?php include('../includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("../login.php"); } 
ob_start();
?>

<?php
if(isset($_POST['btn'])){
    redirect("http://sacreationsknr.info");
}

if(isset($_POST['submit']) && isset($_POST['period'])){
    $subscriber = new Subscriber();
    $subscriber->name = $_POST['name'];
    $subscriber->so = $_POST['so'];
    $subscriber->house = $_POST['house'];
    $subscriber->place = $_POST['place'];
    $subscriber->email = $_POST['email'];
    $subscriber->phone = $_POST['phone'];
    $subscriber->district = $_POST['district'];
    $subscriber->post = $_POST['post'];
    $subscriber->pincode = $_POST['pincode'];
    $subscriber->type = $_POST['type'];
    $subscriber->duration = $_POST['duration'];
    $subscriber->period = $_POST['period'];
    $subscriber->total = $_POST['total'];
    $subscriber->promo = $_POST['promoCode'];
    $subscriber->payment = $_POST['payment'];
    $subscriber->created_at = Database::now();

    $_SESSION['pincode'] = $_POST['pincode'];

    $url = "subscribers/pay.php";

    
    if($subscriber->create()){
        $insert_id = $db->last_id();
        if(Subscriber::updateRms($_SESSION['pincode'], $insert_id)){
            unset($_SESSION['pincode']);
        }
        redirect("http://".$_SERVER['HTTP_HOST']."/manage/".$url."?id=".$insert_id);
    }else{
        redirect($_SERVER['PHP_SELF']. "?failed");
    }
    

}




?>

<?php include('../includes/sidebar.php'); ?>

<?php include('../includes/topnav.php'); ?>



<style>
    span#prm {
        padding: 8px;
        background: #eee;
        border-radius: 8px;
        font-weight: 600;
    }
    .bordered{
        border: 1px solid #999;
        padding: 5px;
        border-radius: 8px;
    }

    span.loader {
        padding: 10px;
        line-height: 30px;
    }

    #gen{
        cursor: pointer;
    }
</style>

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Add new</h1>
</div>


<!-- Content Row -->

<!-- <div class="row"></div> -->

<div class="row">
  <div class="col-md-8">
  <div class="errors w-100">
            <?php
                if(isset($_GET['error'])){
                    if($_GET['error'] == 'email'){
                        echo '<div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Error!</strong> The email address you have entered is already registered.
                    </div>';
                    }elseif($_GET['error'] == 'phone'){
                        echo '<div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Error!</strong> The mobile number you have entered is already registered.
                    </div>';
                    }elseif($_GET['error'] == 'enp'){
                        echo '<div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Error!</strong> The email address you have entered is already registered.
                    </div>';
                        echo '<div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Error!</strong> The mobile number you have entered is already registered.
                    </div>';
                    }else{
                        echo '';
                    }

                }
            ?>
            
        </div>
    <div class="card shadow mb-4">
      <div class="card-header">
        <h6 class="m-0 font-weight-bold text-primary">New subscriber</h6>
      </div>
      <div class="card-body">
      <form action="" method="post" class="form-horizontal">

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="name">Name *</label>
        <input type="text" class="form-control" id="name" placeholder="Name" name="name" >
    </div>
    <div class="form-group col-md-6">
        <label for="so">S/o, D/o, C/o *</label>
        <input type="text" class="form-control" name="so" id="so" placeholder="Son of/ Daughter of/ Care of" >
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="house">House *</label>
        <input type="text" class="form-control" id="house" name="house" placeholder="House" >
    </div>
    <div class="form-group col-md-6">
        <label for="place">Place  *</label>
        <input type="text" class="form-control" id="place" name="place" placeholder="Place" >
    </div>
</div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Valid email adress">
            <div class="mt-1">
                <span id="gen"><small><span class="fa fa-circle-notch fa-fw"></span> Generate fake email</span></small>
            </div>
        </div>
        <div class="form-group col-md-6">
            <label for="phone">Phone  *</label>
            <input type="text" class="form-control" id="phone" name="phone" placeholder="Contact number" >
        </div>
</div>


<div class="form-row">
    <div class="form-group col-md-6">
        <label for="district">Dis­trict *</label>
        <input placeholder="District" type="text" class="form-control" name="district" id="district" >
    </div>
    <div class="form-group col-md-6">
        <label for="post">P.O *</label>
        <input placeholder="Post office" type="text" class="form-control" name="post" id="post" >
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="rms"><span class="caps">RMS</span> *</label>
        <input placeholder="RMS" type="text" class="form-control" id="rms" name="rms" >
    </div>
    <div class="form-group col-md-6">
        <label for="pincode"><span class="caps">PIN</span> *</label>
        <input placeholder="Pincode" type="text" class="form-control" id="pincode" name="pincode" >
    </div>
</div>
<div class="form-group subs">
    <label for="plan">Sub­scrip­tion *</label>
    <div class="form-row">
        <div class="col-md-3">
            <select name="type" id="type"  class="form-control" required>
                <option value="">Select type</option>
                <option value="print">Print</option>
                <option value="digital">Digital</option>
            </select>
        </div>
        <div class="col-md-6">
            <select name="duration" id="duration" class="form-control" required>
                <option value="">Select duration</option>
                <option value="year">Year</option>
                <option value="month">Month</option>
            </select>
        </div>
        <div class="col-md-3" id="period-div">
            <input type="number" min="0" id="period" class="form-control" name="period" required>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="d-flex justify-content-center">
        <label for="total" class="control-form mr-2" style="line-height: 55px;">Total <i class="fa fa-rupee-sign"></i></label>
             <h1 class="bordered w-25 text-center" id="total">0</h1>
             <input type="hidden" name="total" value="0" id="totali">
             <input type="hidden" name="payment" value="0" id="payment">
             <span class="loader"></span>
    </div>
</div>
<hr>
<span class="promoStatus">Have a promo code? Apply</span> <span id="promoBtn"style="cursor:pointer" role="button" data-toggle="modal" data-target="#promoModal"><b>here</b></span>
<input type="hidden" name="promo" id="promo">
<!-- Promo Modal-->
<div class="modal fade" id="promoModal" tabindex="-1" role="dialog" aria-labelledby="promoModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="promoModalLabel">Apply promo code</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                </div>
                <div class="modal-body">If you have a valid promo code apply here, else contact the authority to get one.<br>
                    <input type="text" name="promoCode" id="promoCode" class="form-control col-md-3 mx-auto mt-2" placeholder="Eg: TH000">

                </div>
                <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <!-- <button type="button" class="btn btn-primary" id="#apply">Apply</button> -->
                <span class="btn btn-primary" id="apply">Apply</span>
                </div>
            </div>
            </div>
        </div>

        <!-- Model end -->

        <div class="form-group mt-2">
            <div class="promoInserted">
                <span id="prm"></span>
            </div>
        </div>
        <div class="form-group mt-4">
            <input type="hidden" name="submit">
            <button id="add" type="submit" class="btn btn-primary" name="submit"><i class="fa fa-plus fa-fw"></i> Add subscriber</button>
        </div>
    </form>
      </div>
    </div>
  </div>
</div>

<!-- Content Row -->



<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('../includes/footer.php'); ?>

<script src="../js/price_calculator.js"></script>

<script>
  $(function(){
      if($('#prm').text() == ""){
          $('#prm').hide();
      }else{
          $('#prm').show();
      }

      $('#apply').click(function(){
          var promoModal = $("#promoModal");
          var pormoCode = promoModal.find('#promoCode').val();
          var prm = $('#prm');

          if(pormoCode !== ''){
              prm.text(pormoCode);
              promoModal.find('#promoCode').empty();
              $('#promoModal').modal('hide');
              $('.promoStatus').text('Promo code inserted. ');
              $('#promoBtn b').text("Change");
              prm.show();
          }else{
              prm.empty();
              $('.promoStatus').text('Have a promo code? Apply ');
              $('#promoBtn b').text("here");
              $('#prm').hide();
          }
      });

    $('#duration').change(function(){
        var period = $('#period');

        if($(this).val() == 'year'){
            period.attr('placeholder', 'Year');
        }
        
        if($(this).val() == 'month'){
            period.attr('placeholder', 'Month');
        }

        if($(this).val() == ''){
            period.attr('placeholder', '');
        }


    });


    $('#gen').click(function(){
        var name = $('#name').val();
        var fkemail = '@thelicham.com';
        var email = $('#email');

        if(name !== ''){
            name = name.toLowerCase();
            name = name.replace(/\s+/g, '');
            fkemail = name + fkemail;
        }

        $(this).find('.fa-circle-notch').addClass('fa-spin');

        if(email.val(fkemail)){
            $(this).find('.fa-circle-notch').removeClass('fa-spin');
        }
    });
      
  });
</script>
