<?php include('../includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("../login.php"); } ?>

<?php
$name = "";
$so = "";
$house = "";
$place = "";
$email = "";
$phone = "";
$rms = "";
$district = "";
$post = "";
$pincode = "";
$type = "";
$duration = "";
$period = "";
$total = "";

if(isset($_GET['id']) || !empty($_GET['id'])){
    $id = $_GET['id'];
    $_SESSION['subid'] = $id;
    $subscriber = Subscriber::findById($id);

    
        $name = $subscriber->name;
        $so = $subscriber->so;
        $house = $subscriber->house;
        $place = $subscriber->place;
        $email = $subscriber->email;
        $phone = $subscriber->phone;
        $rms = $subscriber->rms;
        $district = $subscriber->district;
        $post = $subscriber->post;
        $pincode = $subscriber->pincode;
        $type = $subscriber->type;
        $duration = $subscriber->duration;
        $period = $subscriber->period;
        $total = $subscriber->total;
        $created_at = $subscriber->created_at;
        $payment = $subscriber->payment;
        $promo = $subscriber->promo;
  
  
        if($period > 1){
          $duration = $duration."s";
        }
}else{
    redirect("http://".$_SERVER['HTTP_HOST']."/manage/subscribers/create.php");
}

$url = "";
$curl = "";


if(isset($_POST['pay']) || isset($_POST['ref_no'])){
    $subId = $_POST['subid'];
    $ref_no = $_POST['ref_no'];
    $newType = $_POST['type'];
    $newPeriod = $_POST['period'];
    $newDuration = $_POST['duration'];
    $method_id = $_POST['methods'];

    if($newType == 'print'){
        $curl = "subscribers.php?success";
    }else{
        $curl = "subscribersDgt.php?success";
    }

    if(Subscription::hasSubscription($subId)){
        if(Subscription::updateSubscription($subId, $ref_no, $method_id, $newPeriod, $newDuration)){
            unset($_SESSION['subid']);
            redirect("http://".$_SERVER['HTTP_HOST']."/manage/".$curl);
        }else{
            redirect($_SERVER['PHP_SELF']. "?id=".$_SESSION['subid']."&failed");
        }
    }else{
        if(Subscription::addSubscription($subId, $ref_no, $method_id, $newPeriod, $newDuration) == true){
            unset($_SESSION['subid']);
            redirect("http://".$_SERVER['HTTP_HOST']."/manage/".$curl);
        }else{
            redirect($_SERVER['PHP_SELF']. "?id=".$_SESSION['subid']."&failed");
        }
    }
    
    
}



?>

<?php include('../includes/sidebar.php'); ?>

<?php include('../includes/topnav.php'); ?>


<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Add payment</h1>
</div>


<!-- Content Row -->
    <div class="row">
        <div class="col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Personal Details</h6>
                </div>
                <div class="card-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th width="200">Name</th>
                            <td><?php echo !empty($name) ? $name : 'NULL'; ?></td>
                        </tr>
                        <tr>
                            <th width="200">S/o, D/o, C/o</th>
                            <td><?php echo !empty($so) ? $so : 'NULL'; ?></td>
                        </tr>
                        <tr>
                            <th width="200">House</th>
                            <td><?php echo !empty($house) ? $house : 'NULL'; ?></td>
                        </tr>
                        <tr>
                            <th width="200">Place</th>
                            <td><?php echo !empty($place) ? $place : 'NULL'; ?></td>
                        </tr>
                        <tr>
                            <th width="200">Email</th>
                            <td><?php echo !empty($email) ? $email : 'NULL'; ?></td>
                        </tr>
                        <tr>
                            <th width="200">Phone</th>
                            <td><?php echo !empty($phone) ? $phone : 'NULL'; ?></td>
                        </tr>
                        <tr>
                            <th width="200">District</th>
                            <td><?php echo !empty($district) ? $district : 'NULL'; ?></td>
                        </tr>
                        <tr>
                            <th width="200">P.O</th>
                            <td><?php echo !empty($post) ? $post : 'NULL'; ?></td>
                        </tr>
                        <tr>
                            <th width="200">PIN</th>
                            <td><?php echo !empty($pincode) ? $pincode : 'NULL'; ?></td>
                        </tr>
                        <tr>
                            <th width="200">RMS</th>
                            <td><?php echo !empty($rms) ? $rms : 'NULL'; ?></td>
                        </tr>
                        
                        <!-- <tr>
                            <th width="200">Proccessing fee</th>
                            <td><?php // echo $pfee; ?></td>
                        </tr> -->
                    </tbody>
                </table>
                </div>
            </div>
        </div> <!-- End of col-8 -->

        <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-danger">Payment</h6>
                </div>
                <div class="card-body">
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                        <input type="hidden" name="type" value="<?php echo !empty($type) ? $type : 'print'; ?>">
                        <input type="hidden" name="period" value="<?php echo !empty($period) ? $period : '0'; ?>">
                        <input type="hidden" name="duration" value="<?php echo !empty($duration) ? $duration : 'month'; ?>">
                        <input type="hidden" name="subid" value="<?php echo isset($_GET['id']) ? $_GET['id'] : ''; ?>">

                    <table class="table">
                        <tbody>
                            <tr>
                                <th width="100">Subscription</th>
                                <td><?php echo '<span class="mr-2 badge'. ($type == 'print' ? ' badge-primary' : ' badge-warning').'">'.strtoupper($type) . '</span> <br>'. $period . ' '. $duration; ?></td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td><small><i class="fa fa-rupee-sign fa-fw"></i></small><?php echo !empty($total) ? $total : "NULL"; ?></td>
                            </tr>
                            <tr>
                                <th>Ref.No</th>
                                <td><input type="number" min="0" name="ref_no" id="ref_no" class="form-control" required></td>
                            </tr>
                            <tr>
                                <th colspan="2">Payment Method</th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <select name="methods" id="methods" class="form-control" required>
                                        <option value="">Select a method</option>
                                        <option value="1" selected>Direct</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="submit" value="Pay" class="btn btn-sm btn-block btn-info" name="pay">
                                </td>
                                <td><a href="../subscribers.php?success" class="btn btn-sm btn-block btn-secondary">Cancel</a></td>
                            </tr>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('../includes/footer.php'); ?>