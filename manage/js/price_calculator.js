$(function(){
    
   // total price calculator
   $('#period').on('change keyup', function(){
    var type = $('#type').val();
    var duration = $('#duration').val();
    var period = $(this).val();
    var total = $('#total');
    var price = 15;
    var totali = $('#totali');

    // console.log('Period changed');

    if($(this).val() >= 1){
        if(type == 'print'){
            
            price = 15;

            if(duration == 'year'){
                price = 150;
            }

            if(duration == 'month' && period == 6){
                price = 12.5;
            }

        }else{
            price = 10;
        }

        if(type == 'digital'){
            if(duration == 'month' && period == 1){
                price = 10;
            }

            if(duration == 'year'){
                price = 100;
            }
        }

        if(type == '' || duration == ''){
            price = 0;
            total.empty();
        }

        price = price * $(this).val();

        total.text(price);
        totali.val(price);
    }else{
        total.text(0);
    } 
    

});

$('#type').change(function(){
    var type = $(this).val();
    var duration = $('#duration').val();
    var period = $('#period').val();
    var price = 0;
    var total = $('#total');
    var totali = $('#totali');


    if(type !== '' && duration !== '' && period !== ''){
        if(type == 'print'){
            price = 15;

            if(duration == 'year'){
                price = 150;
            }

            if(duration == 'month' && period == 6){
                price = 12.5;
            }

        }else{
            price = 10;

            if(duration == 'year'){
                price = 100;
            }
        }

        

        if(period >= 1){
            price = price * period;
        }else{
            price = 0;
        }

        total.text(price);
        totali.val(price);
        
    }else{
        total.text(0);
    }
});

$('#duration').change(function(){
    var duration = $(this).val();
    var type = $('#type').val();
    var period = $('#period').val();
    var price = 0;
    var total = $('#total');
    var totali = $('#totali');

    if(type !== '' || period !== ''){
        if(type == 'print'){
            price = 15;

            if(duration == 'year'){
                price = 150;
            }

            if(duration == 'month' && period == 6){
                price = 12.5;
            }
        }else{
            price = 10;

            if(duration == 'year'){
                price = 100;
            }
        }

        

        if(period >= 1){
            price = price * period;
        }else{
            price = 0;
        }

        total.text(price);
        totali.val(price);
        
    }else{
        price = 0;
        total.text(price);
        totali.val(price);
    }

    if(duration == ''){
        price = 0;
        total.text(price);
        totali.val(price);
    }
});

});