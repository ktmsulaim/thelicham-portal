<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>

<?php
    //get issue details by id
    $id = '';
    if(isset($_GET['id'])){
        if($_GET['id'] !== ''){
            $id = $_GET['id'];
        }
    }
    $issue = Issue::findById($id);

    // get subscribers by issue id
    $subscribers = $db->query("SELECT s.id, s.name, s.phone FROM subscribers s INNER JOIN dispatch_history dh ON s.id = dh.subscriber_id WHERE dh.issue_id={$id} AND dh.type='group'");

?>

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">View</h1>
</div>

<!-- Content Row -->

<div class="row">
    <div class="col-md-8">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">Subscribers</h6>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if($subscribers->num_rows >= 1){
                                $i = 1;
                                foreach($subscribers as $subscriber) : ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $subscriber['id']; ?></td>
                                    <td><?php echo $subscriber['name']; ?></td>
                                    <td><?php echo $subscriber['phone']; ?></td>
                                    <td class="text-center"><a class="btn" href="subscribers/view.php?id=<?php echo $subscriber['id']; ?>">View</a></td>
                                </tr>
                            <?php 
                                $i++;
                                endforeach;
                            }else{
                                echo '<div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>No data!</strong> try again later.
                            </div>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-info">Issue</h6>
            </div>
            <div class="card-body">
                <?php
                    if($issue){
                        $issueImg = $issue->cover;
                        $issueMonth = $issue->month;
                        $issueYear = $issue->year;
                    }
                ?>
                <p class="text-center">
                    <img src="<?php echo $issueImg !== 'null' ? 'issues/'.$issueImg : 'issues/dummy.jpg'; ?>" alt="" width="150"><br>
                    <span><?php echo $issueMonth.' '.$issueYear ?></span>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Content Row -->



<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('includes/footer.php'); ?>
<script>
$(document).ready(function() {
  $('.table').DataTable();
});
</script>