<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>

<?php

$msg = "";

if(isset($_POST['add'])){
    $name = $db->escapeString($_POST['name']);
    $place = $db->escapeString($_POST['place']);
    $phone = $db->escapeString($_POST['phone']);
    $email = $db->escapeString($_POST['email']);

    $add = Subscriber::addInstitution($name, $place, $phone, $email);

    if($add){
        $msg = "Added successfully";
    }else{
        $msg = "Failed to add";
    }

    // if($add){
    //     redirect("insti.php");
    // }else{
    //     redirect("addInsti.php?error");
    // }
}

?>

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">New institution</h1>
</div>

<!-- Content Row -->

<!-- <div class="row"></div> -->

<!-- Content Row -->
<div class="row">
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add new institution</h6>
            </div>
            <div class="card-body">
                <form action="addInst.php" method="post">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" type="text" id="name" name="name" placeholder="Full name of institution" required>
                    </div>
                    <div class="form-group">
                        <label for="place">Place</label>
                        <input class="form-control" type="text" id="place" name="place" placeholder="Place of institution" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input class="form-control" type="text" id="phone" name="phone" placeholder="Contact number" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" type="email" id="email" name="email" placeholder="Valid email address" required>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-info" name="add" role="submit"><i class="fa fa-plus fa-fw"></i> Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php if($msg !== ''){ ?>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-success">Success</h6>
            </div>
            <div class="card-body">
                <div class="alert alert-success">Added successfully</div>
            </div>
        </div>

        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-info">Institution details</h6>
            </div>
            <div class="card-body">
                <table>
                    <tr>
                        <th>Name</th>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <?php
    }else{} ?>
</div>





<!-- Content Row -->


</div>
<!-- /.container-fluid -->

</div>

<?php include('includes/footer.php'); ?>