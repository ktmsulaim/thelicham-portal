<?php include('includes/head.php'); ?>
<?php if($session->isLoggedIn()){}else{ redirect("login.php"); } ?>
<style>
    ul.subs {
        margin-top: 15px;
        border: 1px solid #999;
        padding: 10px;
    }
    ul.subs li {
        padding: 8px;
        border-bottom: 1px solid #eee;
    }
    span#prm {
        padding: 8px;
        background: #eee;
        border-radius: 8px;
        font-weight: 600;
    }
    .bordered{
        border: 1px solid #999;
        padding: 5px;
        border-radius: 8px;
    }

    span.loader {
        padding: 10px;
        line-height: 30px;
    }
    
</style>
<?php
// grant for all subscribers
if(isset($_POST['grant']) || isset($_POST['ref_no'])){
    $subscribers = Subscriber::all();
    foreach($subscribers as $subscriber){
        if($subscriber->payment == 1){
            if(Subscription::hasSubscription($subscriber->id)){
                // echo "Already";
            }else{
                if(Subscription::addSubscription($subscriber->id, '', 1, $subscriber->period, $subscriber->duration)){
                    redirect($_SERVER['PHP_SELF']."?granted");
                }else{
                    redirect($_SERVER['PHP_SELF']."?failed=granted");
                }
            }
        }
    }
}

// delete subscription
if(isset($_POST['delete']) && isset($_POST['id'])){
    $id = $_POST['id'];
    $subs = Subscription::findById($id);
    if($subs->delete()){
        echo 1;
    }else{
        echo 0;
    }
}

// execute the MySQL query
$notifiable = Subscriber::query("SELECT subscribers.*, subscriptions.credits_left FROM subscribers LEFT JOIN subscriptions ON subscribers.id = subscriptions.subscriber_id WHERE subscriptions.credits_left < 3");



$notiCount = count($notifiable);

?>

<?php include('includes/sidebar.php'); ?>

<?php include('includes/topnav.php'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Subscriptions</h1>
</div>

<!-- Content Row -->
<div class="row">

</div>
<div class="row">
    <div class="col-md-8">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">Alerts <small>Less than 2 credits left</small></h6>
            </div>
            <div class="card-body">
                <?php if($notiCount >= 1){ ?>
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Credits</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach($notifiable as $sub){ ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $sub->name; ?></td>
                            <td><?php echo $sub->email; ?></td>
                            <td><?php echo $sub->phone; ?></td>
                            <td><?php echo $sub->credits_left; ?></td>
                            <td><a href="subscribers/view.php?id=<?php echo $sub->id; ?>"><i class="fa fa-eye"></i></a></td>
                        </tr>
                      <?php $i++;  } ?>
                    </tbody>
                </table>
                <?php 
                 }else{ ?>

                    
                        <h4 class="text-center">Great!</h4>
                        <p class="text-center">No subscriber has to be notified</p>
                 <?php } ?>
            </div>
        </div>


        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add / Update subscription</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; transform: translate3d(-156px, 19px, 0px); top: 0px; left: 0px; will-change: transform;">
                      <div class="dropdown-header">Action:</div>
                      <a class="dropdown-item" href="subscribers.php"><i class="fa fa-search mr-1"></i>Get subscriber id</a>
                      <a class="dropdown-item" href="subscription.php"><i class="fa fa-circle-notch mr-1"></i>Search again</a>
                    </div>
                  </div>
                </div>
            <div class="card-body">
                <?php if(isset($_GET['id'])){
                    $id = $_GET['id'];
                    $subscriber = Subscriber::findById($id);
                    if($subscriber){
                            $subId = $subscriber->id;
                            $name =  $subscriber->name; 
                            $email =  $subscriber->email; 
                            $phone =  $subscriber->phone; 
                            $place =  $subscriber->place;
                            $period = $subscriber->period; 
                            $duration = $subscriber->duration; 
                            $total = $subscriber->total; 
                            $type = $subscriber->type; 

                    
                ?>
                <table class="table mb-2" id="dataTable">
                    <tbody>
                        <tr>
                            <th>Name</th>
                            <td><?php echo $name; ?></td>
                            <th>Place</th>
                            <td><?php echo $place; ?></td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td><?php echo $phone; ?></td>
                            <th>Email</th>
                            <td><?php echo $email; ?></td>
                        </tr>
                    </tbody>
                </table>

                <?php }else{
                    echo '<h4 class="text-center mb-2">No subscriber found!</h4>';
                    echo '<form action="subscription.php" method="GET">
                    <label for="id">Enter subscriber id</label>
                    <div class="input-group">
                        <input type="text" id="id" name="id" class="form-control bg-light border-0 small" placeholder="Get subscriber" required>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>';
                } 
                
                //check subscription
                $subscription = Subscription::getSubscription($id);
                // get subscription
                $packages = $db->query("SELECT duration, period, type FROM subscribers WHERE id = {$id}");
                if($packages->num_rows == 1){
                    foreach($packages as $pkg){
                        if($pkg['period'] > 1){
                            $pkg['duration'] = $pkg['duration'].'s';
                        }
                        $subs = $pkg['period'].' '.$pkg['duration'].' '.$pkg['type'];
                    }
                }
                if($subscription->num_rows == 1){ ?>
                    <h6 class="font-weight-bold mt-2">Subscription</h6>
                    <ul class="container list-group">
                        
               <?php     foreach($subscription as $sbc){
                            echo '<li class="list-group-item text-danger"><div class="row"><div class="col-md-6">Receipt number</div> <div class="col-md-6">'.$sbc['ref_no'].'</div></div></li>';
                            echo '<li class="list-group-item"><div class="row"><div class="col-md-6">Valid from</div> <div class="col-md-6">'.date('j M, Y', strtotime($sbc['subscription_date'])).'</div></div></li>';
                            echo '<li class="list-group-item"><div class="row"><div class="col-md-6">Subscription</div> <div class="col-md-6">'.$subs.'</div></div></li>';
                            echo '<li class="list-group-item"><div class="row"><div class="col-md-6">Expiry date</div> <div class="col-md-6">'.date('j M, Y', strtotime($sbc['expiry_date'])).'</div></div></li>';
                            echo '<li class="list-group-item"><div class="row"><div class="col-md-6">Total credits</div> <div class="col-md-6">'.$sbc['credits'].'</div></div></li>';
                            echo '<li class="list-group-item"><div class="row"><div class="col-md-6">Credits left</div> <div class="col-md-6">'.$sbc['credits_left'].'</div></div></li>';
                            echo '<li class="list-group-item"><div class="row"><div class="col-md-6">Status</div> <div class="col-md-6"><span class="badge '.($sbc['status'] == 1 ? 'badge-success' : 'badge-danger').'">'.($sbc['status'] == 1 ? 'Active' : 'Not active').'</span></div></div></li>';
                    } ?>
                    
                    </ul>
                    <span title="Update subscription" class="float-right btn btn-primary mt-4" style="cursor:pointer" role="button" data-toggle="modal" data-target="#updateModal"><i class="fa fa-edit mr-1"></i> Update subscription</span>
               <?php  }elseif($subscription->num_rows > 1){
                        $subscriptions = Subscription::getBySubscriberId($id);
                        if(count($subscriptions) > 0){
                   ?>
                        <ul class="list-group">
                            <?php foreach($subscriptions as $sb) : ?>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <span><b>Credits</b></span><br>
                                            <p><?php echo $sb->credits; ?></p>
                                        </div>
                                        <div class="col-md-3">
                                        <span><b>Credits Left</b></span><br>
                                        <p><?php echo $sb->credits_left; ?></p>
                                        </div>
                                        <div class="col-md-3">
                                        <span><b>Subscription Date</b></span><br>
                                        <?php echo $sb->subscription_date; ?>
                                        </div>
                                        <div class="col-md-3">
                                        <span><b>Expiry Date</b></span><br>
                                        <?php echo $sb->expiry_date; ?>
                                        </div>
                                        <div class="col-md-1">
                                            <span id="delete" class="delete" style="cursor:pointer">
                                                <i class="fa fa-minus-circle"></i>
                                                <input type="hidden" name="sbid" id="sbid" value="<?php echo $sb->id; ?>">
                                            </span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
            <?php  } 
                }else{
                    echo '<h6 class="font-weight-bold mt-4 text-danger text-center mt-2 mb-2">No subscription found!</h6>';
                    echo '<span title="Update subscription" class="float-right btn btn-primary" style="cursor:pointer" role="button" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus mr-1"></i>Add subscription</span>';
                    }
                
                }else{ ?>
                    <form action="subscription.php" method="GET">
                        <label for="id">Enter subscriber id</label>
                        <div class="input-group">
                            <input type="text" id="id" name="id" class="form-control bg-light border-0 small" placeholder="Get subscriber" required>
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form>

                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card border-bottom-primary shadow py-2 mb-4">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Subscribers to notify</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $notiCount; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                    </div>
                </div>
            </div>
        <div class="card shadow py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Grant subscription</div>
                            <p class="small text-muted">Grant subscription to all payed subscribers having no subscription yet.</p>
                            <h4><?php $nsb = Subscriber::hasNoSubscription(); echo $nsb->num_rows; ?> <span class="small text-muted ml-2">Subscribers</span></h4>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                <form action="" method="post">
                                    <input type="hidden" name="ref_no" value="">
                                    <button id="grant" type="submit" class="btn btn-sm btn-primary" name="grant">SET</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="clearfix"></div>
</div>

<!-- Content Row -->
<?php if(isset($_GET['id'])){ 
    if($subscription->num_rows == 1){    
?>
<!-- Subscription update Modal-->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
              <h5 class="modal-title" id="updateModalLabel">Update subscription</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
              </div>
              <form action="update_subscription.php" method="post">
                <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                <div class="modal-body">
                    <div class="form-group subs">
                        <label for="plan">Sub­scrip­tion *</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php
                                        if(isset($_GET['id'])){
                                            $subscipt = Subscription::findBySubscriberId($_GET['id']);
                                            if($subscipt){
                                                $ref_no = $subscipt->ref_no;
                                            }
                                        }
                                    ?>
                                    <label for="ref_no">Receipt number</label>
                                    <input type="text" class="form-control" name="ref_no" value="<?php echo $ref_no; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-3">
                                <select name="type" id="type"  class="form-control" required>
                                    <option value="">Select type</option>
                                    <option value="print" <?php echo $type == 'print' ? 'selected' : ''; ?>>Print</option>
                                    <option value="digital" <?php echo $type == 'digital' ? 'selected' : ''; ?>>Digital</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <select name="duration" id="duration" class="form-control" >
                                    <option value="">Select duration</option>
                                    <option value="year" <?php echo $duration == 'year' ? 'selected' : ''; ?>>Year</option>
                                    <option value="month" <?php echo $duration == 'month' ? 'selected' : ''; ?>>Month</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <input type="number" value="<?php echo $period; ?>" id="period" class="form-control" name="period" placeholder="Number of months / years" >
                            </div>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-center">
                            <label for="total" class="control-form mr-2" style="line-height: 55px;">Total <i class="fa fa-rupee-sign"></i></label>
                                <h1 class="bordered w-25 text-center" id="total"><?php echo $total == 0 ? 0 : $total; ?></h1>
                                <input type="hidden" name="total" value="<?php echo $total == 0 ? 0 : $total; ?>" id="totali">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="pwd">Enter password *</label>
                        <input type="password" name="pwd" id="pwd" placeholder="Enter admin password" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                <button class="btn btn-primary" type="submit" name="update">Update</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                </div>
              </form>
          </div>
          </div>
      </div>

<?php }else{?>
<!-- Subscription add Modal-->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
              <h5 class="modal-title" id="addModalLabel">Add subscription</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
              </div>
              <form action="add_subscription.php" method="post">
                <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                <div class="modal-body">
                    <div class="form-group subs">
                        <label for="plan">Sub­scrip­tion *</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="ref_no">Receipt number</label>
                                    <input type="text" class="form-control" name="ref_no">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-3">
                                <select name="type" id="type"  class="form-control" required>
                                    <option value="">Select type</option>
                                    <option value="print">Print</option>
                                    <option value="digital">Digital</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <select name="duration" id="duration" class="form-control" >
                                    <option value="">Select duration</option>
                                    <option value="year">Year</option>
                                    <option value="month">Month</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <input type="number" id="period" class="form-control" name="period" placeholder="Number of months / years" >
                            </div>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-center">
                            <label for="total" class="control-form mr-2" style="line-height: 55px;">Total <i class="fa fa-rupee-sign"></i></label>
                                <h1 class="bordered w-25 text-center" id="total">0</h1>
                                <input type="hidden" name="total" value="0" id="totali">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="pwd">Enter password *</label>
                        <input type="password" name="pwd" id="pwd" placeholder="Enter admin password" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                <button class="btn btn-primary" type="submit" name="add"><i class="fa fa-plus mr-2"></i>Add</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                </div>
              </form>
          </div>
          </div>
      </div>

<!-- Content Row -->
<?php } }?>

</div>
<!-- /.container-fluid -->

</div>

<?php include('includes/footer.php'); ?>
<script src="js/price_calculator.js"></script>
<script>
$(document).ready(function() {
  $('#dataTable').DataTable();

//   remove subscription
$('.delete').click(function(){
        var id = $(this).find('#sbid').val();
        var list = $(this).closest('li');
        $.ajax({
            url: "subscription.php",
            method: "POST",
            data: {delete:'delete', id: id}
        }).done(function(data){
                list.remove();
                window.location.reload();
        });
    });

    // alert button
    $('#grant').click(function(){
        return confirm("Are you sure to grant subscriptions to all payed subscribers?");
    });
});
</script>