<?php

define('DS', DIRECTORY_SEPARATOR);
define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT'].'manage');

require_once(SITE_ROOT.'/classes/config.php');
require_once(SITE_ROOT.'/classes/database.php');
require_once(SITE_ROOT.'/classes/model.php');
require_once(SITE_ROOT.'/classes/user.php');
require_once(SITE_ROOT.'/classes/session.php');
require_once(SITE_ROOT.'/classes/subscriber.php');
require_once(SITE_ROOT.'/classes/subscription.php');
require_once(SITE_ROOT.'/classes/dispatch.php');
require_once(SITE_ROOT.'/classes/pincode.php');
require_once(SITE_ROOT.'/classes/institution.php');
require_once(SITE_ROOT.'/classes/issue.php');
require_once(SITE_ROOT.'/classes/pagination.php');
require_once(SITE_ROOT.'/includes/functions.php');

?>