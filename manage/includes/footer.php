<!-- Footer -->
<footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Thelicham Monthly <?php echo date('Y'); ?> </span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div> 

  <!-- Bootstrap core JavaScript-->
  <script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/js/jquery.min.js"></script>
  <script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.min.js"></script>
  

  <!-- Core plugin JavaScript-->
  <script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/js/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/js/Chart.min.js"></script>
  <script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/js/jquery.dataTables.min.js"></script>
  <script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/js/dataTables.bootstrap4.min.js"></script>

  <script>
    $(function(){
      $('form').submit(function() {
        $(this).find("button[type='submit'], input[type='submit']").attr('disabled','disabled');
        $(this).find("input[type='submit']").val('Working...');
        $(this).find("button[type='submit']").text('Working...');
      });
    });
  </script>

  <!-- Page level custom scripts -->


</body>

</html>