<?php

function autoLoader($class){
    $class = strtolower($class);
    $path = "classes/{$class}.php";

    if(file_exists($path)){
        require_once($path);
    }else{
        die("The file name {$class}.php cannot find");
    }
}
spl_autoload_register('autoLoader');

function redirect($url){
    header("Location: {$url}");
}

?>