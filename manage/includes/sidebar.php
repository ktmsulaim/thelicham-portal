<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gray-900 sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" target="_blank" href="http://subscribe.thelicham.com">
        <div class="sidebar-brand-icon rotate-n-15">
          <!-- <i class="fas fa-laugh-wink"></i> -->
          <b>T</b>
        </div>
        <div class="sidebar-brand-text mx-3">Thelicham Monthly</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Main 
      </div>
      
      <?php

        $role = "";
        if($session){
          $currentUser = User::findById($session->userId);
          if($currentUser){
            $role = $currentUser->is_admin;
          }
        }

      ?>

      <?php if($role == 1){ ?>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-users"></i>
          <span>Subscribers</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Print edition:</h6>
            <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers/create.php">New subscriber</a>
            <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers.php">All subscribers</a>
            <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers.php?active">Active subscribers</a>
          </div>
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Digital edition:</h6>
            <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers/create.php">New subscriber</a>
            <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribersDgt.php">All subscribers</a>
            <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribersDgt.php?active">Active subscribers</a>
          </div>
        </div>
      </li>

      <!-- subscription -->

      <li class="nav-item">
        <a class="nav-link" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscription.php">
          <i class="fas fa-address-card"></i>
          <span>Subscriptions</span></a>
      </li>
      <!-- Pincodes -->

      <li class="nav-item">
        <a class="nav-link" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/pincodes.php">
          <i class="fas fa-sort-numeric-down"></i>
          <span>Pincodes</span></a>
      </li>

      <!-- Dispatch -->

      <li class="nav-item">
        <a class="nav-link" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/dispatch.php">
          <i class="fas fa-fw fa-truck"></i>
          <span>Dispatch</span></a>
      </li>

      <!-- Print -->

      <li class="nav-item">
        <a class="nav-link" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/printMgmt.php">
          <i class="fas fa-fw fa-print"></i>
          <span>Print</span></a>
      </li>

      
      <!-- Issues -->

      <li class="nav-item">
        <a class="nav-link" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/issues.php">
          <i class="fas fa-fw fa-file"></i>
          <span>Issues</span></a>
      </li>



      

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Compania '19
      </div>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-cog"></i>
          <span>Competitions</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Items:</h6>
            <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/insti.php">Institutions</a>
          </div>
        </div>
      </li>

      <?php }else{ 
        // Restrict pages 
        $allowed = array('index.php', 'subscribers.php', 'subscribersDgt.php','create.php', 'edit.php', 'view.php', 'delete.php', 'pay.php', 'withdrawIssue.php');
        if(in_array(basename($_SERVER['PHP_SELF']), $allowed)){

        }else{
          redirect('index.php');
        }
        
        ?>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-users"></i>
            <span>Subscribers</span>
          </a>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">Print edition:</h6>
              <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers/create.php">New subscriber</a>
              <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers.php">All subscribers</a>
              <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers.php?active">Active subscribers</a>
            </div>
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">Digital edition:</h6>
              <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribers/create.php">New subscriber</a>
              <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribersDgt.php">All subscribers</a>
              <a class="collapse-item" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/subscribersDgt.php?active">Active subscribers</a>
            </div>
          </div>
        </li>
      <?php } ?>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

      <div class="mt-3 text-center">
        <span class="badge badge-pill badge-secondary">v 1.0</span>
      </div>

    </ul>