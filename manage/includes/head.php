<?php require_once('init.php'); ?>
<?php ob_start(); ?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Thelicham Monthly Subscribers | Manage</title>

  <!-- Custom fonts for this template-->
  <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/manage/css/sb-admin-2.min.css" rel="stylesheet">

  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

</head>