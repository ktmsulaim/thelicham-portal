<?php
$MERCHANT_KEY = "dAssb6h0";
$SALT = "YSH7jatBJG";
// Merchant Key and Salt as provided by Payu.

// $PAYU_BASE_URL = "https://sandboxsecure.payu.in";		// For Sandbox Mode
$PAYU_BASE_URL = "https://secure.payu.in";			// For Production Mode

$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
	
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}



    $id = "";
    $name = "";
    $so = "";
    $house = "";
    $place = "";
    $email = "";
    $phone = "";
    $district = "";
    $post = "";
    $pincode = "";
    $type = "";
    $duration = "";
    $period = "";
    $total = "";
    $productInfo = "Thelicham Monthly";
    $pfee = 0;
    $amount = "";

    

    if(isset($_GET['submit']) && isset($_GET['id'])){

        require_once("db.php");

        $id = $_GET['id'];
        $subscribers = $db::findById($id);

        while($row = mysqli_fetch_array($subscribers)){
            $name = $row['name'];
            $so = $row['so'];
            $house = $row['house'];
            $place = $row['place'];
            $email = $row['email'];
            $phone = $row['phone'];
            $district = $row['district'];
            $post = $row['post'];
            $pincode = $row['pincode'];
            $type = $row['type'];
            $duration = $row['duration'];
            $period = $row['period'];
            $total = $row['total'];
        }
        
        // $grand_total = trim($_GET['amount']);
        $pfee = ($total * 2) / 100;

        //check subscription
        if(Database::checkSubscription($id)){
            $creditsCheck = $db::query("SELECT * FROM subscription WHERE subscriber_id = {$id}");
            
        }else{
            echo 'has no subscription';
            $creditsCheck = "";
        }
    }

    if(isset($_POST['pay'])){

        require_once('db.php');
        
        session_start();

        $_SESSION['sub_id'] = $_POST['id'];
        $_SESSION['method_id'] = 3;
        $_SESSION['period'] = $_POST['period'];
        $_SESSION['duration'] = $_POST['duration'];
        $_SESSION['status'] = 0;

    }


?>


<!DOCTYPE html>
<html lang="en">
    <head>
    
        <title>Thelicham Online | subscribe</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="style.css">

        <link rel="icon" href="img/favicon.ico" type="image/x-icon">
        

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,900&display=swap" rel="stylesheet">
        </head>

        <script>
            var hash = '<?php echo $hash ?>';
            function submitPayuForm() {
            if(hash == '') {
                return;
            }
            var payuForm = document.forms.payuForm;
            payuForm.submit();
            }
        </script>
    <body>

        <header class="secondary" id="pay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="img/2.png" width="200" alt="">
                            <table class="float-right text-white">
                                    <tr>
                                        <td width="30px"><i class="fa fa-phone"></i></td>
                                        <td><a href="tel:+91 98473 33306">+91 98473 33306</a></td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><i class="fa fa-envelope"></i></td>
                                        <td><a href="mailto:info@thelicham.com">info@thelicham.com</a></td>
                                    </tr>
                                </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center">Pay</h1>
                    </div>
                </div>
            </div>
        </header>
        <main>
            <section class="section" id="payment">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="pay-box form-holder">
                                <div class="form-header">
                                    <h4 class="form-title text-center mb-4">Confirm details</h4>
                                    <p class="form-tip">Make sure you made it correctly</p>
                                </div>
                                <div class="form-body">
                                
                                <?php 
                                    if($creditsCheck->num_rows >= 1){
                                        foreach($creditsCheck as $cr){
                                            $valid = $cr['subscription_date'];
                                            $expiry = $cr['expiry_date'];
                                            $cr_left = $cr['credits_left'];
                                        } 

                                        echo '<p class="note">The subscriber has already a subscription valid from <b>'.date('F j, Y', strtotime($valid)).'</b> and expires on <b>'.date('F j, Y', strtotime($expiry)).'</b>. The remaining credits: <b>'.$cr_left.'</b>.</p>';
                                    }else{
                                        $valid = "";
                                        $expiry = "";
                                        $cr_left = "";
                                    }
                                    
                                
                                ?>

                                <?php if($formError) { ?>
                                    <span style="color:red">Please fill all mandatory fields.</span>
                                    <br/>
                                    <br/>
                                <?php } ?>
                                <div class="table-holder">
                                    <form action="<?php echo $action; ?>" method="post" name="payuForm">
                                        <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
                                        <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                                        <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />


                                        <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? $productInfo : $posted['productinfo'] ?>">
                                        <input type="hidden" name="surl" value="<?php echo (empty($posted['surl'])) ? 'http://'.$_SERVER['HTTP_HOST'].'/success.php' : $posted['surl'] ?>" size="64" />
                                        <input type="hidden" name="furl" value="<?php echo (empty($posted['furl'])) ? 'http://'.$_SERVER['HTTP_HOST'].'/cancel.php' : $posted['furl'] ?>" size="64" />
                                        <input type="hidden" name="curl" value="<?php echo (empty($posted['curl'])) ? 'http:/'.$_SERVER['HTTP_HOST'].'/cancel.php' : $posted['curl'] ?>" size="64" />
                                        <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
                                        <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                        <input type="hidden" name="period" value="<?php echo $period; ?>" />
                                        <input type="hidden" name="duration" value="<?php echo $duration; ?>" />

                                    <?php if(isset($_GET['submit'])){ ?>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th width="200">Name</th>
                                            <td><input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? $name : $posted['firstname']; ?>" /><?php echo ucfirst($name); ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">S/o, D/o, C/o</th>
                                            <td><input type="hidden" name="so" value="<?php echo $so; ?>"><?php echo $so; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">House</th>
                                            <td><input type="hidden" name="house" value="<?php echo $house; ?>"><?php echo $house; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">Place</th>
                                            <td><input type="hidden" name="place" value="<?php echo $place; ?>"><?php echo $place; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">Email</th>
                                            <td><input type="hidden" name="email" id="email" value="<?php echo  $email; ?>" /><?php echo $email; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">Phone</th>
                                            <td><input type="hidden" name="phone" value="<?php echo  $phone; ?>" /><?php echo $phone; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">District</th>
                                            <td><input type="hidden" name="district" value="<?php echo $district; ?>"><?php echo $district; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">P.O</th>
                                            <td><input type="hidden" name="post" value="<?php echo $post; ?>"><?php echo $post; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">PIN</th>
                                            <td><input type="hidden" name="pincode" value="<?php echo $pincode; ?>"><?php echo $pincode; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">Subscription</th>
                                            <td><?php echo '<span class="mr-2 badge'. ($type == 'print' ? ' badge-primary' : ' badge-warning').'">'.strtoupper($type) . '</span> '. $period . ' '. $duration; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">Amount</th>
                                            <td><?php echo '<i class="fa fa-rupee-sign small"></i>'.$total; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">Proccessing fee</th>
                                            <td><i class="fa fa-rupee-sign small"></i><?php echo $pfee; ?></td>
                                        </tr>
                                        <tr>
                                            <th width="200">Total</th>
                                            <td><big>
                                                <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? ($total+$pfee) : $posted['amount'] ?>" />
                                                <?php echo '<i class="fa fa-rupee-sign small"></i>'. ($total+$pfee); ?></big></td>
                                        </tr>
                                        
                                        </tbody>
                                    </table>
                                    <div class="btn-holder">
                                    <?php if(!$hash) { ?>
                                        <button class="btn btn-modern" role="submit" name="pay"><i class="fa fa-credit-card mr-1"></i> Pay now</button>
                                    <?php }else{ ?>
                                        <button class="btn btn-modern disabled" disabled role="submit" name="pay"><i class="fas fa-circle-notch fa-spin"></i> Paying...</button>
                                    <?php } ?>
                                    </div>

                                    </form>
                                    <?php  }else{?>

                                        <table class="table">
                                            <tr>
                                                <th class="text-center">Please fill the form to proceed.</th>
                                            </tr>
                                        </table>
                                    <?php } ?>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        
        <?php include_once('footer.php'); ?>


    
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/jquery.inputmask.min.js"></script>
        <script src="main.js"></script>

        <script>
            $(function(){
                submitPayuForm();
            });
        </script>


    </body>
</html>