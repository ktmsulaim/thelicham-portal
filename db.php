<?php

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'subscribers');

class Database{

    public static $connection;

    function __construct()
    {
        self::connect();
    }



    public static function connect(){
       self::$connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

       if(self::$connection){
        // echo "success";
        }else{
            die("connection failed. ". self::$connection->connect_error());
        }
    }

    public static function query($sql){
       $results = self::$connection->query($sql);
       return $results;
    }

    public static function findById($id){
        $sql = "SELECT * FROM subscribers WHERE id = {$id}";
        return self::query($sql);
    }

    public static function verifyEmail($email){
        $sql = "SELECT * FROM subscribers WHERE email = '{$email}'";
        $results = self::query($sql);
        
        if($results->num_rows > 1){
            return false;
        }else{
            return true;
        }
    }

    public static function verifyPhone($phone){
        $sql = "SELECT * FROM subscribers WHERE phone = '{$phone}'";
        $results = self::query($sql);

        if($results->num_rows > 1){
            return false;
        }else{
            return true;
        }
    }

    public static function addSubscriber($name, $so, $house, $place, $email, $phone, $district, $post, $pincode, $period, $type, $duration, $total, $promo){
        date_default_timezone_set('Asia/Kolkata');
        $date = date('Y-m-d h:i:s');

        $sql = "INSERT INTO subscribers(`name`, `so`, `house`, `place`, `email`, `phone`, `district`, `post`, `pincode`, `period`, `type`, `duration`, `total`, `promo`,`created_at`) ";
        $sql .= "VALUES('{$name}', '{$so}', '{$house}', '{$place}', '{$email}', '{$phone}', '{$district}', '{$post}', '{$pincode}', '{$period}', '{$type}', '{$duration}', '{$total}', '{$promo}', '{$date}')";

        $results = self::query($sql);

        if($results){
                session_start();

                $pfee=($total * 2) / 100;

                $_SESSION['name'] = $name;
                $_SESSION['so'] = $so;
                $_SESSION['house'] = $house;
                $_SESSION['place'] = $place;
                $_SESSION['email'] = $email;
                $_SESSION['phone'] = $phone;
                $_SESSION['district'] = $district;
                $_SESSION['post'] = $post;
                $_SESSION['pincode'] = $pincode;
                $_SESSION['type'] = $type;
                $_SESSION['duration'] = $duration;
                $_SESSION['period'] = $period;
                $_SESSION['total'] = $total;
                $_SESSION['pfee']  = $pfee;
                $_SESSION['grand_total'] = ($total + $pfee);
                $_SESSION['promo'] = $promo;

                $insert_id = mysqli_insert_id(self::$connection);


            self::redirect("thankyou.php?added&id=".$insert_id);
        }else{
            die('Failed '.self::$connection->error);
            session_unset();
        }
    }

    public function escapeString($string){
        $escaped = self::$connection->real_escape_string($string);
        return $escaped;
    }

    public static function redirect($url){
        header("Location: {$url}");
    }

    public static function currentIssue(){
        $result = self::query("SELECT issue_id FROM current_issue");
        if($result->num_rows >= 1){
            foreach($result as $cid){
                $id = $cid['issue_id'];
            }

            $fetch = self::query("SELECT * FROM issues WHERE id = {$id}");
            if($fetch->num_rows >= 1){
                return $fetch;
            }
        }else{
            return false;
        }
        
    }

     //update payment info
     public static function makePayment($subId){

        if(self::query("UPDATE subscribers SET payment = 1 WHERE id = {$subId}")){
            return true;
        }else{
            return false;
        }
    }


    //add subscription
    public static function addSubscription($subId, $method_id, $taxid, $period, $duration){

        date_default_timezone_set('Asia/Kolkata');
        $now = date('Y-m-d', strtotime('now'));

        $credits = 0;
        $status = 0;

        $date = new DateTime($now);

        $newDate = date_format($date, 'Y-m-d');

        $diff =  $period." ".$duration;

        $expiry_date = date_add($date, date_interval_create_from_date_string($diff));
        
        $exp_date = date_format($expiry_date, 'Y-m-d');

        $newDate1 = new DateTime($newDate);
        $newExp = new DateTime($exp_date);
        $interval = $newDate1->diff($newExp);
        $days = $interval->days;
        $months = floor($days / 30);

        $credits = $months;

        if($credits >= 1){
            $status = 1;
        }
        

        $sql = "INSERT INTO subscriptions (subscriber_id, method_id, tax_id, subscription_date, expiry_date, credits, credits_left,status) ";
        $sql .= "VALUES({$subId}, {$method_id}, '{$taxid}', '{$newDate}', '{$exp_date}', {$credits}, {$credits}, '{$status}')";

        if(self::query($sql)){
            self::makePayment($subId);
            return true;
        }else{
            return false;
        }

    }

    public static function updateSubscription($subId, $method_id, $taxid, $period, $duration){

        date_default_timezone_set('Asia/Kolkata');
        $now = date('Y-m-d', strtotime('now'));

        $credits = 0;
        $status = 0;

        $date = new DateTime($now);

        $newDate = date_format($date, 'Y-m-d');

        $diff =  $period." ".$duration;

        $expiry_date = date_add($date, date_interval_create_from_date_string($diff));
        
        $exp_date = date_format($expiry_date, 'Y-m-d');

        $newDate1 = new DateTime($newDate);
        $newExp = new DateTime($exp_date);
        $interval = $newDate1->diff($newExp);
        $days = $interval->days;
        $months = floor($days / 30);

        $credits = $months;

        if($credits >= 1){
            $status = 1;
        }

        $sql = "UPDATE subscriptions SET method_id = {$method_id}, tax_id = '{$taxid}', expiry_date = '{$exp_date}', credits = {$credits}, credits_left = {$credits}, status = '{$status}' ";
        $sql .= "WHERE subscriber_id = {$subId}";

        if(self::query($sql)){
            self::makePayment($subId);
        }else{
            return false;
        }

        return mysqli_affected_rows(self::$connection) == 1 ? true : false;
    }



    public static function isPayed($subId){
        $result = self::query("SELECT payment FROM subscribers WHERE id = {$subId}");
        if($result->num_rows >= 1){
            foreach($result as $payment){
                $pay = $payment['payment'];
            }
            if($pay === 1){
                return true;
            }else{
                return false;
            }
        }
    }

    public static function checkSubscription($subId){
        if(self::isPayed($subId)){
            // check has a subscription
            $result = self::query("SELECT * FROM subscriptions WHERE subscriber_id = {$subId}");
            if($result->num_rows === 1){
                return true;
            }else{
                return false;
            }
        }else{
            return "Not payed";
        }   

    }

    public static function checkSubscription2($subId){
            // check has a subscription
            $result = self::query("SELECT * FROM subscriptions WHERE subscriber_id = {$subId}");
            if($result->num_rows === 1){
                return true;
            }else{
                return false;
            }  
    }

    public static function getSubscription($subId){

        $sql = "SELECT * FROM subscriptions WHERE subscriber_id = {$subId}";

        $results = self::query($sql);

        return $results;
    }

    public static function getIssues($subId){
        $results = self::query("SELECT * FROM dispatch_history WHERE subscriber_id = {$subId} ORDER BY date DESC");
        return $results;
    }

    public static function getIssueById($id){
        $issue = self::query("SELECT * FROM issues WHERE id = {$id}");
        return $issue;
    }

    public static function getCurrent(){
        $result = self::query("SELECT * FROM current_issue");
        foreach($result as $current){
            return $current;
        }   
    }

    public static function checkDispatch($subId, $issueId){
        $result = self::query("SELECT * FROM dispatch_history WHERE subscriber_id = {$subId} AND issue_id = {$issueId} LIMIT 1");
        if($result->num_rows >= 1){
            foreach($result as $log){
                $date = $log['date'];
            }
            return $date;
            
        }else{
            return false;
        }
    }

    public static function search($id, $phone){
        $result = self::query("SELECT * FROM subscribers WHERE id = {$id} AND phone = '{$phone}'");
        return $result;

    }


    public static function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }


}

$db = new Database();
?>