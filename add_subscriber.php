<?php
require_once('db.php');

if(isset($_GET['submit'])){
    $name = !empty($_GET['name']) ? $_GET['name'] : 'No data';
    $so = !empty($_GET['so']) ? $_GET['so'] : 'No data';
    $house = !empty($_GET['house']) ? $_GET['house'] : 'No data';
    $place = !empty($_GET['place']) ? $_GET['place'] : 'No data';
    $email = !empty($_GET['email']) ? $_GET['email'] : 'No data';
    $phone = !empty($_GET['phone']) ? $_GET['phone'] : 'No data';
    $district = !empty($_GET['district']) ? $_GET['district'] : 'No data';
    $post = !empty($_GET['post']) ? $_GET['post'] : 'No data';
    $pincode = !empty($_GET['pincode']) ? $_GET['pincode'] : 'No data';
    $type = !empty($_GET['type']) ? $_GET['type'] : 'No data';
    $duration = !empty($_GET['duration']) ? $_GET['duration'] : 'No data';
    $period = !empty($_GET['period']) ? $_GET['period'] : 'No data';
    $total = !empty($_GET['total']) ? $_GET['total'] : 'No data';
    $productInfo = "Thelicham Magazine ".$type ." ".$period." ".$duration;
    $pfee=($total * 2) / 100;
    $promo = $_GET['promoCode'];


    $name = $db->escapeString($name);
    $so = $db->escapeString($so);
    $house = $db->escapeString($house);
    $place = $db->escapeString($place);
    $email = $db->escapeString($email);
    $phone = $db->escapeString($phone);
    $district = $db->escapeString($district);
    $post = $db->escapeString($post);
    $pincode = $db->escapeString($pincode);
    $type = $db->escapeString($type);
    $duration = $db->escapeString($duration);
    $period = $db->escapeString($period);
    $total = $db->escapeString($total);
    $promo = $db->escapeString($promo);

    if($db::verifyEmail($email) == false){
        header('Location: index.php?error=email#form');
    }else{
        $add = $db->addSubscriber($name, $so, $house, $place, $email, $phone, $district, $post, $pincode, $period, $type, $duration, $total, $promo);
    }
    
}



?>